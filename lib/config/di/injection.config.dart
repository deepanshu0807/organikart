// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart'
    as _i3;
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart'
    as _i4;
import 'package:organikart_shared/domain/category/i_category_repo.dart' as _i6;
import 'package:organikart_shared/domain/features/i_customer_repo.dart' as _i7;
import 'package:organikart_shared/domain/orders/i_order_repo.dart' as _i8;
import 'package:organikart_shared/domain/payment/i_payment_repo.dart' as _i9;
import 'package:organikart_shared/organikart_shared_package.dart' as _i5;

import '../../application/basket_form_bloc/basket_form_bloc.dart' as _i15;
import '../../application/customer_details_form_bloc/customer_details_form_bloc.dart'
    as _i16;
import '../../application/customer_details_watcher_bloc/customer_details_watcher_bloc.dart'
    as _i17;
import '../../application/otp_login/otp_login_bloc.dart' as _i10;
import '../../application/saved_product_form_bloc/saved_product_form_bloc.dart'
    as _i11;
import '../../application/saved_product_watcher_bloc/saved_product_watcher_bloc.dart'
    as _i12;
import '../../application/sign_in_form/signinform_bloc.dart' as _i13;
import '../../application/sign_up_form/signupform_bloc.dart' as _i14;
import 'injectables.dart' as _i18; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String environment, _i2.EnvironmentFilter environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final blocInjectablemodule = _$BlocInjectablemodule();
  gh.lazySingleton<_i3.AuthWatcherBloc>(
      () => blocInjectablemodule.authWatcherBloc);
  gh.factory<_i4.CategoryWatcherBloc>(
      () => blocInjectablemodule.categoryWatcherBloc);
  gh.factory<_i5.ContentWatcherBloc>(
      () => blocInjectablemodule.contentWatcherBloc);
  gh.lazySingleton<_i5.FirebaseAuth>(() => blocInjectablemodule.fbAuth);
  gh.lazySingleton<_i5.FirebaseFirestore>(() => blocInjectablemodule.fireStore);
  gh.lazySingleton<_i5.FirebaseStorage>(() => blocInjectablemodule.storage);
  gh.lazySingleton<_i5.IAuth>(() => blocInjectablemodule.fbAuthService);
  gh.lazySingleton<_i6.ICategoryRepo>(() => blocInjectablemodule.categoryRepo);
  gh.lazySingleton<_i5.IContentRepo>(() => blocInjectablemodule.contentRepo);
  gh.lazySingleton<_i7.ICustomerRepo>(() => blocInjectablemodule.customerRepo);
  gh.lazySingleton<_i8.IOrdersRepo>(() => blocInjectablemodule.orderRepo);
  gh.lazySingleton<_i9.IPaymentRepo>(() => blocInjectablemodule.payService);
  gh.factory<_i10.OtpLoginBloc>(() => _i10.OtpLoginBloc(get<_i5.IAuth>()));
  gh.factory<_i11.SavedProductFormBloc>(
      () => _i11.SavedProductFormBloc(get<_i7.ICustomerRepo>()));
  gh.factory<_i12.SavedProductWatcherBloc>(
      () => _i12.SavedProductWatcherBloc(get<_i7.ICustomerRepo>()));
  gh.factory<_i13.SignInFormBloc>(() => _i13.SignInFormBloc(get<_i5.IAuth>()));
  gh.factory<_i14.SignupformBloc>(() => _i14.SignupformBloc(get<_i5.IAuth>()));
  gh.factory<_i15.BasketFormBloc>(() =>
      _i15.BasketFormBloc(get<_i8.IOrdersRepo>(), get<_i9.IPaymentRepo>()));
  gh.lazySingleton<_i16.CustomerDetailsFormBloc>(
      () => _i16.CustomerDetailsFormBloc(get<_i7.ICustomerRepo>()));
  gh.lazySingleton<_i17.CustomerDetailsWatcherBloc>(
      () => _i17.CustomerDetailsWatcherBloc(get<_i7.ICustomerRepo>()));
  return get;
}

class _$BlocInjectablemodule extends _i18.BlocInjectablemodule {}
