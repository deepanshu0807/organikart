import 'package:injectable/injectable.dart';
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/domain/category/i_category_repo.dart';
import 'package:organikart_shared/domain/features/i_customer_repo.dart';
import 'package:organikart_shared/domain/orders/i_order_repo.dart';
import 'package:organikart_shared/domain/payment/i_payment_repo.dart';
import 'package:organikart_shared/infrastructure/category/category_repo_mobile.dart';
import 'package:organikart_shared/infrastructure/content/content_repo_mobile.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:organikart_shared/infrastructure/features/customer_repo.dart';
import 'package:organikart_shared/infrastructure/order/order_repo.dart';
import 'package:organikart_shared/infrastructure/payment/razor_pay_service.dart';

import 'package:organikart_shared/organikart_shared_package.dart';
// import 'package:firebase/firebase.dart' as fb;

// import '../../application/auth/auth_bloc.dart' as _myAuth;

@module
abstract class BlocInjectablemodule {
  //Externals
  @lazySingleton
  FirebaseAuth get fbAuth => FirebaseAuth.instance;

  @lazySingleton
  FirebaseFirestore get fireStore => FirebaseFirestore.instance;

  @lazySingleton
  FirebaseStorage get storage => FirebaseStorage.instance;

  // Services
  @LazySingleton(as: IAuth)
  FirebaseAuthService get fbAuthService => FirebaseAuthService(fbAuth);

  @LazySingleton(as: ICustomerRepo)
  CustomerRepo get customerRepo => CustomerRepo(fireStore);

  //Blocs
  @lazySingleton
  AuthWatcherBloc get authWatcherBloc => AuthWatcherBloc(fbAuthService);

  @LazySingleton(as: ICategoryRepo)
  CategoryRepo get categoryRepo => CategoryRepo(fireStore, storage);

  @LazySingleton(as: IContentRepo)
  ContentRepo get contentRepo => ContentRepo(fireStore, storage);

  @LazySingleton(as: IOrdersRepo)
  OrderRepo get orderRepo => OrderRepo(fireStore);

  @LazySingleton(as: IPaymentRepo)
  RazorPayService get payService => RazorPayService();

  @injectable
  CategoryWatcherBloc get categoryWatcherBloc =>
      CategoryWatcherBloc(categoryRepo);

  @injectable
  ContentWatcherBloc get contentWatcherBloc => ContentWatcherBloc(contentRepo);
}
