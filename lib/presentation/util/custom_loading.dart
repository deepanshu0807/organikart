import 'package:flutter/material.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 45,
        width: 45,
        child: CircularProgressIndicator(
          backgroundColor: Colors.white,
          valueColor:
              AlwaysStoppedAnimation<Color>(AppColors.getPrimaryColor()),
        ),
      ),
    );
  }
}
