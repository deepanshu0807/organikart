import 'package:flutter/material.dart';

TextStyle text12 =
    TextStyle(fontFamily: 'Gilroy', fontSize: 12, color: Colors.black);

TextStyle text14 =
    TextStyle(fontFamily: 'Gilroy', fontSize: 14, color: Colors.black);

// TextStyle text16 =
//     TextStyle(fontFamily: 'Gilroy', fontSize: 16, color: Colors.black);
TextStyle text18 =
    TextStyle(fontFamily: 'Gilroy', fontSize: 18, color: Colors.black);

TextStyle text20 =
    TextStyle(fontFamily: 'Gilroy', fontSize: 20, color: Colors.black);

TextStyle text22 =
    TextStyle(fontFamily: 'Gilroy', fontSize: 22, color: Colors.black);

TextStyle text24 =
    TextStyle(fontFamily: 'Gilroy', fontSize: 24, color: Colors.black);
