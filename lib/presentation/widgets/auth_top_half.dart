import 'package:flutter/material.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class AuthTopHalf extends StatelessWidget {
  final String title;
  final bool isSignUp;
  const AuthTopHalf({
    Key key,
    this.title,
    this.isSignUp = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          Container(
            color: AppColors.getSecondaryColor().withOpacity(0.4),
            width: screenWidth(context),
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(70),
                ),
                color: Colors.white),
            width: screenWidth(context),
            child: Stack(
              children: [
                Center(
                  child: Image.asset(
                    isSignUp
                        ? "assets/images/signup.png"
                        : "assets/images/login.png",
                    width: screenWidth(context) / 1.5,
                    // height: 160.h,
                  ),
                ),
                Positioned(
                  bottom: isSignUp ? 15.h : 40.h,
                  left: 0,
                  right: 0,
                  child: Center(
                    child: Text(
                      title,
                      style: text24.copyWith(
                          fontWeight: FontWeight.bold,
                          color: AppColors.getPrimaryColor(),
                          fontSize: 34.sp),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
