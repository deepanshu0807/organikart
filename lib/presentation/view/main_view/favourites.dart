import 'package:flutter/material.dart';
import 'package:organikart/application/saved_product_form_bloc/saved_product_form_bloc.dart';
import 'package:organikart/application/saved_product_watcher_bloc/saved_product_watcher_bloc.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/view/other_view/product_detail.dart';
import 'package:organikart/presentation/widgets/cached_network_widget.dart';
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class Favourites extends StatefulWidget {
  @override
  _FavouritesState createState() => _FavouritesState();
}

class _FavouritesState extends State<Favourites> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[50],
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 15),
        height: screenHeight(context),
        width: screenWidth(context),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Your",
                      style: text24.copyWith(
                          fontSize: 30.sp,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey)),
                  Text("Wishlist",
                      overflow: TextOverflow.ellipsis,
                      style: text24.copyWith(
                          fontSize: 34.sp,
                          fontWeight: FontWeight.w600,
                          color: Colors.grey[700]))
                ],
              ),
            ),
            verticalSpaceLarge,
            BlocBuilder<SavedProductWatcherBloc, SavedProductWatcherState>(
                builder: (context, savedProductState) {
              return savedProductState.map(
                initial: (_) => Loading(),
                loadInProgress: (_) => Loading(),
                loadFailure: (_) => Loading(),
                loadSuccess: (p) {
                  return SizedBox(
                    height: screenHeight(context) / 1.4,
                    child: ListView.builder(
                      itemCount: p.savedProducts.allProducts.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        final thisProd = p.savedProducts.allProducts[index];
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProductDetail(
                                          content: thisProd,
                                        )));
                          },
                          child: Container(
                            height: 100.h,
                            margin: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 25),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x267088D2),
                                  blurRadius: 30.0,
                                  offset: Offset(0, 10),
                                ),
                              ],
                            ),
                            child: Stack(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20),
                                            bottomLeft: Radius.circular(20)),
                                        child: Hero(
                                          tag: thisProd.picUrl,
                                          child: ImageDisplayWidgetWithSize(
                                            url: thisProd.picUrl,
                                            height: 100.h,
                                            width: 120.w,
                                            fit: BoxFit.cover,
                                          ),
                                        )),
                                    horizontalSpaceMedium20,
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          verticalSpaceSmall,
                                          Text(
                                            thisProd.name,
                                            style: text22.copyWith(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 20),
                                          ),
                                          verticalSpaceTiny,
                                          Text(
                                            "in ${thisProd.category.title}",
                                            style: text16,
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                BlocBuilder<AuthWatcherBloc, AuthWatcherState>(
                                    builder: (context, authState) {
                                  return authState.map(
                                    initial: (e) => Container(),
                                    unauthenticated: (e) => Container(),
                                    authenticated: (e) {
                                      final user = e.user;
                                      return Positioned(
                                        top: 0,
                                        bottom: 0,
                                        right: 10,
                                        child: Container(
                                          alignment: Alignment.center,
                                          height: 40,
                                          width: 40,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: AppColors.getPrimaryColor(),
                                          ),
                                          child: InkWell(
                                            onTap: () {
                                              context
                                                  .read<SavedProductFormBloc>()
                                                  .add(
                                                    SavedProductFormEvent
                                                        .saveOrRemoveProductToMySavedList(
                                                            thisProd, user),
                                                  );
                                            },
                                            child: Icon(
                                              Icons.close,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  );
                                }),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                },
              );
            }),
          ],
        ),
      ),
    );
  }
}
