import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:organikart/application/basket_form_bloc/basket_form_bloc.dart';
import 'package:organikart/presentation/view/main_view/favourites.dart';
import 'package:organikart/presentation/view/main_view/home_page.dart';
import 'package:organikart/presentation/view/main_view/profile.dart';
import 'package:organikart/presentation/view/main_view/shopping_cart.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title, this.user}) : super(key: key);

  final String title;
  final OrganikartUser user;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: <Widget>[
              MyHomePage(),
              Favourites(),
              ShoppingCart(),
              Profile(
                user: widget.user,
              )
            ].elementAt(_selectedIndex),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              decoration: BoxDecoration(
                  color: Colors.white,
                  // image: DecorationImage(
                  //     fit: BoxFit.cover,
                  //     image: AssetImage("assets/images/test.jpg")),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50))),
              child: GNav(
                  gap: 8,
                  activeColor: Colors.white,
                  iconSize: 24,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  duration: Duration(milliseconds: 800),
                  tabBackgroundColor: Colors.grey[800],
                  tabs: [
                    GButton(
                      icon: Icons.home,
                      text: 'Home',
                      iconActiveColor: Colors.teal,
                      textColor: Colors.teal,
                      backgroundColor: Colors.teal[100],
                    ),
                    GButton(
                      icon: Icons.favorite,
                      text: 'Wished',
                      iconActiveColor: Colors.red,
                      textColor: Colors.red,
                      backgroundColor: Colors.red[100],
                    ),
                    GButton(
                      // leading: BlocBuilder<BasketFormBloc, BasketFormState>(
                      //   builder: (context, state) {
                      //     if (state.order.items.isEmpty) {
                      //       return Container();
                      //     } else {
                      //       return Text(
                      //         "${state.order.items.length}",
                      //         style: text12.copyWith(
                      //           color: Colors.amber,
                      //           fontWeight: FontWeight.bold,
                      //         ),
                      //       );
                      //     }
                      //   },
                      // ),
                      icon: Icons.card_travel,
                      text: 'Cart',
                      iconActiveColor: Colors.amber,
                      textColor: Colors.amber,
                      backgroundColor: Colors.amber[100],
                    ),
                    GButton(
                      icon: Icons.account_box,
                      text: 'Profile',
                      iconActiveColor: Colors.purple,
                      textColor: Colors.purple,
                      backgroundColor: Colors.purple[100],
                    ),
                  ],
                  selectedIndex: _selectedIndex,
                  onTabChange: (index) {
                    setState(() {
                      _selectedIndex = index;
                    });
                  }),
            ),
          )
        ],
      ),
    );
  }
}
