import 'package:flutter/material.dart';
import 'package:organikart/application/customer_details_watcher_bloc/customer_details_watcher_bloc.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/view/introductory_view/auth_navigator.dart';
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class Profile extends StatefulWidget {
  final OrganikartUser user;

  const Profile({Key key, this.user}) : super(key: key);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    // print("++++++++++++++++++++++++++ ${widget.user}");
    return Scaffold(
      backgroundColor: Colors.purple[50],
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        height: screenHeight(context),
        width: screenWidth(context),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Hi",
                        style: text24.copyWith(
                            fontSize: 30.sp,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey)),
                    Text("Customer",
                        overflow: TextOverflow.ellipsis,
                        style: text24.copyWith(
                            fontSize: 34.sp,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey[700])),
                  ],
                ),
                AuthNavigator(
                  child: BlocBuilder<AuthWatcherBloc, AuthWatcherState>(
                    builder: (context, state) {
                      return InkWell(
                        onTap: state.map(
                          initial: (_) => null,
                          authenticated: (user) => () {
                            context
                                .read<AuthWatcherBloc>()
                                .add(AuthWatcherEvent.signedOut(user.user));
                          },
                          unauthenticated: (_) => null,
                        ),
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(
                              vertical: 8.h, horizontal: 10.w),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x267088D2),
                                blurRadius: 30.0,
                                offset: Offset(0, 10),
                              ),
                            ],
                          ),
                          child: Icon(
                            Icons.logout,
                            color: Colors.purple,
                            size: 32.sp,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
            verticalSpaceLarge,
            ProfileButton(
              text: "user@gmail.com",
              onTap: () {},
              icon: Icons.email_outlined,
            ),
            verticalSpaceMedium30,
            ProfileButton(
              text: "Purchase History",
              onTap: () {},
              icon: Icons.shopping_basket_outlined,
            ),
            verticalSpaceMedium30,
            ProfileButton(
              text: "Terms of Use",
              onTap: () {},
              icon: Icons.policy_outlined,
            ),
            verticalSpaceMedium30,
            ProfileButton(
              text: "Privacy Policy",
              onTap: () {},
              icon: Icons.privacy_tip_outlined,
            ),
            verticalSpaceMedium30,
            ProfileButton(
              text: "About Us",
              onTap: () {},
              icon: Icons.info_outline,
            ),
            verticalSpaceMedium30,
            ProfileButton(
              text: "Rate Us",
              onTap: () {},
              icon: Icons.rate_review_outlined,
            ),
            verticalSpaceMedium30,
          ],
        ),
      ),
    );
  }
}

class ProfileButton extends StatelessWidget {
  final String text;
  final Function onTap;
  final IconData icon;
  const ProfileButton({
    Key key,
    this.text,
    this.onTap,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Color(0x267088D2),
            blurRadius: 30.0,
            offset: Offset(0, 10),
          ),
        ],
      ),
      child: FlatButton(
        splashColor: Colors.purple[200],
        focusColor: Colors.transparent,
        highlightColor: Colors.transparent,
        minWidth: double.infinity,
        height: 55,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: Colors.white,
        onPressed: onTap,
        child: Row(
          children: [
            Icon(
              icon,
              color: Colors.purple,
            ),
            Text(
              "  $text",
              style: text22,
            )
          ],
        ),
      ),
    );
  }
}
