import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/view/other_view/product_of_category.dart';
import 'package:organikart/presentation/widgets/cached_network_widget.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.only(bottom: 25),
        height: screenHeight(context),
        width: screenWidth(context),
        color: Colors.teal[50],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            verticalSpaceMedium30,
            Container(
              height: 50,
              width: double.infinity,
              margin: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
              padding: EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x267088D2),
                    blurRadius: 30.0,
                    offset: Offset(0, 10),
                  ),
                ],
              ),
              child: Row(
                children: [
                  Icon(
                    Icons.search,
                    color: Colors.teal,
                  ),
                  Text(
                    "  Search Products",
                    style: text18,
                  )
                ],
              ),
            ),
            verticalSpaceSmall,
            _title(),
            BlocBuilder<CategoryWatcherBloc, CategoryWatcherState>(
              builder: (context, state) {
                return state.map(
                  initial: (_) => Loading(),
                  loadInProgress: (_) => Loading(),
                  loadFailure: (_) => Loading(),
                  loadSuccess: (c) {
                    if (c.categories.isEmpty) {
                      return Center(
                        child: Text(
                          "No category found",
                          style: text22,
                        ),
                      );
                    } else {
                      return Expanded(
                        child: GridView.builder(
                          shrinkWrap: true,
                          primary: true,
                          physics: AlwaysScrollableScrollPhysics(),
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 18.h,
                            crossAxisSpacing: 18.w,
                          ),
                          itemCount: c.categories.length,
                          itemBuilder: (BuildContext context, int index) {
                            final cat = c.categories[index];
                            return InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductOfCategory(
                                              category: cat,
                                            )));
                              },
                              child: Container(
                                  margin: index % 2 == 0
                                      ? EdgeInsets.only(left: 15)
                                      : EdgeInsets.only(right: 15),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(20),
                                    // image: DecorationImage(
                                    //     image: NetworkImage(cat.titlePicUrl),
                                    //     fit: BoxFit.cover),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0x267088D2),
                                        blurRadius: 30.0,
                                        offset: Offset(0, 10),
                                      ),
                                    ],
                                  ),
                                  child: Stack(
                                    fit: StackFit.expand,
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(20),
                                        child: Hero(
                                          tag: cat.titlePicUrl,
                                          child: ImageDisplayWidget(
                                            url: cat.titlePicUrl,
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          gradient: LinearGradient(
                                              colors: [
                                                Colors.black,
                                                Colors.transparent
                                              ],
                                              begin: Alignment.bottomCenter,
                                              end: Alignment.topCenter),
                                        ),
                                      ),
                                      Positioned(
                                        bottom: 5,
                                        left: 5,
                                        right: 5,
                                        child: Text(
                                          cat.title,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.center,
                                          style: text18.copyWith(
                                              color: Colors.white,
                                              fontSize: 23),
                                        ),
                                      ),
                                    ],
                                  )),
                            );
                          },
                        ),
                      );
                    }
                  },
                );
              },
            ),
            verticalSpaceLarge
          ],
        ),
      ),
    );
  }

  _title() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 20.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Explore",
              style: text24.copyWith(
                  fontSize: 30.sp,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey)),
          Text("Categories",
              style: text24.copyWith(
                  fontSize: 34.sp,
                  fontWeight: FontWeight.w600,
                  color: Colors.grey[700]))
        ],
      ),
    );
  }
}
