import 'package:flutter/material.dart';
import 'package:organikart/application/basket_form_bloc/basket_form_bloc.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/widgets/cached_network_widget.dart';
import 'package:organikart_shared/domain/orders/orders_domain.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:organikart_shared/domain/orders/orders.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';

class ShoppingCart extends StatefulWidget {
  @override
  _ShoppingCartState createState() => _ShoppingCartState();
}

class _ShoppingCartState extends State<ShoppingCart> {
  IOrdersRepo _iOrdersRepo;
  final _razorpay = Razorpay();
  OrderEntity orderEntity;

  @override
  void initState() {
    super.initState();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  void openRazorpayPortal(OrderEntity order) async {
    var options = {
      'key': 'rzp_test_iqdVEyiV6zKZ28',
      'amount': (double.parse(order.amountToBePaid().toStringAsFixed(2)) * 100)
          .toInt(),
      'name': '${order?.items?.first?.name ?? "No item"} etc..',
      'description': "Placing order on organikart",
      'prefill': {
        'contact': order.customer.phoneNumber.getOrElse(""),
        'email': order.customer.emailAddress.getOrElse(""),
        'name': order.customer.name.getOrElse(""),
      },
      'external': {
        'wallets': ['paytm']
      },
      'theme': {
        'color': "#99BF00",
      }
    };

    _razorpay.open(options);
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    context.bloc<BasketFormBloc>().add(
        BasketFormEvent.onPaymentSuccess(orderEntity, PaymentMethod.online()));
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    context.bloc<BasketFormBloc>().add(
        BasketFormEvent.onPaymentFailed(orderEntity, PaymentMethod.online()));
  }

  void _handleExternalWallet(PaymentFailureResponse response) {}

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BasketFormBloc, BasketFormState>(
      builder: (context, state) {
        final allBasketProducts = state?.order?.items ?? [];
        return Scaffold(
          backgroundColor: Colors.amber[50],
          body: SafeArea(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 25),
              height: screenHeight(context),
              width: screenWidth(context),
              child: allBasketProducts.length == 0
                  ? ListView(
                      physics: BouncingScrollPhysics(),
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Your",
                                style: text24.copyWith(
                                    fontSize: 30.sp,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey)),
                            Text("Shopping Cart",
                                overflow: TextOverflow.ellipsis,
                                style: text24.copyWith(
                                    fontSize: 34.sp,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[700]))
                          ],
                        ),
                        verticalSpaceMedium30,
                        Column(
                          children: [
                            verticalSpaceMassive,
                            verticalSpaceMassive,
                            Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              color: Colors.black,
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: ListTile(
                                leading: Icon(
                                  Icons.error_outline,
                                  color: Colors.white,
                                ),
                                title: Text(
                                  'Your basket is empty ',
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .subtitle1
                                      .copyWith(color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    )
                  : ListView(
                      physics: BouncingScrollPhysics(),
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Your",
                                style: text24.copyWith(
                                    fontSize: 30.sp,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey)),
                            Text("Shopping Cart",
                                overflow: TextOverflow.ellipsis,
                                style: text24.copyWith(
                                    fontSize: 34.sp,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey[700]))
                          ],
                        ),
                        verticalSpaceMedium30,
                        ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: allBasketProducts.length,
                          shrinkWrap: true,
                          primary: false,
                          itemBuilder: (BuildContext context, int index) {
                            return CartItem(
                              product: allBasketProducts[index],
                            );
                          },
                        ),
                        verticalSpaceMedium25,
                        Text(
                          'Receipt',
                          style: text25.copyWith(color: Colors.grey),
                        ),
                        verticalSpaceMedium15,
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                          ),
                          padding: EdgeInsets.all(15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Price (${state.order.items.length} items)",
                                    style: text18.copyWith(
                                        color: Colors.grey.shade900),
                                  ),
                                  Text(
                                    "₹ ${state.order.totalActualPrice()}",
                                    style: text24.copyWith(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              verticalSpaceSmall,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Discount (${state.order.items.length} items)",
                                    style: text18.copyWith(
                                        color: Colors.grey.shade900),
                                  ),
                                  Text(
                                    "₹ ${state.order.totalDiscount()}",
                                    style: text24.copyWith(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              verticalSpaceSmall,
                              Divider(
                                color: AppColors.getPrimaryColor(),
                              ),
                              verticalSpaceSmall,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Order Total",
                                    style: text18.copyWith(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "₹ ${state.order.amountToBePaid()}",
                                    style: text24.copyWith(
                                        fontWeight: FontWeight.bold,
                                        color: AppColors.getPrimaryColor()),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        verticalSpaceMedium30,
                        TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.amber,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            minimumSize: Size(double.infinity, 60),
                          ),
                          onPressed: () {
                            setState(() {
                              orderEntity = state.order;
                            });
                            // _iOrdersRepo.createOrder(state.order);
                            openRazorpayPortal(state.order);
                            // context.read<BasketFormBloc>().add(
                            //     BasketFormEvent.onPaymentSuccess(
                            //         state.order, PaymentMethod.online()));
                          },
                          child: Text(
                            "Confirm & Pay",
                            style: text22.copyWith(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                        verticalSpaceMassive,
                      ],
                    ),
            ),
          ),
        );
      },
    );
  }
}

class CartItem extends StatelessWidget {
  final Content product;
  CartItem({this.product});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120.h,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Stack(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(20)),
                  child: ImageDisplayWidgetWithSize(
                    url: product.picUrl,
                    height: 120.h,
                    width: 120.w,
                    fit: BoxFit.cover,
                  )),
              horizontalSpaceMedium20,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    verticalSpaceSmall,
                    Text(
                      product.name,
                      style: text22.copyWith(
                          fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    verticalSpaceTiny,
                    Text(
                      "in ${product.category.title}",
                      style: text16,
                    ),
                    verticalSpaceMedium15,
                    Row(
                      children: [
                        Text(
                          "₹ ${product.discountedMrp.getOrElse(0)}",
                          style: text18.copyWith(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                        horizontalSpaceSmall,
                        Text("₹ ${product.mrp.getOrElse(0)}",
                            style: text14.copyWith(
                                decoration: TextDecoration.lineThrough,
                                color: Colors.black54)),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          BlocBuilder<BasketFormBloc, BasketFormState>(
            builder: (context, state) {
              return Positioned(
                top: 0,
                bottom: 0,
                right: 10,
                child: Container(
                  alignment: Alignment.center,
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.red,
                  ),
                  child: InkWell(
                    onTap: () {
                      context
                          .read<BasketFormBloc>()
                          .add(BasketFormEvent.subtractItem(product));
                    },
                    child: Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
