import 'package:flutter/material.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/util/walkthrough_model.dart';
import 'package:organikart/presentation/view/introductory_view/sign_in.dart';
import 'package:organikart/managers/shared_preferences_manager.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class WalkThroughView extends StatefulWidget {
  @override
  _WalkThroughViewState createState() => _WalkThroughViewState();
}

class _WalkThroughViewState extends State<WalkThroughView> {
  PageController _pageController;
  double _currentPage = 0.0;
  List<WalkThroughModel> _onBoardingList;

  bool _onScroll(ScrollNotification notification) {
    final metrics = notification.metrics;
    if (metrics is PageMetrics) {
      setState(() => _currentPage = metrics.page);
    }
    return false;
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
    _onBoardingList = [
      WalkThroughModel(
        imageName: 'walk1.png',
        title: "True Organic Products",
      ),
      WalkThroughModel(
        imageName: 'walk2.png',
        title: "Order Online from Anywhere",
      ),
      WalkThroughModel(
        imageName: 'walk3.png',
        title: "Its only Seller and You",
      ),
    ];
  }

  _setWalkThroughVisitedState() async {
    await SharedPreferencesManager.setIsWalkThrough(true);
  }

  @override
  void dispose() {
    _pageController.dispose();
    _onBoardingList.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: screenHeight(context),
            width: screenWidth(context),
            color: Colors.white,
            child: NotificationListener<ScrollNotification>(
              onNotification: _onScroll,
              child: PageView.builder(
                itemCount: _onBoardingList.length,
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                controller: _pageController,
                itemBuilder: (BuildContext context, int index) {
                  return Column(
                    children: [
                      verticalSpaceLarge,
                      Text(
                        _currentPage == 0
                            ? '1/3'
                            : _currentPage == 1
                                ? '2/3'
                                : '3/3',
                        style: text24.copyWith(
                            fontSize: 18.sp,
                            fontWeight: FontWeight.bold,
                            color: AppColors.getSecondaryColor()),
                        textAlign: TextAlign.center,
                      ),
                      verticalSpaceLarge,
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 55.0),
                        child: Text(
                          '${_onBoardingList[index].title}',
                          style: text24.copyWith(
                              fontSize: 34.sp,
                              fontWeight: FontWeight.bold,
                              color: AppColors.getPrimaryColor()),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      verticalSpaceLarge,
                      Image.asset(
                        "assets/images/${_onBoardingList[index].imageName}",
                        height: 330.h,
                        width: screenWidth(context),
                        fit: BoxFit.cover,
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
          Positioned(
            bottom: 75.h,
            left: 0,
            right: 0,
            child: Container(
              height: 80,
              width: 80,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.getPrimaryColor(),
                // boxShadow: [
                //   BoxShadow(
                //       offset: Offset(-5, -5),
                //       spreadRadius: -5,
                //       blurRadius: 20,
                //       color: Colors.grey)
                // ],
              ),
              child: InkWell(
                onTap: () {
                  if (_currentPage == 2) {
                    _setWalkThroughVisitedState();
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => SignIn()));
                  } else {
                    _pageController.animateToPage(_currentPage.toInt() + 1,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.decelerate);
                  }
                },
                child: _currentPage == 2.0
                    ? Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 30,
                      )
                    : Icon(
                        Icons.keyboard_arrow_right,
                        color: Colors.white,
                        size: 35,
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
