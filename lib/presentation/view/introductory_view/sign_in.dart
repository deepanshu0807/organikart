import 'package:flutter/material.dart';
import 'package:organikart/application/sign_in_form/signinform_bloc.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/view/introductory_view/auth_navigator.dart';

import 'package:organikart/presentation/view/main_view/main_page.dart';
import 'package:organikart/presentation/widgets/auth_top_half.dart';
import 'package:organikart/routes/router.gr.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:progress_indicators/progress_indicators.dart';

class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AuthNavigator(
      child: BlocBuilder<SignInFormBloc, SignInFormState>(
          builder: (context, state) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            height: screenHeight(context),
            width: screenWidth(context),
            child: Column(
              children: [
                AuthTopHalf(
                  title: 'Sign In',
                ),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(70),
                      ),
                      color: AppColors.getSecondaryColor().withOpacity(0.4),
                    ),
                    width: screenWidth(context),
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.symmetric(horizontal: 40.w),
                      children: [
                        verticalSpaceLarge,
                        Text(
                          "Email Address",
                          style: text18,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: TextFormField(
                                style: text18.copyWith(
                                    fontSize: 37.sp,
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.getPrimaryColor()),
                                cursorColor: Colors.black,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  hintText: "Enter ",
                                  hintStyle: text18.copyWith(
                                      fontSize: 37.sp,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey),
                                ),
                                onChanged: (value) {
                                  context
                                      .read<SignInFormBloc>()
                                      .add(SignInFormEvent.emailChanged(value));
                                },
                                validator: (_) => context
                                    .read<SignInFormBloc>()
                                    .state
                                    .emailAddress
                                    .value
                                    .fold(
                                      (f) => f.maybeMap(
                                          invalidEmailAdress: (_) =>
                                              "Invalid Email",
                                          orElse: () => null),
                                      (_) => null,
                                    ),
                              ),
                            )
                          ],
                        ),
                        verticalSpaceMedium30,
                        Text(
                          "Password",
                          style: text18,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: TextFormField(
                            obscureText: true,
                            style: text18.copyWith(
                                fontSize: 37.sp,
                                fontWeight: FontWeight.bold,
                                color: AppColors.getPrimaryColor()),
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: "********",
                              hintStyle: text18.copyWith(
                                  fontSize: 37.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            onChanged: (value) {
                              context
                                  .read<SignInFormBloc>()
                                  .add(SignInFormEvent.passwordChanged(value));
                            },
                          ),
                        ),
                        verticalSpaceMedium30,
                        state.isSubmitting
                            ? Loading()
                            : FlatButton(
                                height: 50,
                                color: AppColors.getPrimaryColor(),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)),
                                onPressed: () {
                                  context.read<SignInFormBloc>().add(
                                      SignInFormEvent
                                          .signInWithEmailAndPasswordPressed());
                                },
                                child: Text(
                                  "Sign In",
                                  style: text18.copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                        verticalSpaceMedium15,
                        Center(
                          child: InkWell(
                            onTap: () {
                              ExtendedNavigator.of(context).push(Routes.signup);
                            },
                            child: Text(
                              "Don\'t have an account? Sign Up",
                              style: text16.copyWith(
                                  color: AppColors.getPrimaryColor()),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }
}
