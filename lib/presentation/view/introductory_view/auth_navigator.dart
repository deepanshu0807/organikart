import 'package:flutter/material.dart';
import 'package:organikart/application/basket_form_bloc/basket_form_bloc.dart';
import 'package:organikart/application/customer_details_form_bloc/customer_details_form_bloc.dart';
import 'package:organikart/application/customer_details_watcher_bloc/customer_details_watcher_bloc.dart';
import 'package:organikart/application/saved_product_watcher_bloc/saved_product_watcher_bloc.dart';
import 'package:organikart/application/sign_in_form/signinform_bloc.dart';
import 'package:organikart/application/sign_up_form/signupform_bloc.dart';
import 'package:organikart/presentation/view/main_view/main_page.dart';
import 'package:organikart/routes/router.gr.dart';
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class AuthNavigator extends StatelessWidget {
  final Widget child;

  const AuthNavigator({Key key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<AuthWatcherBloc, AuthWatcherState>(
          listener: (context, state) {
            state.map(
              initial: (_) {
                ExtendedNavigator.of(context)
                    .pushAndRemoveUntil(Routes.splash, (route) => false);
              },
              authenticated: (authUser) {
                print("%%%%%%%%%%%%%%%%%%%%%% authuser: " +
                    authUser.user.toString());
                context.read<CustomerDetailsWatcherBloc>().add(
                    CustomerDetailsWatcherEvent.getMySavedDetails(
                        authUser.user));
                context.read<SavedProductWatcherBloc>().add(
                    SavedProductWatcherEvent.getMySavedProducts(authUser.user));

                Future.delayed(
                  const Duration(milliseconds: 1500),
                  () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MainPage(
                                  user: authUser.user,
                                )));
                  },
                );

                ExtendedNavigator.of(context)
                    .pushAndRemoveUntil(Routes.main, (route) => false);
              },
              unauthenticated: (_) {
                ExtendedNavigator.of(context)
                    .pushAndRemoveUntil(Routes.onboarding, (route) => false);
              },
            );
          },
        ),
        BlocListener<SignupformBloc, SignupformState>(
          listener: (context, state) {
            print(state);
            state.authFailureOrSuccessOption.fold(
              () => null,
              (a) => a.fold(
                (l) {
                  l.maybeMap(
                    phoneAlreadyExist: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "This phone number already exist!");
                    },
                    emailAlreadyExist: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "This email already exist!");
                    },
                    invalidCredential: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "Invalid input data");
                    },
                    orElse: () {
                      DisplayMessage.showErrorMessage(context, "Unknown error");
                    },
                  );
                },
                (r) {
                  DisplayMessage.showSuccessMessage(
                      context, "Welcome to Organikart!");
                },
              ),
            );
          },
        ),
        BlocListener<SignInFormBloc, SignInFormState>(
          listener: (context, state) {
            print(state);
            state.authFailureOrSuccessOption.fold(
              () => null,
              (a) => a.fold(
                (l) {
                  l.maybeMap(
                    phoneAlreadyExist: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "This phone number already exist!");
                    },
                    emailAlreadyExist: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "This email already exist!");
                    },
                    invalidCredential: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "Invalid Email or Password");
                    },
                    invalidEmailOrPasswordValue: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "Invalid Email or Password");
                    },
                    invalidEmailPasswordCombination: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "Invalid Email or Password");
                    },
                    invalidEmail: (_) {
                      DisplayMessage.showErrorMessage(
                          context, "Invalid Email address");
                    },
                    orElse: () {
                      DisplayMessage.showErrorMessage(context, "Unknown error");
                    },
                  );
                },
                (r) {
                  DisplayMessage.showSuccessMessage(
                      context, "Welcome to Organikart!");
                },
              ),
            );
          },
        ),
        BlocListener<CustomerDetailsWatcherBloc, CustomerDetailsWatcherState>(
          listener: (context, state) {
            state.map(
              initial: (_) {},
              loadInProgress: (_) {},
              loadFailure: (_) {},
              loadSuccess: (load) {
                debugPrint("StoreUser:${load.storeUser.name}");
                context
                    .read<CustomerDetailsFormBloc>()
                    .add(CustomerDetailsFormEv.initializeUser(load.storeUser));
              },
            );
          },
        ),
        BlocListener<BasketFormBloc, BasketFormState>(
          listener: (context, state) {
            state.saveFailureOrSuccessOption.fold(
              () {},
              (a) => a.fold(
                (l) {
                  DisplayMessage.showErrorMessage(
                      context, l.getDisplayMessage());
                },
                (r) {
                  DisplayMessage.showSuccessMessage(
                      context, DisplayMessage.orderIsSaved);
                  context
                      .read<BasketFormBloc>()
                      .add(BasketFormEvent.initialize());
                },
              ),
            );
          },
        ),
      ],
      child: child,
    );
  }
}
