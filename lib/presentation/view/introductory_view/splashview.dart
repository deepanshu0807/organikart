import 'dart:async';
import 'package:flutter/material.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/managers/shared_preferences_manager.dart';
import 'package:organikart/presentation/view/introductory_view/auth_navigator.dart';
import 'package:organikart/presentation/view/introductory_view/walkthrough_view.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:progress_indicators/progress_indicators.dart';

// class SplashView extends StatefulWidget {
//   @override
//   _SplashViewState createState() => _SplashViewState();
// }

// class _SplashViewState extends State<SplashView> {
//   @override
//   void initState() {
//     super.initState();
//     //startTimeout();
//   }

// startTimeout() {
//   return Timer(Duration(seconds: 5), changeScreen);
// }

// changeScreen() async {
//   await SharedPreferencesManager.getIsWalkThrough()
//       .then((value) => value == true
//           ? Navigator.pushReplacement(context,
//               MaterialPageRoute(builder: (context) => WalkThroughView()))
//           // Navigator.pushReplacement(context,
//           //     MaterialPageRoute(builder: (context) => Authentication()))
//           : Navigator.pushReplacement(context,
//               MaterialPageRoute(builder: (context) => WalkThroughView())))
//       .catchError((error) {
//     print(error);
//   });
// }

class SplashView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AuthNavigator(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Image.asset(
                  'assets/images/main.png',
                ),
                verticalSpaceMedium15,
                Text(
                  'OrganiKart',
                  style: TextStyle(
                    color: AppColors.getPrimaryColor(),
                    fontSize: 30.0,
                    fontFamily: 'Benillia',
                  ),
                ),
                verticalSpaceSmall,
                Text(
                  "Organic Products from Mountains for You",
                  textAlign: TextAlign.center,
                  style: text18,
                )
              ],
            ),
            JumpingDotsProgressIndicator(
              fontSize: 60,
              color: AppColors.getSecondaryColor(),
            )
          ],
        )),
      ),
    );
  }
}
