// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';

// import 'package:organikart/presentation/view/main_view/main_page.dart';

// class UserInputScreen extends StatefulWidget {
//   @override
//   _UserInputScreenState createState() => _UserInputScreenState();
// }

// class _UserInputScreenState extends State<UserInputScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         children: <Widget>[
//           Container(
//             height: 730,
//             width: 600,
//             child: Image.asset("assets/images/bg.jpg", fit: BoxFit.fitHeight),
//           ),
//           Container(
//             padding: EdgeInsets.all(10),
//             margin: EdgeInsets.fromLTRB(0, 7, 60, 20),
//             //width: 300,
//             height: 500,
//             transform: Matrix4.translationValues(30, 100, 0),
//             decoration: BoxDecoration(
//               border: Border.all(
//                 color: Colors.white,
//                 width: 1,
//               ),
//               borderRadius: BorderRadius.circular(60),
//               color: Colors.black38.withOpacity(0.6),
//             ),
//             child: SingleChildScrollView(
//               child: Column(
//                 children: <Widget>[
//                   Container(
//                     padding: EdgeInsets.fromLTRB(10, 3, 3, 0),
//                     margin: EdgeInsets.fromLTRB(20, 110, 20, 0),
//                     decoration: BoxDecoration(
//                       border: Border.all(
//                         color: Colors.white,
//                         width: 2,
//                       ),
//                       borderRadius: BorderRadius.circular(30),
//                     ),
//                     child: TextField(
//                       decoration: InputDecoration(
//                         border: InputBorder.none,
//                         hintText: "enter name",
//                         hintStyle: TextStyle(
//                           color: Colors.white,
//                         ),
//                       ),
//                     ),
//                   ),
//                   Container(
//                     padding: EdgeInsets.fromLTRB(10, 3, 3, 0),
//                     margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
//                     decoration: BoxDecoration(
//                       border: Border.all(
//                         color: Colors.white,
//                         width: 2,
//                       ),
//                       borderRadius: BorderRadius.circular(30),
//                     ),
//                     child: Row(
//                       children: <Widget>[
//                         Container(
//                           width: 40,
//                           child: Icon(
//                             Icons.email,
//                             color: Colors.white,
//                           ),
//                         ),
//                         Expanded(
//                           child: TextField(
//                             decoration: InputDecoration(
//                               border: InputBorder.none,
//                               hintText: "enter email",
//                               hintStyle: TextStyle(
//                                 color: Colors.white,
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   Container(
//                     padding: EdgeInsets.fromLTRB(10, 3, 3, 0),
//                     margin: EdgeInsets.fromLTRB(20, 20, 20, 50),
//                     decoration: BoxDecoration(
//                       border: Border.all(
//                         color: Colors.white,
//                         width: 2,
//                       ),
//                       borderRadius: BorderRadius.circular(30),
//                     ),
//                     child: TextField(
//                       decoration: InputDecoration(
//                         border: InputBorder.none,
//                         hintText: "enter location",
//                         hintStyle: TextStyle(
//                           color: Colors.white,
//                         ),
//                       ),
//                     ),
//                   ),
//                   Container(
//                     margin: EdgeInsets.fromLTRB(20, 60, 20, 0),
//                     child: Container(
//                       child: IconButton(
//                         icon: Icon(Icons.arrow_forward),
//                         onPressed: () {
//                           Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => MainPage()));
//                         },
//                         color: Colors.white,
//                         iconSize: 30,
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//           Container(
//             //alignment: Alignment.topCenter,
//             margin: EdgeInsets.fromLTRB(130, 50, 130, 0),
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(49),
//               border: Border.all(
//                 color: Colors.white,
//                 width: 1,
//               ),
//               gradient: LinearGradient(
//                 colors: [Colors.orangeAccent, Colors.brown, Colors.white],
//                 begin: Alignment.topCenter,
//                 end: Alignment.bottomCenter,
//               ),
//             ),
//             child: CircleAvatar(
//               backgroundColor: Colors.transparent,
//               child:
//                   Image.asset("assets/images/logo.png", fit: BoxFit.fitHeight),
//             ),
//             height: 100,
//             width: 100,
//           ),
//         ],
//       ),
//     );
//   }
// }
