import 'package:flutter/material.dart';
import 'package:organikart/application/customer_details_form_bloc/customer_details_form_bloc.dart';
import 'package:organikart/application/sign_up_form/signupform_bloc.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/view/introductory_view/auth_navigator.dart';
import 'package:organikart/presentation/widgets/auth_top_half.dart';
import 'package:organikart/routes/router.gr.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:progress_indicators/progress_indicators.dart';

class SignUp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AuthNavigator(
      child: BlocBuilder<SignupformBloc, SignupformState>(
          builder: (context, state) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: Container(
            height: screenHeight(context),
            width: screenWidth(context),
            child: Column(
              children: [
                AuthTopHalf(
                  title: 'Sign Up',
                  isSignUp: true,
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(70),
                      ),
                      color: AppColors.getSecondaryColor().withOpacity(0.4),
                    ),
                    width: screenWidth(context),
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.symmetric(horizontal: 40.w),
                      children: [
                        verticalSpaceLarge,
                        Text(
                          "Name",
                          style: text18,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: TextFormField(
                            style: text18.copyWith(
                                fontSize: 37.sp,
                                fontWeight: FontWeight.bold,
                                color: AppColors.getPrimaryColor()),
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: "Enter ",
                              hintStyle: text18.copyWith(
                                  fontSize: 37.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            onChanged: (value) {
                              context
                                  .read<SignupformBloc>()
                                  .add(SignupformEvent.nameChanged(value));
                              context.read<CustomerDetailsFormBloc>().add(
                                  CustomerDetailsFormEv.nameChanged(value));
                            },
                          ),
                        ),
                        verticalSpaceMedium30,
                        Text(
                          "Phone Number",
                          style: text18,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: TextFormField(
                            style: text18.copyWith(
                                fontSize: 37.sp,
                                fontWeight: FontWeight.bold,
                                color: AppColors.getPrimaryColor()),
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: "+91 ...",
                              hintStyle: text18.copyWith(
                                  fontSize: 37.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            onChanged: (value) {
                              context.read<CustomerDetailsFormBloc>().add(
                                  CustomerDetailsFormEv.phoneNumberChanged(
                                      value));
                            },
                          ),
                        ),
                        verticalSpaceMedium30,
                        Text(
                          "Email Address",
                          style: text18,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: TextFormField(
                            style: text18.copyWith(
                                fontSize: 37.sp,
                                fontWeight: FontWeight.bold,
                                color: AppColors.getPrimaryColor()),
                            cursorColor: Colors.black,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: "Enter ",
                              hintStyle: text18.copyWith(
                                  fontSize: 37.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            onChanged: (value) {
                              context
                                  .read<SignupformBloc>()
                                  .add(SignupformEvent.emailChanged(value));
                              context.read<CustomerDetailsFormBloc>().add(
                                  CustomerDetailsFormEv.emailChanged(value));
                            },
                            validator: (_) => context
                                .read<SignupformBloc>()
                                .state
                                .emailAddress
                                .value
                                .fold(
                                  (f) => f.maybeMap(
                                      invalidEmailAdress: (_) =>
                                          "Invalid Email",
                                      orElse: () => null),
                                  (_) => null,
                                ),
                          ),
                        ),
                        verticalSpaceMedium30,
                        Text(
                          "Password",
                          style: text18,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: TextFormField(
                            obscureText: true,
                            style: text18.copyWith(
                                fontSize: 37.sp,
                                fontWeight: FontWeight.bold,
                                color: AppColors.getPrimaryColor()),
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              hintText: "********",
                              hintStyle: text18.copyWith(
                                  fontSize: 37.sp,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey),
                            ),
                            onChanged: (value) {
                              context
                                  .read<SignupformBloc>()
                                  .add(SignupformEvent.passwordChanged(value));
                            },
                          ),
                        ),
                        verticalSpaceMedium30,
                        state.isSubmitting
                            ? Loading()
                            : FlatButton(
                                height: 50,
                                color: AppColors.getPrimaryColor(),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)),
                                onPressed: () {
                                  context
                                      .read<SignupformBloc>()
                                      .add(SignupformEvent.signUp());
                                  context.read<CustomerDetailsFormBloc>().add(
                                      const CustomerDetailsFormEv
                                          .saveChanges());
                                },
                                child: Text(
                                  "Create Account",
                                  style: text18.copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                        verticalSpaceMedium15,
                        Center(
                          child: InkWell(
                            onTap: () {
                              ExtendedNavigator.of(context).push(Routes.signin);
                            },
                            child: Text(
                              "Already have an account? Sign In",
                              style: text16.copyWith(
                                  color: AppColors.getPrimaryColor()),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }
}
