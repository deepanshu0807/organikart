import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:organikart/application/basket_form_bloc/basket_form_bloc.dart';
import 'package:organikart/application/saved_product_form_bloc/saved_product_form_bloc.dart';
import 'package:organikart/application/saved_product_watcher_bloc/saved_product_watcher_bloc.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/view/other_view/product_detail.dart';
import 'package:organikart/presentation/widgets/cached_network_widget.dart';
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/domain/category/category.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

class ProductOfCategory extends StatelessWidget {
  final Category category;

  const ProductOfCategory({Key key, @required this.category}) : super(key: key);

  void showFloatingFlushbar(BuildContext context) {
    Flushbar(
      margin: EdgeInsets.all(10),
      duration: Duration(seconds: 4),
      padding: EdgeInsets.all(10),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [AppColors.getPrimaryColor(), AppColors.getSecondaryColor()],
      ),
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      title: 'Yay! Item added to Cart',
      message: 'Go to checkout page to place order',
    ).show(context);
  }

  @override
  Widget build(BuildContext context) {
    context.read<ContentWatcherBloc>().add(ContentWatcherEvent.getContent());
    return Scaffold(
      backgroundColor: AppColors.getSecondaryColor(),
      body: Container(
        height: screenHeight(context),
        width: screenWidth(context),
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            verticalSpaceMedium30,
            InkWell(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                alignment: Alignment.center,
                height: 40.h,
                width: 45.w,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey[300],
                        blurRadius: 10.0,
                      ),
                    ]),
                child: Icon(
                  Icons.keyboard_arrow_left_outlined,
                  color: AppColors.getSecondaryColor(),
                  size: 35.sp,
                ),
              ),
            ),
            Row(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("You are in",
                          style: text24.copyWith(
                              fontSize: 18.sp,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey)),
                      SizedBox(
                        child: Text(category.title,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: text24.copyWith(
                                fontSize: 28.sp,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[700])),
                      )
                    ],
                  ),
                ),
                horizontalSpaceMedium15,
                Container(
                  height: 120.h,
                  width: 120.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0x267088D2),
                        blurRadius: 30.0,
                        offset: Offset(0, 10),
                      ),
                    ],
                  ),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Hero(
                        tag: category.titlePicUrl,
                        child: ImageDisplayWidget(
                          url: category.titlePicUrl,
                          fit: BoxFit.cover,
                        ),
                      )),
                )
              ],
            ),
            verticalSpaceMedium30,
            Expanded(
              child: SizedBox(
                  child: BlocBuilder<ContentWatcherBloc, ContentWatcherState>(
                builder: (context, state) {
                  return state.map(
                    initial: (_) => Loading(),
                    loadInProgress: (_) => Loading(),
                    loadFailure: (_) => Loading(),
                    loadSuccess: (c) {
                      final products = c.content
                          .where((e) => e.category.id == category.id)
                          .toList();

                      if (products.isEmpty) {
                        return Center(
                          child: Text(
                            "Products coming soon",
                            style: text22.copyWith(color: Colors.black),
                          ),
                        );
                      } else {
                        return ListView.builder(
                          itemCount: products.length,
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            final thisProduct = products[index];
                            return InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductDetail(
                                              content: thisProduct,
                                            )));
                              },
                              child: Container(
                                height: 140.h,
                                margin: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0x267088D2),
                                      blurRadius: 30.0,
                                      offset: Offset(0, 10),
                                    ),
                                  ],
                                ),
                                child: Stack(
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        ClipRRect(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(20),
                                                bottomLeft:
                                                    Radius.circular(20)),
                                            child: Hero(
                                              tag: thisProduct.picUrl,
                                              child: ImageDisplayWidgetWithSize(
                                                height: 140.h,
                                                width: 120.w,
                                                url: thisProduct.picUrl,
                                                fit: BoxFit.cover,
                                              ),
                                            )),
                                        horizontalSpaceMedium15,
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              // verticalSpaceSmall,
                                              Text(
                                                thisProduct.name,
                                                style: text22.copyWith(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20),
                                              ),
                                              // verticalSpaceMedium15,
                                              RichText(
                                                text: TextSpan(
                                                  children: [
                                                    TextSpan(
                                                        text:
                                                            "₹${thisProduct.mrp.getOrElse(0)}",
                                                        style: text18.copyWith(
                                                            color: Colors.black,
                                                            fontSize: 18.sp,
                                                            decoration:
                                                                TextDecoration
                                                                    .lineThrough,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold)),
                                                    TextSpan(
                                                        text:
                                                            "  ₹${thisProduct.discountedMrp.getOrElse(0)}",
                                                        style: text14.copyWith(
                                                            color: AppColors
                                                                .getPrimaryColor(),
                                                            fontSize: 22.sp,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold))
                                                  ],
                                                ),
                                              ),
                                              // verticalSpaceSmall,
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    vertical: 5),
                                                width: 90.w,
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                  color: AppColors
                                                      .getPrimaryColor(),
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                ),
                                                child: RichText(
                                                  text: TextSpan(
                                                    children: [
                                                      TextSpan(
                                                          text:
                                                              "${thisProduct.discountPercentage.getOrElse(0)}%",
                                                          style: text18.copyWith(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 19.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                      TextSpan(
                                                          text: " Off",
                                                          style: text14.copyWith(
                                                              color:
                                                                  Colors.white,
                                                              fontSize: 16.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold))
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    Positioned(
                                      top: 0,
                                      bottom: 0,
                                      right: 10,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          BlocBuilder<BasketFormBloc,
                                              BasketFormState>(
                                            builder: (context, state) {
                                              return InkWell(
                                                onTap: () {
                                                  if (state.order.items
                                                      .contains(thisProduct)) {
                                                    context
                                                        .read<BasketFormBloc>()
                                                        .add(BasketFormEvent
                                                            .subtractItem(
                                                                thisProduct));
                                                  } else {
                                                    context
                                                        .bloc<BasketFormBloc>()
                                                        .add(BasketFormEvent
                                                            .addItem(
                                                                thisProduct));
                                                    showFloatingFlushbar(
                                                        context);
                                                  }
                                                },
                                                child: Icon(
                                                  state.order.items
                                                          .contains(thisProduct)
                                                      ? Icons.check_box
                                                      : Icons
                                                          .shopping_bag_outlined,
                                                  color: AppColors
                                                      .getPrimaryColor(),
                                                ),
                                              );
                                            },
                                          ),

                                          SaveUnSaveProductWidget(
                                              product: thisProduct),
                                          // InkWell(
                                          //   onTap: () {},
                                          //   child: Icon(
                                          //     Icons.favorite_border,
                                          //     color: Colors.red,
                                          //   ),
                                          // ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      }
                    },
                  );
                },
              )),
            )
          ],
        ),
      ),
    );
  }
}

class SaveUnSaveProductWidget extends StatelessWidget {
  final Content product;
  const SaveUnSaveProductWidget({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthWatcherBloc, AuthWatcherState>(
        builder: (context, authState) {
      return authState.map(
        initial: (e) => Container(),
        unauthenticated: (e) => Container(),
        authenticated: (e) {
          final user = e.user;
          return BlocBuilder<SavedProductWatcherBloc, SavedProductWatcherState>(
            builder: (context, savedProductState) {
              final isThisProductSaved = savedProductState.map(
                initial: (_) => null,
                loadInProgress: (_) => null,
                loadFailure: (_) => null,
                loadSuccess: (p) {
                  return p.savedProducts.allProducts.firstWhere(
                      (element) =>
                          element.id.getOrElse("dflt") ==
                          product.id.getOrElse("Na"),
                      orElse: () => null);
                },
              );

              return InkWell(
                child: Icon(
                  isThisProductSaved == null
                      ? Icons.favorite_border
                      : Icons.favorite,
                  color: Colors.red,
                ),
                onTap: () {
                  print("tapped");
                  context.read<SavedProductFormBloc>().add(
                        SavedProductFormEvent.saveOrRemoveProductToMySavedList(
                            product, user),
                      );
                },
              );
            },
          );
        },
      );
    });
  }
}
