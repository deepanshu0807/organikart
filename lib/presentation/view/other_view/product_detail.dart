import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:organikart/application/basket_form_bloc/basket_form_bloc.dart';
import 'package:organikart/application/saved_product_form_bloc/saved_product_form_bloc.dart';
import 'package:organikart/application/saved_product_watcher_bloc/saved_product_watcher_bloc.dart';
import 'package:organikart/presentation/util/utility.dart';
import 'package:organikart/presentation/widgets/cached_network_widget.dart';
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:flushbar/flushbar.dart';

class ProductDetail extends StatefulWidget {
  final Content content;

  const ProductDetail({
    Key key,
    @required this.content,
  }) : super(key: key);
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  Widget _appBar() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            onTap: () => Navigator.of(context).pop(),
            child: Container(
              alignment: Alignment.center,
              height: 40.h,
              width: 45.w,
              decoration: BoxDecoration(
                  color: AppColors.getPrimaryColor(),
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300],
                      blurRadius: 10.0,
                    ),
                  ]),
              child: Icon(
                Icons.keyboard_arrow_left_outlined,
                color: Colors.white,
                size: 35.sp,
              ),
            ),
          ),
          SaveUnSaveProductWidget(
            product: widget.content,
          ),
        ],
      ),
    );
  }

  void showFloatingFlushbar(BuildContext context) {
    Flushbar(
      // mainButton: FlatButton(
      //   // color: Colors.white,
      //   padding: EdgeInsets.all(5),
      //   onPressed: () {
      //     Navigator.push(
      //         context,
      //         MaterialPageRoute(
      //             builder: (context) => MainView(
      //                   selectedIndexForBN: 2,
      //                 )));
      //   },
      //   child: Text(
      //     "Cart",
      //     style: TextStyle(color: Colors.white),
      //   ),
      // ),
      margin: EdgeInsets.all(10),
      duration: Duration(seconds: 4),
      padding: EdgeInsets.all(10),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [AppColors.getPrimaryColor(), AppColors.getSecondaryColor()],
        //stops: [0.6, 1],
      ),
      // boxShadows: [
      //   const BoxShadow(
      //       color: Colors.black26,
      //       offset: Offset(3, 3),
      //       blurRadius: 2,
      //       spreadRadius: 4),
      // ],

      dismissDirection: FlushbarDismissDirection.HORIZONTAL,

      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      title: 'Yay! Item added to Cart',
      message: 'Go to checkout page to place order',
    ).show(context);
  }

  Widget _floatingButton() {
    return BlocBuilder<BasketFormBloc, BasketFormState>(
      builder: (context, state) {
        return FloatingActionButton(
          onPressed: () {
            context
                .bloc<BasketFormBloc>()
                .add(BasketFormEvent.addItem(widget.content));
            showFloatingFlushbar(context);
          },
          backgroundColor: Colors.white,
          child: Icon(Icons.shopping_basket, color: Colors.black),
          tooltip: 'Add to Cart',
        );
      },
    );
  }

  Widget _productImage() {
    return Container(
      color: Colors.transparent,
      // margin: EdgeInsets.symmetric(horizontal: 20),
      height: screenHeight(context) / 1.8,
      width: double.infinity,
      // alignment: Alignment.center,
      child: Hero(
        tag: widget.content.picUrl,
        child: ImageDisplayWidgetWithSize(
          // height: screenHeight(context) / 2.2,
          // width: screenWidth(context) / 1.6,
          url: widget.content.picUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _detailWidget() {
    return DraggableScrollableSheet(
        maxChildSize: .8,
        initialChildSize: .53,
        minChildSize: .53,
        builder: (context, scrollController) {
          return Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
                color: Colors.black),
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Center(
                    child: Container(
                      width: 80,
                      height: 6,
                      decoration: BoxDecoration(
                          color: Colors.grey[500],
                          borderRadius: BorderRadius.circular(30)),
                    ),
                  ),
                  verticalSpaceMedium30,
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          widget.content.name,
                          style: text24.copyWith(
                            color: AppColors.getPrimaryColor(),
                            fontSize: 33.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  verticalSpaceMedium20,
                  Row(
                    children: [
                      Text(
                        "₹ ${widget.content.discountedMrp.getOrElse(0)}",
                        style: text22.copyWith(
                            color: Colors.white, fontSize: 30.sp),
                      ),
                      horizontalSpaceSmall,
                      Text("₹ ${widget.content.mrp.getOrElse(0)}",
                          style: text22.copyWith(
                              decoration: TextDecoration.lineThrough,
                              color: Colors.white54,
                              fontSize: 22.sp)),
                    ],
                  ),
                  verticalSpaceMedium15,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                  text:
                                      "${widget.content.discountPercentage.getOrElse(0)}%",
                                  style: text18.copyWith(
                                    color: Colors.black,
                                    fontSize: 24.sp,
                                    fontWeight: FontWeight.bold,
                                  )),
                              TextSpan(
                                text: " Off",
                                style: text14.copyWith(
                                  color: AppColors.getPrimaryColor(),
                                  fontSize: 16.sp,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  verticalSpaceLarge,
                  Text(
                    widget.content.description,
                    style:
                        text14.copyWith(color: Colors.white, fontSize: 20.sp),
                  ),
                  verticalSpaceLarge
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: _floatingButton(),
      body: SafeArea(
        child: Container(
          width: screenWidth(context),
          height: screenHeight(context),
          child: Stack(
            children: <Widget>[_productImage(), _appBar(), _detailWidget()],
          ),
        ),
      ),
    );
  }
}

class SaveUnSaveProductWidget extends StatelessWidget {
  final Content product;
  const SaveUnSaveProductWidget({
    Key key,
    @required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthWatcherBloc, AuthWatcherState>(
        builder: (context, authState) {
      return authState.map(
        initial: (e) => Container(),
        unauthenticated: (e) => Container(),
        authenticated: (e) {
          final user = e.user;
          return BlocBuilder<SavedProductWatcherBloc, SavedProductWatcherState>(
            builder: (context, savedProductState) {
              final isThisProductSaved = savedProductState.map(
                initial: (_) => null,
                loadInProgress: (_) => null,
                loadFailure: (_) => null,
                loadSuccess: (p) {
                  return p.savedProducts.allProducts.firstWhere(
                      (element) =>
                          element.id.getOrElse("dflt") ==
                          product.id.getOrElse("Na"),
                      orElse: () => null);
                },
              );

              return InkWell(
                onTap: () {
                  context.read<SavedProductFormBloc>().add(
                        SavedProductFormEvent.saveOrRemoveProductToMySavedList(
                            product, user),
                      );
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 40.h,
                  width: 45.w,
                  decoration: BoxDecoration(
                      color: isThisProductSaved == null
                          ? Colors.white
                          : Colors.red,
                      borderRadius: BorderRadius.circular(15),
                      // border: Border.all(color: Colors.grey[400]),
                      boxShadow: [
                        BoxShadow(
                          color: isThisProductSaved == null
                              ? Colors.grey[400]
                              : Colors.red[200],
                          blurRadius: 10.0,
                        ),
                      ]),
                  child: Icon(
                    isThisProductSaved == null
                        ? Icons.favorite_border
                        : Icons.favorite,
                    color:
                        isThisProductSaved == null ? Colors.red : Colors.white,
                    size: isThisProductSaved == null ? 25.sp : 30.sp,
                  ),
                ),
              );
            },
          );
        },
      );
    });
  }
}
