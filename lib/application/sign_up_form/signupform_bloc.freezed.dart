// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'signupform_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SignupformEventTearOff {
  const _$SignupformEventTearOff();

// ignore: unused_element
  EvEmailChanged emailChanged(String emailStr) {
    return EvEmailChanged(
      emailStr,
    );
  }

// ignore: unused_element
  _EnameChanged nameChanged(String name) {
    return _EnameChanged(
      name,
    );
  }

// ignore: unused_element
  _EphoneNumberChanged passwordChanged(String password) {
    return _EphoneNumberChanged(
      password,
    );
  }

// ignore: unused_element
  _EsignUp signUp() {
    return const _EsignUp();
  }
}

/// @nodoc
// ignore: unused_element
const $SignupformEvent = _$SignupformEventTearOff();

/// @nodoc
mixin _$SignupformEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult emailChanged(String emailStr),
    @required TResult nameChanged(String name),
    @required TResult passwordChanged(String password),
    @required TResult signUp(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult emailChanged(String emailStr),
    TResult nameChanged(String name),
    TResult passwordChanged(String password),
    TResult signUp(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult emailChanged(EvEmailChanged value),
    @required TResult nameChanged(_EnameChanged value),
    @required TResult passwordChanged(_EphoneNumberChanged value),
    @required TResult signUp(_EsignUp value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult emailChanged(EvEmailChanged value),
    TResult nameChanged(_EnameChanged value),
    TResult passwordChanged(_EphoneNumberChanged value),
    TResult signUp(_EsignUp value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $SignupformEventCopyWith<$Res> {
  factory $SignupformEventCopyWith(
          SignupformEvent value, $Res Function(SignupformEvent) then) =
      _$SignupformEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignupformEventCopyWithImpl<$Res>
    implements $SignupformEventCopyWith<$Res> {
  _$SignupformEventCopyWithImpl(this._value, this._then);

  final SignupformEvent _value;
  // ignore: unused_field
  final $Res Function(SignupformEvent) _then;
}

/// @nodoc
abstract class $EvEmailChangedCopyWith<$Res> {
  factory $EvEmailChangedCopyWith(
          EvEmailChanged value, $Res Function(EvEmailChanged) then) =
      _$EvEmailChangedCopyWithImpl<$Res>;
  $Res call({String emailStr});
}

/// @nodoc
class _$EvEmailChangedCopyWithImpl<$Res>
    extends _$SignupformEventCopyWithImpl<$Res>
    implements $EvEmailChangedCopyWith<$Res> {
  _$EvEmailChangedCopyWithImpl(
      EvEmailChanged _value, $Res Function(EvEmailChanged) _then)
      : super(_value, (v) => _then(v as EvEmailChanged));

  @override
  EvEmailChanged get _value => super._value as EvEmailChanged;

  @override
  $Res call({
    Object emailStr = freezed,
  }) {
    return _then(EvEmailChanged(
      emailStr == freezed ? _value.emailStr : emailStr as String,
    ));
  }
}

/// @nodoc
class _$EvEmailChanged implements EvEmailChanged {
  const _$EvEmailChanged(this.emailStr) : assert(emailStr != null);

  @override
  final String emailStr;

  @override
  String toString() {
    return 'SignupformEvent.emailChanged(emailStr: $emailStr)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is EvEmailChanged &&
            (identical(other.emailStr, emailStr) ||
                const DeepCollectionEquality()
                    .equals(other.emailStr, emailStr)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(emailStr);

  @override
  $EvEmailChangedCopyWith<EvEmailChanged> get copyWith =>
      _$EvEmailChangedCopyWithImpl<EvEmailChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult emailChanged(String emailStr),
    @required TResult nameChanged(String name),
    @required TResult passwordChanged(String password),
    @required TResult signUp(),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return emailChanged(emailStr);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult emailChanged(String emailStr),
    TResult nameChanged(String name),
    TResult passwordChanged(String password),
    TResult signUp(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (emailChanged != null) {
      return emailChanged(emailStr);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult emailChanged(EvEmailChanged value),
    @required TResult nameChanged(_EnameChanged value),
    @required TResult passwordChanged(_EphoneNumberChanged value),
    @required TResult signUp(_EsignUp value),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return emailChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult emailChanged(EvEmailChanged value),
    TResult nameChanged(_EnameChanged value),
    TResult passwordChanged(_EphoneNumberChanged value),
    TResult signUp(_EsignUp value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (emailChanged != null) {
      return emailChanged(this);
    }
    return orElse();
  }
}

abstract class EvEmailChanged implements SignupformEvent {
  const factory EvEmailChanged(String emailStr) = _$EvEmailChanged;

  String get emailStr;
  $EvEmailChangedCopyWith<EvEmailChanged> get copyWith;
}

/// @nodoc
abstract class _$EnameChangedCopyWith<$Res> {
  factory _$EnameChangedCopyWith(
          _EnameChanged value, $Res Function(_EnameChanged) then) =
      __$EnameChangedCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class __$EnameChangedCopyWithImpl<$Res>
    extends _$SignupformEventCopyWithImpl<$Res>
    implements _$EnameChangedCopyWith<$Res> {
  __$EnameChangedCopyWithImpl(
      _EnameChanged _value, $Res Function(_EnameChanged) _then)
      : super(_value, (v) => _then(v as _EnameChanged));

  @override
  _EnameChanged get _value => super._value as _EnameChanged;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_EnameChanged(
      name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
class _$_EnameChanged implements _EnameChanged {
  const _$_EnameChanged(this.name) : assert(name != null);

  @override
  final String name;

  @override
  String toString() {
    return 'SignupformEvent.nameChanged(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EnameChanged &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @override
  _$EnameChangedCopyWith<_EnameChanged> get copyWith =>
      __$EnameChangedCopyWithImpl<_EnameChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult emailChanged(String emailStr),
    @required TResult nameChanged(String name),
    @required TResult passwordChanged(String password),
    @required TResult signUp(),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return nameChanged(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult emailChanged(String emailStr),
    TResult nameChanged(String name),
    TResult passwordChanged(String password),
    TResult signUp(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult emailChanged(EvEmailChanged value),
    @required TResult nameChanged(_EnameChanged value),
    @required TResult passwordChanged(_EphoneNumberChanged value),
    @required TResult signUp(_EsignUp value),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return nameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult emailChanged(EvEmailChanged value),
    TResult nameChanged(_EnameChanged value),
    TResult passwordChanged(_EphoneNumberChanged value),
    TResult signUp(_EsignUp value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(this);
    }
    return orElse();
  }
}

abstract class _EnameChanged implements SignupformEvent {
  const factory _EnameChanged(String name) = _$_EnameChanged;

  String get name;
  _$EnameChangedCopyWith<_EnameChanged> get copyWith;
}

/// @nodoc
abstract class _$EphoneNumberChangedCopyWith<$Res> {
  factory _$EphoneNumberChangedCopyWith(_EphoneNumberChanged value,
          $Res Function(_EphoneNumberChanged) then) =
      __$EphoneNumberChangedCopyWithImpl<$Res>;
  $Res call({String password});
}

/// @nodoc
class __$EphoneNumberChangedCopyWithImpl<$Res>
    extends _$SignupformEventCopyWithImpl<$Res>
    implements _$EphoneNumberChangedCopyWith<$Res> {
  __$EphoneNumberChangedCopyWithImpl(
      _EphoneNumberChanged _value, $Res Function(_EphoneNumberChanged) _then)
      : super(_value, (v) => _then(v as _EphoneNumberChanged));

  @override
  _EphoneNumberChanged get _value => super._value as _EphoneNumberChanged;

  @override
  $Res call({
    Object password = freezed,
  }) {
    return _then(_EphoneNumberChanged(
      password == freezed ? _value.password : password as String,
    ));
  }
}

/// @nodoc
class _$_EphoneNumberChanged implements _EphoneNumberChanged {
  const _$_EphoneNumberChanged(this.password) : assert(password != null);

  @override
  final String password;

  @override
  String toString() {
    return 'SignupformEvent.passwordChanged(password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EphoneNumberChanged &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(password);

  @override
  _$EphoneNumberChangedCopyWith<_EphoneNumberChanged> get copyWith =>
      __$EphoneNumberChangedCopyWithImpl<_EphoneNumberChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult emailChanged(String emailStr),
    @required TResult nameChanged(String name),
    @required TResult passwordChanged(String password),
    @required TResult signUp(),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return passwordChanged(password);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult emailChanged(String emailStr),
    TResult nameChanged(String name),
    TResult passwordChanged(String password),
    TResult signUp(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (passwordChanged != null) {
      return passwordChanged(password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult emailChanged(EvEmailChanged value),
    @required TResult nameChanged(_EnameChanged value),
    @required TResult passwordChanged(_EphoneNumberChanged value),
    @required TResult signUp(_EsignUp value),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return passwordChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult emailChanged(EvEmailChanged value),
    TResult nameChanged(_EnameChanged value),
    TResult passwordChanged(_EphoneNumberChanged value),
    TResult signUp(_EsignUp value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (passwordChanged != null) {
      return passwordChanged(this);
    }
    return orElse();
  }
}

abstract class _EphoneNumberChanged implements SignupformEvent {
  const factory _EphoneNumberChanged(String password) = _$_EphoneNumberChanged;

  String get password;
  _$EphoneNumberChangedCopyWith<_EphoneNumberChanged> get copyWith;
}

/// @nodoc
abstract class _$EsignUpCopyWith<$Res> {
  factory _$EsignUpCopyWith(_EsignUp value, $Res Function(_EsignUp) then) =
      __$EsignUpCopyWithImpl<$Res>;
}

/// @nodoc
class __$EsignUpCopyWithImpl<$Res> extends _$SignupformEventCopyWithImpl<$Res>
    implements _$EsignUpCopyWith<$Res> {
  __$EsignUpCopyWithImpl(_EsignUp _value, $Res Function(_EsignUp) _then)
      : super(_value, (v) => _then(v as _EsignUp));

  @override
  _EsignUp get _value => super._value as _EsignUp;
}

/// @nodoc
class _$_EsignUp implements _EsignUp {
  const _$_EsignUp();

  @override
  String toString() {
    return 'SignupformEvent.signUp()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _EsignUp);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult emailChanged(String emailStr),
    @required TResult nameChanged(String name),
    @required TResult passwordChanged(String password),
    @required TResult signUp(),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return signUp();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult emailChanged(String emailStr),
    TResult nameChanged(String name),
    TResult passwordChanged(String password),
    TResult signUp(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signUp != null) {
      return signUp();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult emailChanged(EvEmailChanged value),
    @required TResult nameChanged(_EnameChanged value),
    @required TResult passwordChanged(_EphoneNumberChanged value),
    @required TResult signUp(_EsignUp value),
  }) {
    assert(emailChanged != null);
    assert(nameChanged != null);
    assert(passwordChanged != null);
    assert(signUp != null);
    return signUp(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult emailChanged(EvEmailChanged value),
    TResult nameChanged(_EnameChanged value),
    TResult passwordChanged(_EphoneNumberChanged value),
    TResult signUp(_EsignUp value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (signUp != null) {
      return signUp(this);
    }
    return orElse();
  }
}

abstract class _EsignUp implements SignupformEvent {
  const factory _EsignUp() = _$_EsignUp;
}

/// @nodoc
class _$SignupformStateTearOff {
  const _$SignupformStateTearOff();

// ignore: unused_element
  _SignupformState call(
      {@required EmailAddress emailAddress,
      @required Name name,
      @required Password password,
      @required bool showErrorMessages,
      @required bool isSubmitting,
      @required Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption}) {
    return _SignupformState(
      emailAddress: emailAddress,
      name: name,
      password: password,
      showErrorMessages: showErrorMessages,
      isSubmitting: isSubmitting,
      authFailureOrSuccessOption: authFailureOrSuccessOption,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SignupformState = _$SignupformStateTearOff();

/// @nodoc
mixin _$SignupformState {
  EmailAddress get emailAddress;
  Name get name;
  Password get password;
  bool get showErrorMessages;
  bool get isSubmitting;
  Option<Either<AuthFailure, Unit>> get authFailureOrSuccessOption;

  $SignupformStateCopyWith<SignupformState> get copyWith;
}

/// @nodoc
abstract class $SignupformStateCopyWith<$Res> {
  factory $SignupformStateCopyWith(
          SignupformState value, $Res Function(SignupformState) then) =
      _$SignupformStateCopyWithImpl<$Res>;
  $Res call(
      {EmailAddress emailAddress,
      Name name,
      Password password,
      bool showErrorMessages,
      bool isSubmitting,
      Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption});
}

/// @nodoc
class _$SignupformStateCopyWithImpl<$Res>
    implements $SignupformStateCopyWith<$Res> {
  _$SignupformStateCopyWithImpl(this._value, this._then);

  final SignupformState _value;
  // ignore: unused_field
  final $Res Function(SignupformState) _then;

  @override
  $Res call({
    Object emailAddress = freezed,
    Object name = freezed,
    Object password = freezed,
    Object showErrorMessages = freezed,
    Object isSubmitting = freezed,
    Object authFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      name: name == freezed ? _value.name : name as Name,
      password: password == freezed ? _value.password : password as Password,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages as bool,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption as Option<Either<AuthFailure, Unit>>,
    ));
  }
}

/// @nodoc
abstract class _$SignupformStateCopyWith<$Res>
    implements $SignupformStateCopyWith<$Res> {
  factory _$SignupformStateCopyWith(
          _SignupformState value, $Res Function(_SignupformState) then) =
      __$SignupformStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {EmailAddress emailAddress,
      Name name,
      Password password,
      bool showErrorMessages,
      bool isSubmitting,
      Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption});
}

/// @nodoc
class __$SignupformStateCopyWithImpl<$Res>
    extends _$SignupformStateCopyWithImpl<$Res>
    implements _$SignupformStateCopyWith<$Res> {
  __$SignupformStateCopyWithImpl(
      _SignupformState _value, $Res Function(_SignupformState) _then)
      : super(_value, (v) => _then(v as _SignupformState));

  @override
  _SignupformState get _value => super._value as _SignupformState;

  @override
  $Res call({
    Object emailAddress = freezed,
    Object name = freezed,
    Object password = freezed,
    Object showErrorMessages = freezed,
    Object isSubmitting = freezed,
    Object authFailureOrSuccessOption = freezed,
  }) {
    return _then(_SignupformState(
      emailAddress: emailAddress == freezed
          ? _value.emailAddress
          : emailAddress as EmailAddress,
      name: name == freezed ? _value.name : name as Name,
      password: password == freezed ? _value.password : password as Password,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages as bool,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption as Option<Either<AuthFailure, Unit>>,
    ));
  }
}

/// @nodoc
class _$_SignupformState implements _SignupformState {
  const _$_SignupformState(
      {@required this.emailAddress,
      @required this.name,
      @required this.password,
      @required this.showErrorMessages,
      @required this.isSubmitting,
      @required this.authFailureOrSuccessOption})
      : assert(emailAddress != null),
        assert(name != null),
        assert(password != null),
        assert(showErrorMessages != null),
        assert(isSubmitting != null),
        assert(authFailureOrSuccessOption != null);

  @override
  final EmailAddress emailAddress;
  @override
  final Name name;
  @override
  final Password password;
  @override
  final bool showErrorMessages;
  @override
  final bool isSubmitting;
  @override
  final Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption;

  @override
  String toString() {
    return 'SignupformState(emailAddress: $emailAddress, name: $name, password: $password, showErrorMessages: $showErrorMessages, isSubmitting: $isSubmitting, authFailureOrSuccessOption: $authFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SignupformState &&
            (identical(other.emailAddress, emailAddress) ||
                const DeepCollectionEquality()
                    .equals(other.emailAddress, emailAddress)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.password, password) ||
                const DeepCollectionEquality()
                    .equals(other.password, password)) &&
            (identical(other.showErrorMessages, showErrorMessages) ||
                const DeepCollectionEquality()
                    .equals(other.showErrorMessages, showErrorMessages)) &&
            (identical(other.isSubmitting, isSubmitting) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitting, isSubmitting)) &&
            (identical(other.authFailureOrSuccessOption,
                    authFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.authFailureOrSuccessOption,
                    authFailureOrSuccessOption)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(emailAddress) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(password) ^
      const DeepCollectionEquality().hash(showErrorMessages) ^
      const DeepCollectionEquality().hash(isSubmitting) ^
      const DeepCollectionEquality().hash(authFailureOrSuccessOption);

  @override
  _$SignupformStateCopyWith<_SignupformState> get copyWith =>
      __$SignupformStateCopyWithImpl<_SignupformState>(this, _$identity);
}

abstract class _SignupformState implements SignupformState {
  const factory _SignupformState(
          {@required
              EmailAddress emailAddress,
          @required
              Name name,
          @required
              Password password,
          @required
              bool showErrorMessages,
          @required
              bool isSubmitting,
          @required
              Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption}) =
      _$_SignupformState;

  @override
  EmailAddress get emailAddress;
  @override
  Name get name;
  @override
  Password get password;
  @override
  bool get showErrorMessages;
  @override
  bool get isSubmitting;
  @override
  Option<Either<AuthFailure, Unit>> get authFailureOrSuccessOption;
  @override
  _$SignupformStateCopyWith<_SignupformState> get copyWith;
}
