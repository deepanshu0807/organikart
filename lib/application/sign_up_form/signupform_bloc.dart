import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:organikart_shared/organikart_shared_package.dart';
import 'package:dartz/dartz.dart';

part 'signupform_event.dart';
part 'signupform_state.dart';
part 'signupform_bloc.freezed.dart';

@injectable
class SignupformBloc extends Bloc<SignupformEvent, SignupformState> {
  final IAuth _iAuth;
  SignupformBloc(this._iAuth) : super(SignupformState.initial());

  @override
  Stream<SignupformState> mapEventToState(
    SignupformEvent event,
  ) async* {
    yield* event.map(
      emailChanged: (e) async* {
        yield state.copyWith(
          emailAddress: EmailAddress(e.emailStr),
          isSubmitting: false,
          authFailureOrSuccessOption: none(),
        );
      },
      nameChanged: (e) async* {
        yield state.copyWith(
          name: Name(e.name),
          isSubmitting: false,
          authFailureOrSuccessOption: none(),
        );
      },
      passwordChanged: (e) async* {
        yield state.copyWith(
          password: Password(e.password),
          isSubmitting: false,
          authFailureOrSuccessOption: none(),
        );
      },
      signUp: (e) async* {
        yield state.copyWith(
          isSubmitting: true,
          showErrorMessages: true,
        );
        if (state.name.isValid() &&
            state.password.isValid() &&
            state.emailAddress.isValid()) {
          final result = await _iAuth.registerUser(
              name: state.name,
              email: state.emailAddress,
              password: state.password);

          yield state.copyWith(
            isSubmitting: false,
            showErrorMessages: true,
            authFailureOrSuccessOption: some(result),
          );
          print("result\n");
          print(result);
        } else {
          yield state.copyWith(
            isSubmitting: false,
            showErrorMessages: true,
            authFailureOrSuccessOption:
                some(left(const AuthFailure.invalidCredential())),
          );
        }
      },
    );
  }
}
