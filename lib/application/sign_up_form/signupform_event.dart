part of 'signupform_bloc.dart';

@freezed
abstract class SignupformEvent with _$SignupformEvent {
  const factory SignupformEvent.emailChanged(String emailStr) = EvEmailChanged;
  const factory SignupformEvent.nameChanged(String name) = _EnameChanged;
  const factory SignupformEvent.passwordChanged(String password) =
      _EphoneNumberChanged;
  const factory SignupformEvent.signUp() = _EsignUp;
}
