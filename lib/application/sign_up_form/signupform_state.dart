part of 'signupform_bloc.dart';

@freezed
abstract class SignupformState with _$SignupformState {
  const factory SignupformState({
    @required EmailAddress emailAddress,
    @required Name name,
    @required Password password,
    @required bool showErrorMessages,
    @required bool isSubmitting,
    @required Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption,
  }) = _SignupformState;

  factory SignupformState.initial() => SignupformState(
        emailAddress: EmailAddress(''),
        name: Name(''),
        password: Password(""),
        showErrorMessages: false,
        isSubmitting: false,
        authFailureOrSuccessOption: none(),
      );
}
