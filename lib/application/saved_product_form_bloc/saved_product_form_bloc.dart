import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:organikart_shared/domain/features/i_customer_repo.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'saved_product_form_event.dart';
part 'saved_product_form_state.dart';
part 'saved_product_form_bloc.freezed.dart';

@injectable
class SavedProductFormBloc
    extends Bloc<SavedProductFormEvent, SavedProductFormState> {
  final ICustomerRepo _iCustomerRepo;
  SavedProductFormBloc(this._iCustomerRepo)
      : super(SavedProductFormState.initial());

  @override
  Stream<SavedProductFormState> mapEventToState(
    SavedProductFormEvent event,
  ) async* {
    yield* event.map(
      saveOrRemoveProductToMySavedList: (e) async* {
        yield state.copyWith(
          isSaving: true,
          saveFailureOrSuccessOption: none(),
        );
        final result = await _iCustomerRepo.saveOrRemoveProductToMySavedList(
            e.user, e.product);
        yield state.copyWith(
            isSaving: false, saveFailureOrSuccessOption: some(result));
      },
    );
  }
}
