part of 'saved_product_form_bloc.dart';

@freezed
abstract class SavedProductFormState with _$SavedProductFormState {
  const factory SavedProductFormState({
    @required bool isSaving,
    @required Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
  }) = _SavedProductFormState;

  factory SavedProductFormState.initial() => SavedProductFormState(
        isSaving: false,
        saveFailureOrSuccessOption: none(),
      );
}
