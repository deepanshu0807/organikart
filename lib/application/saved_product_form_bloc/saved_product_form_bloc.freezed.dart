// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'saved_product_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SavedProductFormEventTearOff {
  const _$SavedProductFormEventTearOff();

// ignore: unused_element
  _SaveOrRemoveProductToMySavedList saveOrRemoveProductToMySavedList(
      Content product, OrganikartUser user) {
    return _SaveOrRemoveProductToMySavedList(
      product,
      user,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SavedProductFormEvent = _$SavedProductFormEventTearOff();

/// @nodoc
mixin _$SavedProductFormEvent {
  Content get product;
  OrganikartUser get user;

  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult saveOrRemoveProductToMySavedList(
            Content product, OrganikartUser user),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult saveOrRemoveProductToMySavedList(
        Content product, OrganikartUser user),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required
        TResult saveOrRemoveProductToMySavedList(
            _SaveOrRemoveProductToMySavedList value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult saveOrRemoveProductToMySavedList(
        _SaveOrRemoveProductToMySavedList value),
    @required TResult orElse(),
  });

  $SavedProductFormEventCopyWith<SavedProductFormEvent> get copyWith;
}

/// @nodoc
abstract class $SavedProductFormEventCopyWith<$Res> {
  factory $SavedProductFormEventCopyWith(SavedProductFormEvent value,
          $Res Function(SavedProductFormEvent) then) =
      _$SavedProductFormEventCopyWithImpl<$Res>;
  $Res call({Content product, OrganikartUser user});

  $ContentCopyWith<$Res> get product;
  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class _$SavedProductFormEventCopyWithImpl<$Res>
    implements $SavedProductFormEventCopyWith<$Res> {
  _$SavedProductFormEventCopyWithImpl(this._value, this._then);

  final SavedProductFormEvent _value;
  // ignore: unused_field
  final $Res Function(SavedProductFormEvent) _then;

  @override
  $Res call({
    Object product = freezed,
    Object user = freezed,
  }) {
    return _then(_value.copyWith(
      product: product == freezed ? _value.product : product as Content,
      user: user == freezed ? _value.user : user as OrganikartUser,
    ));
  }

  @override
  $ContentCopyWith<$Res> get product {
    if (_value.product == null) {
      return null;
    }
    return $ContentCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }

  @override
  $OrganikartUserCopyWith<$Res> get user {
    if (_value.user == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc
abstract class _$SaveOrRemoveProductToMySavedListCopyWith<$Res>
    implements $SavedProductFormEventCopyWith<$Res> {
  factory _$SaveOrRemoveProductToMySavedListCopyWith(
          _SaveOrRemoveProductToMySavedList value,
          $Res Function(_SaveOrRemoveProductToMySavedList) then) =
      __$SaveOrRemoveProductToMySavedListCopyWithImpl<$Res>;
  @override
  $Res call({Content product, OrganikartUser user});

  @override
  $ContentCopyWith<$Res> get product;
  @override
  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class __$SaveOrRemoveProductToMySavedListCopyWithImpl<$Res>
    extends _$SavedProductFormEventCopyWithImpl<$Res>
    implements _$SaveOrRemoveProductToMySavedListCopyWith<$Res> {
  __$SaveOrRemoveProductToMySavedListCopyWithImpl(
      _SaveOrRemoveProductToMySavedList _value,
      $Res Function(_SaveOrRemoveProductToMySavedList) _then)
      : super(_value, (v) => _then(v as _SaveOrRemoveProductToMySavedList));

  @override
  _SaveOrRemoveProductToMySavedList get _value =>
      super._value as _SaveOrRemoveProductToMySavedList;

  @override
  $Res call({
    Object product = freezed,
    Object user = freezed,
  }) {
    return _then(_SaveOrRemoveProductToMySavedList(
      product == freezed ? _value.product : product as Content,
      user == freezed ? _value.user : user as OrganikartUser,
    ));
  }
}

/// @nodoc
class _$_SaveOrRemoveProductToMySavedList
    implements _SaveOrRemoveProductToMySavedList {
  const _$_SaveOrRemoveProductToMySavedList(this.product, this.user)
      : assert(product != null),
        assert(user != null);

  @override
  final Content product;
  @override
  final OrganikartUser user;

  @override
  String toString() {
    return 'SavedProductFormEvent.saveOrRemoveProductToMySavedList(product: $product, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SaveOrRemoveProductToMySavedList &&
            (identical(other.product, product) ||
                const DeepCollectionEquality()
                    .equals(other.product, product)) &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(product) ^
      const DeepCollectionEquality().hash(user);

  @override
  _$SaveOrRemoveProductToMySavedListCopyWith<_SaveOrRemoveProductToMySavedList>
      get copyWith => __$SaveOrRemoveProductToMySavedListCopyWithImpl<
          _SaveOrRemoveProductToMySavedList>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required
        TResult saveOrRemoveProductToMySavedList(
            Content product, OrganikartUser user),
  }) {
    assert(saveOrRemoveProductToMySavedList != null);
    return saveOrRemoveProductToMySavedList(product, user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult saveOrRemoveProductToMySavedList(
        Content product, OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (saveOrRemoveProductToMySavedList != null) {
      return saveOrRemoveProductToMySavedList(product, user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required
        TResult saveOrRemoveProductToMySavedList(
            _SaveOrRemoveProductToMySavedList value),
  }) {
    assert(saveOrRemoveProductToMySavedList != null);
    return saveOrRemoveProductToMySavedList(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult saveOrRemoveProductToMySavedList(
        _SaveOrRemoveProductToMySavedList value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (saveOrRemoveProductToMySavedList != null) {
      return saveOrRemoveProductToMySavedList(this);
    }
    return orElse();
  }
}

abstract class _SaveOrRemoveProductToMySavedList
    implements SavedProductFormEvent {
  const factory _SaveOrRemoveProductToMySavedList(
          Content product, OrganikartUser user) =
      _$_SaveOrRemoveProductToMySavedList;

  @override
  Content get product;
  @override
  OrganikartUser get user;
  @override
  _$SaveOrRemoveProductToMySavedListCopyWith<_SaveOrRemoveProductToMySavedList>
      get copyWith;
}

/// @nodoc
class _$SavedProductFormStateTearOff {
  const _$SavedProductFormStateTearOff();

// ignore: unused_element
  _SavedProductFormState call(
      {@required
          bool isSaving,
      @required
          Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption}) {
    return _SavedProductFormState(
      isSaving: isSaving,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SavedProductFormState = _$SavedProductFormStateTearOff();

/// @nodoc
mixin _$SavedProductFormState {
  bool get isSaving;
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;

  $SavedProductFormStateCopyWith<SavedProductFormState> get copyWith;
}

/// @nodoc
abstract class $SavedProductFormStateCopyWith<$Res> {
  factory $SavedProductFormStateCopyWith(SavedProductFormState value,
          $Res Function(SavedProductFormState) then) =
      _$SavedProductFormStateCopyWithImpl<$Res>;
  $Res call(
      {bool isSaving,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption});
}

/// @nodoc
class _$SavedProductFormStateCopyWithImpl<$Res>
    implements $SavedProductFormStateCopyWith<$Res> {
  _$SavedProductFormStateCopyWithImpl(this._value, this._then);

  final SavedProductFormState _value;
  // ignore: unused_field
  final $Res Function(SavedProductFormState) _then;

  @override
  $Res call({
    Object isSaving = freezed,
    Object saveFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      isSaving: isSaving == freezed ? _value.isSaving : isSaving as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
    ));
  }
}

/// @nodoc
abstract class _$SavedProductFormStateCopyWith<$Res>
    implements $SavedProductFormStateCopyWith<$Res> {
  factory _$SavedProductFormStateCopyWith(_SavedProductFormState value,
          $Res Function(_SavedProductFormState) then) =
      __$SavedProductFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool isSaving,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption});
}

/// @nodoc
class __$SavedProductFormStateCopyWithImpl<$Res>
    extends _$SavedProductFormStateCopyWithImpl<$Res>
    implements _$SavedProductFormStateCopyWith<$Res> {
  __$SavedProductFormStateCopyWithImpl(_SavedProductFormState _value,
      $Res Function(_SavedProductFormState) _then)
      : super(_value, (v) => _then(v as _SavedProductFormState));

  @override
  _SavedProductFormState get _value => super._value as _SavedProductFormState;

  @override
  $Res call({
    Object isSaving = freezed,
    Object saveFailureOrSuccessOption = freezed,
  }) {
    return _then(_SavedProductFormState(
      isSaving: isSaving == freezed ? _value.isSaving : isSaving as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
    ));
  }
}

/// @nodoc
class _$_SavedProductFormState implements _SavedProductFormState {
  const _$_SavedProductFormState(
      {@required this.isSaving, @required this.saveFailureOrSuccessOption})
      : assert(isSaving != null),
        assert(saveFailureOrSuccessOption != null);

  @override
  final bool isSaving;
  @override
  final Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption;

  @override
  String toString() {
    return 'SavedProductFormState(isSaving: $isSaving, saveFailureOrSuccessOption: $saveFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SavedProductFormState &&
            (identical(other.isSaving, isSaving) ||
                const DeepCollectionEquality()
                    .equals(other.isSaving, isSaving)) &&
            (identical(other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(isSaving) ^
      const DeepCollectionEquality().hash(saveFailureOrSuccessOption);

  @override
  _$SavedProductFormStateCopyWith<_SavedProductFormState> get copyWith =>
      __$SavedProductFormStateCopyWithImpl<_SavedProductFormState>(
          this, _$identity);
}

abstract class _SavedProductFormState implements SavedProductFormState {
  const factory _SavedProductFormState(
          {@required
              bool isSaving,
          @required
              Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption}) =
      _$_SavedProductFormState;

  @override
  bool get isSaving;
  @override
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  @override
  _$SavedProductFormStateCopyWith<_SavedProductFormState> get copyWith;
}
