part of 'saved_product_form_bloc.dart';

@freezed
abstract class SavedProductFormEvent with _$SavedProductFormEvent {
  const factory SavedProductFormEvent.saveOrRemoveProductToMySavedList(
      Content product, OrganikartUser user) = _SaveOrRemoveProductToMySavedList;
}
