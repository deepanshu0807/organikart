part of 'customer_details_form_bloc.dart';

@freezed
abstract class CustomerDetailsFormState with _$CustomerDetailsFormState {
  const factory CustomerDetailsFormState({
    @required OrganikartUser user,
    @required bool showErrorMessages,
    @required bool isSubmitting,
    @required Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
  }) = _CustomerDetailsFormState;

  factory CustomerDetailsFormState.initial() => CustomerDetailsFormState(
        user: OrganikartUser(
          uId: UniqueId(),
          name: Name(""),
          emailAddress: EmailAddress(""),
          phoneNumber: PhoneNumber(""),
          role: const UserRole.customer(),
          lastSignInDateTime: DateTime.now(),
        ),
        showErrorMessages: false,
        isSubmitting: false,
        saveFailureOrSuccessOption: none(),
      );
}
