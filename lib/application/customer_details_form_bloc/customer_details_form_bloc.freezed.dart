// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'customer_details_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$CustomerDetailsFormEvTearOff {
  const _$CustomerDetailsFormEvTearOff();

// ignore: unused_element
  _NameChanged nameChanged(String name) {
    return _NameChanged(
      name,
    );
  }

// ignore: unused_element
  _EmailChanged emailChanged(String email) {
    return _EmailChanged(
      email,
    );
  }

// ignore: unused_element
  _PhoneNumberChanged phoneNumberChanged(String phone) {
    return _PhoneNumberChanged(
      phone,
    );
  }

// ignore: unused_element
  _CustomerDetailsFormSaChanges saveChanges() {
    return const _CustomerDetailsFormSaChanges();
  }

// ignore: unused_element
  _InitializeUser initializeUser(OrganikartUser user) {
    return _InitializeUser(
      user,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CustomerDetailsFormEv = _$CustomerDetailsFormEvTearOff();

/// @nodoc
mixin _$CustomerDetailsFormEv {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult emailChanged(String email),
    @required TResult phoneNumberChanged(String phone),
    @required TResult saveChanges(),
    @required TResult initializeUser(OrganikartUser user),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult emailChanged(String email),
    TResult phoneNumberChanged(String phone),
    TResult saveChanges(),
    TResult initializeUser(OrganikartUser user),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult emailChanged(_EmailChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult saveChanges(_CustomerDetailsFormSaChanges value),
    @required TResult initializeUser(_InitializeUser value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult emailChanged(_EmailChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult saveChanges(_CustomerDetailsFormSaChanges value),
    TResult initializeUser(_InitializeUser value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $CustomerDetailsFormEvCopyWith<$Res> {
  factory $CustomerDetailsFormEvCopyWith(CustomerDetailsFormEv value,
          $Res Function(CustomerDetailsFormEv) then) =
      _$CustomerDetailsFormEvCopyWithImpl<$Res>;
}

/// @nodoc
class _$CustomerDetailsFormEvCopyWithImpl<$Res>
    implements $CustomerDetailsFormEvCopyWith<$Res> {
  _$CustomerDetailsFormEvCopyWithImpl(this._value, this._then);

  final CustomerDetailsFormEv _value;
  // ignore: unused_field
  final $Res Function(CustomerDetailsFormEv) _then;
}

/// @nodoc
abstract class _$NameChangedCopyWith<$Res> {
  factory _$NameChangedCopyWith(
          _NameChanged value, $Res Function(_NameChanged) then) =
      __$NameChangedCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class __$NameChangedCopyWithImpl<$Res>
    extends _$CustomerDetailsFormEvCopyWithImpl<$Res>
    implements _$NameChangedCopyWith<$Res> {
  __$NameChangedCopyWithImpl(
      _NameChanged _value, $Res Function(_NameChanged) _then)
      : super(_value, (v) => _then(v as _NameChanged));

  @override
  _NameChanged get _value => super._value as _NameChanged;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_NameChanged(
      name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
class _$_NameChanged implements _NameChanged {
  const _$_NameChanged(this.name) : assert(name != null);

  @override
  final String name;

  @override
  String toString() {
    return 'CustomerDetailsFormEv.nameChanged(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NameChanged &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @override
  _$NameChangedCopyWith<_NameChanged> get copyWith =>
      __$NameChangedCopyWithImpl<_NameChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult emailChanged(String email),
    @required TResult phoneNumberChanged(String phone),
    @required TResult saveChanges(),
    @required TResult initializeUser(OrganikartUser user),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return nameChanged(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult emailChanged(String email),
    TResult phoneNumberChanged(String phone),
    TResult saveChanges(),
    TResult initializeUser(OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult emailChanged(_EmailChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult saveChanges(_CustomerDetailsFormSaChanges value),
    @required TResult initializeUser(_InitializeUser value),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return nameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult emailChanged(_EmailChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult saveChanges(_CustomerDetailsFormSaChanges value),
    TResult initializeUser(_InitializeUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(this);
    }
    return orElse();
  }
}

abstract class _NameChanged implements CustomerDetailsFormEv {
  const factory _NameChanged(String name) = _$_NameChanged;

  String get name;
  _$NameChangedCopyWith<_NameChanged> get copyWith;
}

/// @nodoc
abstract class _$EmailChangedCopyWith<$Res> {
  factory _$EmailChangedCopyWith(
          _EmailChanged value, $Res Function(_EmailChanged) then) =
      __$EmailChangedCopyWithImpl<$Res>;
  $Res call({String email});
}

/// @nodoc
class __$EmailChangedCopyWithImpl<$Res>
    extends _$CustomerDetailsFormEvCopyWithImpl<$Res>
    implements _$EmailChangedCopyWith<$Res> {
  __$EmailChangedCopyWithImpl(
      _EmailChanged _value, $Res Function(_EmailChanged) _then)
      : super(_value, (v) => _then(v as _EmailChanged));

  @override
  _EmailChanged get _value => super._value as _EmailChanged;

  @override
  $Res call({
    Object email = freezed,
  }) {
    return _then(_EmailChanged(
      email == freezed ? _value.email : email as String,
    ));
  }
}

/// @nodoc
class _$_EmailChanged implements _EmailChanged {
  const _$_EmailChanged(this.email) : assert(email != null);

  @override
  final String email;

  @override
  String toString() {
    return 'CustomerDetailsFormEv.emailChanged(email: $email)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _EmailChanged &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(email);

  @override
  _$EmailChangedCopyWith<_EmailChanged> get copyWith =>
      __$EmailChangedCopyWithImpl<_EmailChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult emailChanged(String email),
    @required TResult phoneNumberChanged(String phone),
    @required TResult saveChanges(),
    @required TResult initializeUser(OrganikartUser user),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return emailChanged(email);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult emailChanged(String email),
    TResult phoneNumberChanged(String phone),
    TResult saveChanges(),
    TResult initializeUser(OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (emailChanged != null) {
      return emailChanged(email);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult emailChanged(_EmailChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult saveChanges(_CustomerDetailsFormSaChanges value),
    @required TResult initializeUser(_InitializeUser value),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return emailChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult emailChanged(_EmailChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult saveChanges(_CustomerDetailsFormSaChanges value),
    TResult initializeUser(_InitializeUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (emailChanged != null) {
      return emailChanged(this);
    }
    return orElse();
  }
}

abstract class _EmailChanged implements CustomerDetailsFormEv {
  const factory _EmailChanged(String email) = _$_EmailChanged;

  String get email;
  _$EmailChangedCopyWith<_EmailChanged> get copyWith;
}

/// @nodoc
abstract class _$PhoneNumberChangedCopyWith<$Res> {
  factory _$PhoneNumberChangedCopyWith(
          _PhoneNumberChanged value, $Res Function(_PhoneNumberChanged) then) =
      __$PhoneNumberChangedCopyWithImpl<$Res>;
  $Res call({String phone});
}

/// @nodoc
class __$PhoneNumberChangedCopyWithImpl<$Res>
    extends _$CustomerDetailsFormEvCopyWithImpl<$Res>
    implements _$PhoneNumberChangedCopyWith<$Res> {
  __$PhoneNumberChangedCopyWithImpl(
      _PhoneNumberChanged _value, $Res Function(_PhoneNumberChanged) _then)
      : super(_value, (v) => _then(v as _PhoneNumberChanged));

  @override
  _PhoneNumberChanged get _value => super._value as _PhoneNumberChanged;

  @override
  $Res call({
    Object phone = freezed,
  }) {
    return _then(_PhoneNumberChanged(
      phone == freezed ? _value.phone : phone as String,
    ));
  }
}

/// @nodoc
class _$_PhoneNumberChanged implements _PhoneNumberChanged {
  const _$_PhoneNumberChanged(this.phone) : assert(phone != null);

  @override
  final String phone;

  @override
  String toString() {
    return 'CustomerDetailsFormEv.phoneNumberChanged(phone: $phone)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PhoneNumberChanged &&
            (identical(other.phone, phone) ||
                const DeepCollectionEquality().equals(other.phone, phone)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(phone);

  @override
  _$PhoneNumberChangedCopyWith<_PhoneNumberChanged> get copyWith =>
      __$PhoneNumberChangedCopyWithImpl<_PhoneNumberChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult emailChanged(String email),
    @required TResult phoneNumberChanged(String phone),
    @required TResult saveChanges(),
    @required TResult initializeUser(OrganikartUser user),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return phoneNumberChanged(phone);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult emailChanged(String email),
    TResult phoneNumberChanged(String phone),
    TResult saveChanges(),
    TResult initializeUser(OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberChanged != null) {
      return phoneNumberChanged(phone);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult emailChanged(_EmailChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult saveChanges(_CustomerDetailsFormSaChanges value),
    @required TResult initializeUser(_InitializeUser value),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return phoneNumberChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult emailChanged(_EmailChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult saveChanges(_CustomerDetailsFormSaChanges value),
    TResult initializeUser(_InitializeUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberChanged != null) {
      return phoneNumberChanged(this);
    }
    return orElse();
  }
}

abstract class _PhoneNumberChanged implements CustomerDetailsFormEv {
  const factory _PhoneNumberChanged(String phone) = _$_PhoneNumberChanged;

  String get phone;
  _$PhoneNumberChangedCopyWith<_PhoneNumberChanged> get copyWith;
}

/// @nodoc
abstract class _$CustomerDetailsFormSaChangesCopyWith<$Res> {
  factory _$CustomerDetailsFormSaChangesCopyWith(
          _CustomerDetailsFormSaChanges value,
          $Res Function(_CustomerDetailsFormSaChanges) then) =
      __$CustomerDetailsFormSaChangesCopyWithImpl<$Res>;
}

/// @nodoc
class __$CustomerDetailsFormSaChangesCopyWithImpl<$Res>
    extends _$CustomerDetailsFormEvCopyWithImpl<$Res>
    implements _$CustomerDetailsFormSaChangesCopyWith<$Res> {
  __$CustomerDetailsFormSaChangesCopyWithImpl(
      _CustomerDetailsFormSaChanges _value,
      $Res Function(_CustomerDetailsFormSaChanges) _then)
      : super(_value, (v) => _then(v as _CustomerDetailsFormSaChanges));

  @override
  _CustomerDetailsFormSaChanges get _value =>
      super._value as _CustomerDetailsFormSaChanges;
}

/// @nodoc
class _$_CustomerDetailsFormSaChanges implements _CustomerDetailsFormSaChanges {
  const _$_CustomerDetailsFormSaChanges();

  @override
  String toString() {
    return 'CustomerDetailsFormEv.saveChanges()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _CustomerDetailsFormSaChanges);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult emailChanged(String email),
    @required TResult phoneNumberChanged(String phone),
    @required TResult saveChanges(),
    @required TResult initializeUser(OrganikartUser user),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return saveChanges();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult emailChanged(String email),
    TResult phoneNumberChanged(String phone),
    TResult saveChanges(),
    TResult initializeUser(OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (saveChanges != null) {
      return saveChanges();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult emailChanged(_EmailChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult saveChanges(_CustomerDetailsFormSaChanges value),
    @required TResult initializeUser(_InitializeUser value),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return saveChanges(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult emailChanged(_EmailChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult saveChanges(_CustomerDetailsFormSaChanges value),
    TResult initializeUser(_InitializeUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (saveChanges != null) {
      return saveChanges(this);
    }
    return orElse();
  }
}

abstract class _CustomerDetailsFormSaChanges implements CustomerDetailsFormEv {
  const factory _CustomerDetailsFormSaChanges() =
      _$_CustomerDetailsFormSaChanges;
}

/// @nodoc
abstract class _$InitializeUserCopyWith<$Res> {
  factory _$InitializeUserCopyWith(
          _InitializeUser value, $Res Function(_InitializeUser) then) =
      __$InitializeUserCopyWithImpl<$Res>;
  $Res call({OrganikartUser user});

  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class __$InitializeUserCopyWithImpl<$Res>
    extends _$CustomerDetailsFormEvCopyWithImpl<$Res>
    implements _$InitializeUserCopyWith<$Res> {
  __$InitializeUserCopyWithImpl(
      _InitializeUser _value, $Res Function(_InitializeUser) _then)
      : super(_value, (v) => _then(v as _InitializeUser));

  @override
  _InitializeUser get _value => super._value as _InitializeUser;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(_InitializeUser(
      user == freezed ? _value.user : user as OrganikartUser,
    ));
  }

  @override
  $OrganikartUserCopyWith<$Res> get user {
    if (_value.user == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc
class _$_InitializeUser implements _InitializeUser {
  const _$_InitializeUser(this.user) : assert(user != null);

  @override
  final OrganikartUser user;

  @override
  String toString() {
    return 'CustomerDetailsFormEv.initializeUser(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _InitializeUser &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @override
  _$InitializeUserCopyWith<_InitializeUser> get copyWith =>
      __$InitializeUserCopyWithImpl<_InitializeUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult emailChanged(String email),
    @required TResult phoneNumberChanged(String phone),
    @required TResult saveChanges(),
    @required TResult initializeUser(OrganikartUser user),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return initializeUser(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult emailChanged(String email),
    TResult phoneNumberChanged(String phone),
    TResult saveChanges(),
    TResult initializeUser(OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initializeUser != null) {
      return initializeUser(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult emailChanged(_EmailChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult saveChanges(_CustomerDetailsFormSaChanges value),
    @required TResult initializeUser(_InitializeUser value),
  }) {
    assert(nameChanged != null);
    assert(emailChanged != null);
    assert(phoneNumberChanged != null);
    assert(saveChanges != null);
    assert(initializeUser != null);
    return initializeUser(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult emailChanged(_EmailChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult saveChanges(_CustomerDetailsFormSaChanges value),
    TResult initializeUser(_InitializeUser value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initializeUser != null) {
      return initializeUser(this);
    }
    return orElse();
  }
}

abstract class _InitializeUser implements CustomerDetailsFormEv {
  const factory _InitializeUser(OrganikartUser user) = _$_InitializeUser;

  OrganikartUser get user;
  _$InitializeUserCopyWith<_InitializeUser> get copyWith;
}

/// @nodoc
class _$CustomerDetailsFormStateTearOff {
  const _$CustomerDetailsFormStateTearOff();

// ignore: unused_element
  _CustomerDetailsFormState call(
      {@required
          OrganikartUser user,
      @required
          bool showErrorMessages,
      @required
          bool isSubmitting,
      @required
          Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption}) {
    return _CustomerDetailsFormState(
      user: user,
      showErrorMessages: showErrorMessages,
      isSubmitting: isSubmitting,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CustomerDetailsFormState = _$CustomerDetailsFormStateTearOff();

/// @nodoc
mixin _$CustomerDetailsFormState {
  OrganikartUser get user;
  bool get showErrorMessages;
  bool get isSubmitting;
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;

  $CustomerDetailsFormStateCopyWith<CustomerDetailsFormState> get copyWith;
}

/// @nodoc
abstract class $CustomerDetailsFormStateCopyWith<$Res> {
  factory $CustomerDetailsFormStateCopyWith(CustomerDetailsFormState value,
          $Res Function(CustomerDetailsFormState) then) =
      _$CustomerDetailsFormStateCopyWithImpl<$Res>;
  $Res call(
      {OrganikartUser user,
      bool showErrorMessages,
      bool isSubmitting,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption});

  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class _$CustomerDetailsFormStateCopyWithImpl<$Res>
    implements $CustomerDetailsFormStateCopyWith<$Res> {
  _$CustomerDetailsFormStateCopyWithImpl(this._value, this._then);

  final CustomerDetailsFormState _value;
  // ignore: unused_field
  final $Res Function(CustomerDetailsFormState) _then;

  @override
  $Res call({
    Object user = freezed,
    Object showErrorMessages = freezed,
    Object isSubmitting = freezed,
    Object saveFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      user: user == freezed ? _value.user : user as OrganikartUser,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages as bool,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
    ));
  }

  @override
  $OrganikartUserCopyWith<$Res> get user {
    if (_value.user == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc
abstract class _$CustomerDetailsFormStateCopyWith<$Res>
    implements $CustomerDetailsFormStateCopyWith<$Res> {
  factory _$CustomerDetailsFormStateCopyWith(_CustomerDetailsFormState value,
          $Res Function(_CustomerDetailsFormState) then) =
      __$CustomerDetailsFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {OrganikartUser user,
      bool showErrorMessages,
      bool isSubmitting,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption});

  @override
  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class __$CustomerDetailsFormStateCopyWithImpl<$Res>
    extends _$CustomerDetailsFormStateCopyWithImpl<$Res>
    implements _$CustomerDetailsFormStateCopyWith<$Res> {
  __$CustomerDetailsFormStateCopyWithImpl(_CustomerDetailsFormState _value,
      $Res Function(_CustomerDetailsFormState) _then)
      : super(_value, (v) => _then(v as _CustomerDetailsFormState));

  @override
  _CustomerDetailsFormState get _value =>
      super._value as _CustomerDetailsFormState;

  @override
  $Res call({
    Object user = freezed,
    Object showErrorMessages = freezed,
    Object isSubmitting = freezed,
    Object saveFailureOrSuccessOption = freezed,
  }) {
    return _then(_CustomerDetailsFormState(
      user: user == freezed ? _value.user : user as OrganikartUser,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages as bool,
      isSubmitting:
          isSubmitting == freezed ? _value.isSubmitting : isSubmitting as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
    ));
  }
}

/// @nodoc
class _$_CustomerDetailsFormState implements _CustomerDetailsFormState {
  const _$_CustomerDetailsFormState(
      {@required this.user,
      @required this.showErrorMessages,
      @required this.isSubmitting,
      @required this.saveFailureOrSuccessOption})
      : assert(user != null),
        assert(showErrorMessages != null),
        assert(isSubmitting != null),
        assert(saveFailureOrSuccessOption != null);

  @override
  final OrganikartUser user;
  @override
  final bool showErrorMessages;
  @override
  final bool isSubmitting;
  @override
  final Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption;

  @override
  String toString() {
    return 'CustomerDetailsFormState(user: $user, showErrorMessages: $showErrorMessages, isSubmitting: $isSubmitting, saveFailureOrSuccessOption: $saveFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CustomerDetailsFormState &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)) &&
            (identical(other.showErrorMessages, showErrorMessages) ||
                const DeepCollectionEquality()
                    .equals(other.showErrorMessages, showErrorMessages)) &&
            (identical(other.isSubmitting, isSubmitting) ||
                const DeepCollectionEquality()
                    .equals(other.isSubmitting, isSubmitting)) &&
            (identical(other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(user) ^
      const DeepCollectionEquality().hash(showErrorMessages) ^
      const DeepCollectionEquality().hash(isSubmitting) ^
      const DeepCollectionEquality().hash(saveFailureOrSuccessOption);

  @override
  _$CustomerDetailsFormStateCopyWith<_CustomerDetailsFormState> get copyWith =>
      __$CustomerDetailsFormStateCopyWithImpl<_CustomerDetailsFormState>(
          this, _$identity);
}

abstract class _CustomerDetailsFormState implements CustomerDetailsFormState {
  const factory _CustomerDetailsFormState(
          {@required
              OrganikartUser user,
          @required
              bool showErrorMessages,
          @required
              bool isSubmitting,
          @required
              Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption}) =
      _$_CustomerDetailsFormState;

  @override
  OrganikartUser get user;
  @override
  bool get showErrorMessages;
  @override
  bool get isSubmitting;
  @override
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  @override
  _$CustomerDetailsFormStateCopyWith<_CustomerDetailsFormState> get copyWith;
}
