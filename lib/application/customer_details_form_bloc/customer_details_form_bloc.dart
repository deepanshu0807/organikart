import 'dart:async';

import 'package:dartz/dartz.dart';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:organikart_shared/domain/features/i_customer_repo.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'customer_details_form_event.dart';
part 'customer_details_form_state.dart';
part 'customer_details_form_bloc.freezed.dart';

@lazySingleton
class CustomerDetailsFormBloc
    extends Bloc<CustomerDetailsFormEv, CustomerDetailsFormState> {
  final ICustomerRepo _iCustomerRepo;

  CustomerDetailsFormBloc(this._iCustomerRepo)
      : super(CustomerDetailsFormState.initial());

  @override
  Stream<CustomerDetailsFormState> mapEventToState(
    CustomerDetailsFormEv event,
  ) async* {
    yield* event.map(
      initializeUser: _initializeUser,
      nameChanged: _nameChanged,
      emailChanged: _emailChanged,
      phoneNumberChanged: _phoneNumberChanged,
      saveChanges: _saveChanges,
    );
  }

  Stream<CustomerDetailsFormState> _nameChanged(_NameChanged value) async* {
    yield state.copyWith(
      saveFailureOrSuccessOption: none(),
      user: state.user.copyWith(
        name: Name(value.name),
      ),
    );
  }

  Stream<CustomerDetailsFormState> _emailChanged(_EmailChanged value) async* {
    yield state.copyWith(
      saveFailureOrSuccessOption: none(),
      user: state.user.copyWith(
        emailAddress: EmailAddress(value.email),
      ),
    );
  }

  Stream<CustomerDetailsFormState> _phoneNumberChanged(
      _PhoneNumberChanged value) async* {
    yield state.copyWith(
      saveFailureOrSuccessOption: none(),
      user: state.user.copyWith(
        phoneNumber: PhoneNumber(value.phone),
      ),
    );
  }

  Stream<CustomerDetailsFormState> _saveChanges(
      _CustomerDetailsFormSaChanges value) async* {
    print("Saving details for register wale part pe hu bhai");
    yield state.copyWith(
      isSubmitting: true,
      saveFailureOrSuccessOption: none(),
    );
    final result = await _iCustomerRepo.create(state.user);

    yield state.copyWith(
      isSubmitting: false,
      saveFailureOrSuccessOption: some(result),
    );
    result.fold(
      (l) => print("failed"),
      (r) => print("success"),
    );
  }

  Stream<CustomerDetailsFormState> _initializeUser(
      _InitializeUser value) async* {
    yield state.copyWith(
      user: value.user,
      saveFailureOrSuccessOption: none(),
    );
  }
}
