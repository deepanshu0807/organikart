part of 'customer_details_form_bloc.dart';

@freezed
abstract class CustomerDetailsFormEv with _$CustomerDetailsFormEv {
  const factory CustomerDetailsFormEv.nameChanged(String name) = _NameChanged;
  const factory CustomerDetailsFormEv.emailChanged(String email) =
      _EmailChanged;
  const factory CustomerDetailsFormEv.phoneNumberChanged(String phone) =
      _PhoneNumberChanged;
  const factory CustomerDetailsFormEv.saveChanges() =
      _CustomerDetailsFormSaChanges;
  const factory CustomerDetailsFormEv.initializeUser(OrganikartUser user) =
      _InitializeUser;
}
