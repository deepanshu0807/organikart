part of 'basket_form_bloc.dart';

@freezed
abstract class BasketFormState with _$BasketFormState {
  const factory BasketFormState({
    @required OrderEntity order,
    @required Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
    @required Option<List<OrderEntity>> allPreviousOrders,
  }) = _BasketFormState;

  factory BasketFormState.initial() => BasketFormState(
        // isPaymentSuccess: false,
        // vrishamLocation: none(),
        saveFailureOrSuccessOption: none(),
        allPreviousOrders: none(),
        order: OrderEntity(
          customer: OrganikartUser(
              emailAddress: EmailAddress(""),
              uId: UniqueId(),
              lastSignInDateTime: DateTime.now(),
              name: Name(""),
              phoneNumber: PhoneNumber(""),
              role: const UserRole.customer(),
              picUrl: ""),
          orderDateTime: DateTime.now(),
          id: UniqueId(),
          items: [],
          paymentMethod: const PaymentMethod.online(),
          isPaymentDone: false,
          selectedTime: DateTime.now(),
          bookingOrderEntitytate: const OrderState.draftOrder(),
        ),
      );
}
