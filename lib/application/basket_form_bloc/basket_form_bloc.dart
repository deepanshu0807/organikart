import 'dart:async';

import 'package:bloc/bloc.dart';

import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/orders/i_order_repo.dart';
import 'package:organikart_shared/domain/orders/order_state.dart';
import 'package:organikart_shared/domain/orders/orders.dart';
import 'package:organikart_shared/domain/orders/payment_method.dart';
import 'package:organikart_shared/domain/payment/i_payment_repo.dart';
import 'package:organikart_shared/infrastructure/order/order_repo.dart';

import 'package:organikart_shared/organikart_shared_package.dart';
part 'basket_form_bloc.freezed.dart';
part 'basket_form_event.dart';
part 'basket_form_state.dart';

@injectable
class BasketFormBloc extends Bloc<BasketFormEvent, BasketFormState> {
  final IOrdersRepo _iOrdersRepo;
  final IPaymentRepo _iPaymentRepo;

  BasketFormBloc(this._iOrdersRepo, this._iPaymentRepo)
      : super(BasketFormState.initial());

  @override
  Stream<BasketFormState> mapEventToState(BasketFormEvent event) async* {
    yield* event.map(
      initialize: (e) async* {
        yield state.copyWith(
          saveFailureOrSuccessOption: none(),
          order: state.order.copyWith(
            id: UniqueId(),
            items: [],
            paymentMethod: const PaymentMethod.online(),
            isPaymentDone: false,
            bookingOrderEntitytate: const OrderState.draftOrder(),
          ),
        );
      },
      previousOrderListUpdated: _previousOrderListUpdated,
      selectedPayementMethod: (e) async* {
        yield state.copyWith(
          saveFailureOrSuccessOption: none(),
          order: state.order.copyWith(
            paymentMethod: e.paymentMethod,
          ),
        );
      },
      customerIdentified: (e) async* {
        yield state.copyWith(
            saveFailureOrSuccessOption: none(),
            order: state.order.copyWith(
              customer: e.customer,
            ));
      },
      addItem: (e) async* {
        final currentItemes = state.order.items.firstWhere(
          (element) => element.id == e.product.id,
          orElse: () => null,
        );

        if (currentItemes == null) {
          yield state.copyWith(
              saveFailureOrSuccessOption: none(),
              order: state.order.copyWith(
                items: [
                  ...state.order.items,
                  e.product,
                ],
              ));
        } else {
          final prevList = state.order.items;
          prevList.remove(currentItemes);

          yield state.copyWith(
              saveFailureOrSuccessOption: none(),
              order: state.order.copyWith(
                items: [
                  ...prevList,
                  e.product,
                ],
              ));
        }
      },
      subtractItem: (e) async* {
        final currentItemes = state.order.items.firstWhere(
          (element) => element.id == e.product.id,
          orElse: () => null,
        );

        if (currentItemes == null) {
          yield state;
        } else {
          final prevList = state.order.items;
          prevList.remove(currentItemes);

          yield state.copyWith(
            saveFailureOrSuccessOption: none(),
            order: state.order.copyWith(
              items: prevList,
              selectedTime: DateTime.now(),
            ),
          );
        }
      },
      placeOrderIsClicked: (e) async* {
        await _iPaymentRepo.onlinePaymentForOrder(
          order: state.order,
          onPaymentSuccess: paymentSuccess,
          onPaymentFailed: paymentFailed,
        );
      },
      onPaymentSuccess: (e) async* {
        final order = e.order.copyWith(
          paymentMethod: const PaymentMethod.online(),
          bookingOrderEntitytate: const OrderState.newOrder(),
          isPaymentDone: true,
        );

        final result = await _iOrdersRepo.createOrder(order).then((value) {
          if (value.isRight()) {
            state.order.items.clear();
          }
        });

        yield state.copyWith(
          saveFailureOrSuccessOption: some(result),
        );
      },
      onPaymentFailed: (e) async* {
        yield state.copyWith(
          saveFailureOrSuccessOption:
              some(left(const InfraFailure.paymentFailure())),
        );
      },
    );
  }

  Function(OrderEntity order, PaymentMethod method) get paymentSuccess =>
      (order, method) {
        add(BasketFormEvent.onPaymentSuccess(order, method));
      };
  Function(OrderEntity order, PaymentMethod paymentMethod) get paymentFailed =>
      (order, paymentMethod) {
        add(BasketFormEvent.onPaymentFailed(order, paymentMethod));
      };

  Stream<BasketFormState> _previousOrderListUpdated(
      _PreviousOrderListUpdated value) async* {
    yield state.copyWith(
      allPreviousOrders: some(value.orders),
    );
  }
}
