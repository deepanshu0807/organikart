part of 'basket_form_bloc.dart';

@freezed
abstract class BasketFormEvent with _$BasketFormEvent {
  const factory BasketFormEvent.initialize() = _Initialize;
  const factory BasketFormEvent.customerIdentified(OrganikartUser customer) =
      _CustomerIdentified;
  const factory BasketFormEvent.previousOrderListUpdated(
      List<OrderEntity> orders) = _PreviousOrderListUpdated;
  const factory BasketFormEvent.addItem(Content product) = _AddItem;
  const factory BasketFormEvent.subtractItem(Content product) = _SubtractItem;
  const factory BasketFormEvent.selectedPayementMethod(
      PaymentMethod paymentMethod) = _SelectedPayementMethod;

  const factory BasketFormEvent.placeOrderIsClicked() = _PlaceOrderIsClicked;
  const factory BasketFormEvent.onPaymentSuccess(
      OrderEntity order, PaymentMethod method) = _OnPaymentSuccess;
  const factory BasketFormEvent.onPaymentFailed(
      OrderEntity order, PaymentMethod method) = _OnPaymentFailed;
}
