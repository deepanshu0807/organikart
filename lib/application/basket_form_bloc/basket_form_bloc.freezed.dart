// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'basket_form_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$BasketFormEventTearOff {
  const _$BasketFormEventTearOff();

// ignore: unused_element
  _Initialize initialize() {
    return const _Initialize();
  }

// ignore: unused_element
  _CustomerIdentified customerIdentified(OrganikartUser customer) {
    return _CustomerIdentified(
      customer,
    );
  }

// ignore: unused_element
  _PreviousOrderListUpdated previousOrderListUpdated(List<OrderEntity> orders) {
    return _PreviousOrderListUpdated(
      orders,
    );
  }

// ignore: unused_element
  _AddItem addItem(Content product) {
    return _AddItem(
      product,
    );
  }

// ignore: unused_element
  _SubtractItem subtractItem(Content product) {
    return _SubtractItem(
      product,
    );
  }

// ignore: unused_element
  _SelectedPayementMethod selectedPayementMethod(PaymentMethod paymentMethod) {
    return _SelectedPayementMethod(
      paymentMethod,
    );
  }

// ignore: unused_element
  _PlaceOrderIsClicked placeOrderIsClicked() {
    return const _PlaceOrderIsClicked();
  }

// ignore: unused_element
  _OnPaymentSuccess onPaymentSuccess(OrderEntity order, PaymentMethod method) {
    return _OnPaymentSuccess(
      order,
      method,
    );
  }

// ignore: unused_element
  _OnPaymentFailed onPaymentFailed(OrderEntity order, PaymentMethod method) {
    return _OnPaymentFailed(
      order,
      method,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $BasketFormEvent = _$BasketFormEventTearOff();

/// @nodoc
mixin _$BasketFormEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $BasketFormEventCopyWith<$Res> {
  factory $BasketFormEventCopyWith(
          BasketFormEvent value, $Res Function(BasketFormEvent) then) =
      _$BasketFormEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$BasketFormEventCopyWithImpl<$Res>
    implements $BasketFormEventCopyWith<$Res> {
  _$BasketFormEventCopyWithImpl(this._value, this._then);

  final BasketFormEvent _value;
  // ignore: unused_field
  final $Res Function(BasketFormEvent) _then;
}

/// @nodoc
abstract class _$InitializeCopyWith<$Res> {
  factory _$InitializeCopyWith(
          _Initialize value, $Res Function(_Initialize) then) =
      __$InitializeCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitializeCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$InitializeCopyWith<$Res> {
  __$InitializeCopyWithImpl(
      _Initialize _value, $Res Function(_Initialize) _then)
      : super(_value, (v) => _then(v as _Initialize));

  @override
  _Initialize get _value => super._value as _Initialize;
}

/// @nodoc
class _$_Initialize implements _Initialize {
  const _$_Initialize();

  @override
  String toString() {
    return 'BasketFormEvent.initialize()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initialize);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return initialize();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return initialize(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize(this);
    }
    return orElse();
  }
}

abstract class _Initialize implements BasketFormEvent {
  const factory _Initialize() = _$_Initialize;
}

/// @nodoc
abstract class _$CustomerIdentifiedCopyWith<$Res> {
  factory _$CustomerIdentifiedCopyWith(
          _CustomerIdentified value, $Res Function(_CustomerIdentified) then) =
      __$CustomerIdentifiedCopyWithImpl<$Res>;
  $Res call({OrganikartUser customer});

  $OrganikartUserCopyWith<$Res> get customer;
}

/// @nodoc
class __$CustomerIdentifiedCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$CustomerIdentifiedCopyWith<$Res> {
  __$CustomerIdentifiedCopyWithImpl(
      _CustomerIdentified _value, $Res Function(_CustomerIdentified) _then)
      : super(_value, (v) => _then(v as _CustomerIdentified));

  @override
  _CustomerIdentified get _value => super._value as _CustomerIdentified;

  @override
  $Res call({
    Object customer = freezed,
  }) {
    return _then(_CustomerIdentified(
      customer == freezed ? _value.customer : customer as OrganikartUser,
    ));
  }

  @override
  $OrganikartUserCopyWith<$Res> get customer {
    if (_value.customer == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.customer, (value) {
      return _then(_value.copyWith(customer: value));
    });
  }
}

/// @nodoc
class _$_CustomerIdentified implements _CustomerIdentified {
  const _$_CustomerIdentified(this.customer) : assert(customer != null);

  @override
  final OrganikartUser customer;

  @override
  String toString() {
    return 'BasketFormEvent.customerIdentified(customer: $customer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CustomerIdentified &&
            (identical(other.customer, customer) ||
                const DeepCollectionEquality()
                    .equals(other.customer, customer)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(customer);

  @override
  _$CustomerIdentifiedCopyWith<_CustomerIdentified> get copyWith =>
      __$CustomerIdentifiedCopyWithImpl<_CustomerIdentified>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return customerIdentified(customer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (customerIdentified != null) {
      return customerIdentified(customer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return customerIdentified(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (customerIdentified != null) {
      return customerIdentified(this);
    }
    return orElse();
  }
}

abstract class _CustomerIdentified implements BasketFormEvent {
  const factory _CustomerIdentified(OrganikartUser customer) =
      _$_CustomerIdentified;

  OrganikartUser get customer;
  _$CustomerIdentifiedCopyWith<_CustomerIdentified> get copyWith;
}

/// @nodoc
abstract class _$PreviousOrderListUpdatedCopyWith<$Res> {
  factory _$PreviousOrderListUpdatedCopyWith(_PreviousOrderListUpdated value,
          $Res Function(_PreviousOrderListUpdated) then) =
      __$PreviousOrderListUpdatedCopyWithImpl<$Res>;
  $Res call({List<OrderEntity> orders});
}

/// @nodoc
class __$PreviousOrderListUpdatedCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$PreviousOrderListUpdatedCopyWith<$Res> {
  __$PreviousOrderListUpdatedCopyWithImpl(_PreviousOrderListUpdated _value,
      $Res Function(_PreviousOrderListUpdated) _then)
      : super(_value, (v) => _then(v as _PreviousOrderListUpdated));

  @override
  _PreviousOrderListUpdated get _value =>
      super._value as _PreviousOrderListUpdated;

  @override
  $Res call({
    Object orders = freezed,
  }) {
    return _then(_PreviousOrderListUpdated(
      orders == freezed ? _value.orders : orders as List<OrderEntity>,
    ));
  }
}

/// @nodoc
class _$_PreviousOrderListUpdated implements _PreviousOrderListUpdated {
  const _$_PreviousOrderListUpdated(this.orders) : assert(orders != null);

  @override
  final List<OrderEntity> orders;

  @override
  String toString() {
    return 'BasketFormEvent.previousOrderListUpdated(orders: $orders)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PreviousOrderListUpdated &&
            (identical(other.orders, orders) ||
                const DeepCollectionEquality().equals(other.orders, orders)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(orders);

  @override
  _$PreviousOrderListUpdatedCopyWith<_PreviousOrderListUpdated> get copyWith =>
      __$PreviousOrderListUpdatedCopyWithImpl<_PreviousOrderListUpdated>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return previousOrderListUpdated(orders);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (previousOrderListUpdated != null) {
      return previousOrderListUpdated(orders);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return previousOrderListUpdated(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (previousOrderListUpdated != null) {
      return previousOrderListUpdated(this);
    }
    return orElse();
  }
}

abstract class _PreviousOrderListUpdated implements BasketFormEvent {
  const factory _PreviousOrderListUpdated(List<OrderEntity> orders) =
      _$_PreviousOrderListUpdated;

  List<OrderEntity> get orders;
  _$PreviousOrderListUpdatedCopyWith<_PreviousOrderListUpdated> get copyWith;
}

/// @nodoc
abstract class _$AddItemCopyWith<$Res> {
  factory _$AddItemCopyWith(_AddItem value, $Res Function(_AddItem) then) =
      __$AddItemCopyWithImpl<$Res>;
  $Res call({Content product});

  $ContentCopyWith<$Res> get product;
}

/// @nodoc
class __$AddItemCopyWithImpl<$Res> extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$AddItemCopyWith<$Res> {
  __$AddItemCopyWithImpl(_AddItem _value, $Res Function(_AddItem) _then)
      : super(_value, (v) => _then(v as _AddItem));

  @override
  _AddItem get _value => super._value as _AddItem;

  @override
  $Res call({
    Object product = freezed,
  }) {
    return _then(_AddItem(
      product == freezed ? _value.product : product as Content,
    ));
  }

  @override
  $ContentCopyWith<$Res> get product {
    if (_value.product == null) {
      return null;
    }
    return $ContentCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc
class _$_AddItem implements _AddItem {
  const _$_AddItem(this.product) : assert(product != null);

  @override
  final Content product;

  @override
  String toString() {
    return 'BasketFormEvent.addItem(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _AddItem &&
            (identical(other.product, product) ||
                const DeepCollectionEquality().equals(other.product, product)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(product);

  @override
  _$AddItemCopyWith<_AddItem> get copyWith =>
      __$AddItemCopyWithImpl<_AddItem>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return addItem(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addItem != null) {
      return addItem(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return addItem(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (addItem != null) {
      return addItem(this);
    }
    return orElse();
  }
}

abstract class _AddItem implements BasketFormEvent {
  const factory _AddItem(Content product) = _$_AddItem;

  Content get product;
  _$AddItemCopyWith<_AddItem> get copyWith;
}

/// @nodoc
abstract class _$SubtractItemCopyWith<$Res> {
  factory _$SubtractItemCopyWith(
          _SubtractItem value, $Res Function(_SubtractItem) then) =
      __$SubtractItemCopyWithImpl<$Res>;
  $Res call({Content product});

  $ContentCopyWith<$Res> get product;
}

/// @nodoc
class __$SubtractItemCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$SubtractItemCopyWith<$Res> {
  __$SubtractItemCopyWithImpl(
      _SubtractItem _value, $Res Function(_SubtractItem) _then)
      : super(_value, (v) => _then(v as _SubtractItem));

  @override
  _SubtractItem get _value => super._value as _SubtractItem;

  @override
  $Res call({
    Object product = freezed,
  }) {
    return _then(_SubtractItem(
      product == freezed ? _value.product : product as Content,
    ));
  }

  @override
  $ContentCopyWith<$Res> get product {
    if (_value.product == null) {
      return null;
    }
    return $ContentCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value));
    });
  }
}

/// @nodoc
class _$_SubtractItem implements _SubtractItem {
  const _$_SubtractItem(this.product) : assert(product != null);

  @override
  final Content product;

  @override
  String toString() {
    return 'BasketFormEvent.subtractItem(product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SubtractItem &&
            (identical(other.product, product) ||
                const DeepCollectionEquality().equals(other.product, product)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(product);

  @override
  _$SubtractItemCopyWith<_SubtractItem> get copyWith =>
      __$SubtractItemCopyWithImpl<_SubtractItem>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return subtractItem(product);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (subtractItem != null) {
      return subtractItem(product);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return subtractItem(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (subtractItem != null) {
      return subtractItem(this);
    }
    return orElse();
  }
}

abstract class _SubtractItem implements BasketFormEvent {
  const factory _SubtractItem(Content product) = _$_SubtractItem;

  Content get product;
  _$SubtractItemCopyWith<_SubtractItem> get copyWith;
}

/// @nodoc
abstract class _$SelectedPayementMethodCopyWith<$Res> {
  factory _$SelectedPayementMethodCopyWith(_SelectedPayementMethod value,
          $Res Function(_SelectedPayementMethod) then) =
      __$SelectedPayementMethodCopyWithImpl<$Res>;
  $Res call({PaymentMethod paymentMethod});

  $PaymentMethodCopyWith<$Res> get paymentMethod;
}

/// @nodoc
class __$SelectedPayementMethodCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$SelectedPayementMethodCopyWith<$Res> {
  __$SelectedPayementMethodCopyWithImpl(_SelectedPayementMethod _value,
      $Res Function(_SelectedPayementMethod) _then)
      : super(_value, (v) => _then(v as _SelectedPayementMethod));

  @override
  _SelectedPayementMethod get _value => super._value as _SelectedPayementMethod;

  @override
  $Res call({
    Object paymentMethod = freezed,
  }) {
    return _then(_SelectedPayementMethod(
      paymentMethod == freezed
          ? _value.paymentMethod
          : paymentMethod as PaymentMethod,
    ));
  }

  @override
  $PaymentMethodCopyWith<$Res> get paymentMethod {
    if (_value.paymentMethod == null) {
      return null;
    }
    return $PaymentMethodCopyWith<$Res>(_value.paymentMethod, (value) {
      return _then(_value.copyWith(paymentMethod: value));
    });
  }
}

/// @nodoc
class _$_SelectedPayementMethod implements _SelectedPayementMethod {
  const _$_SelectedPayementMethod(this.paymentMethod)
      : assert(paymentMethod != null);

  @override
  final PaymentMethod paymentMethod;

  @override
  String toString() {
    return 'BasketFormEvent.selectedPayementMethod(paymentMethod: $paymentMethod)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SelectedPayementMethod &&
            (identical(other.paymentMethod, paymentMethod) ||
                const DeepCollectionEquality()
                    .equals(other.paymentMethod, paymentMethod)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(paymentMethod);

  @override
  _$SelectedPayementMethodCopyWith<_SelectedPayementMethod> get copyWith =>
      __$SelectedPayementMethodCopyWithImpl<_SelectedPayementMethod>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return selectedPayementMethod(paymentMethod);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (selectedPayementMethod != null) {
      return selectedPayementMethod(paymentMethod);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return selectedPayementMethod(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (selectedPayementMethod != null) {
      return selectedPayementMethod(this);
    }
    return orElse();
  }
}

abstract class _SelectedPayementMethod implements BasketFormEvent {
  const factory _SelectedPayementMethod(PaymentMethod paymentMethod) =
      _$_SelectedPayementMethod;

  PaymentMethod get paymentMethod;
  _$SelectedPayementMethodCopyWith<_SelectedPayementMethod> get copyWith;
}

/// @nodoc
abstract class _$PlaceOrderIsClickedCopyWith<$Res> {
  factory _$PlaceOrderIsClickedCopyWith(_PlaceOrderIsClicked value,
          $Res Function(_PlaceOrderIsClicked) then) =
      __$PlaceOrderIsClickedCopyWithImpl<$Res>;
}

/// @nodoc
class __$PlaceOrderIsClickedCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$PlaceOrderIsClickedCopyWith<$Res> {
  __$PlaceOrderIsClickedCopyWithImpl(
      _PlaceOrderIsClicked _value, $Res Function(_PlaceOrderIsClicked) _then)
      : super(_value, (v) => _then(v as _PlaceOrderIsClicked));

  @override
  _PlaceOrderIsClicked get _value => super._value as _PlaceOrderIsClicked;
}

/// @nodoc
class _$_PlaceOrderIsClicked implements _PlaceOrderIsClicked {
  const _$_PlaceOrderIsClicked();

  @override
  String toString() {
    return 'BasketFormEvent.placeOrderIsClicked()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _PlaceOrderIsClicked);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return placeOrderIsClicked();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (placeOrderIsClicked != null) {
      return placeOrderIsClicked();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return placeOrderIsClicked(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (placeOrderIsClicked != null) {
      return placeOrderIsClicked(this);
    }
    return orElse();
  }
}

abstract class _PlaceOrderIsClicked implements BasketFormEvent {
  const factory _PlaceOrderIsClicked() = _$_PlaceOrderIsClicked;
}

/// @nodoc
abstract class _$OnPaymentSuccessCopyWith<$Res> {
  factory _$OnPaymentSuccessCopyWith(
          _OnPaymentSuccess value, $Res Function(_OnPaymentSuccess) then) =
      __$OnPaymentSuccessCopyWithImpl<$Res>;
  $Res call({OrderEntity order, PaymentMethod method});

  $OrderEntityCopyWith<$Res> get order;
  $PaymentMethodCopyWith<$Res> get method;
}

/// @nodoc
class __$OnPaymentSuccessCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$OnPaymentSuccessCopyWith<$Res> {
  __$OnPaymentSuccessCopyWithImpl(
      _OnPaymentSuccess _value, $Res Function(_OnPaymentSuccess) _then)
      : super(_value, (v) => _then(v as _OnPaymentSuccess));

  @override
  _OnPaymentSuccess get _value => super._value as _OnPaymentSuccess;

  @override
  $Res call({
    Object order = freezed,
    Object method = freezed,
  }) {
    return _then(_OnPaymentSuccess(
      order == freezed ? _value.order : order as OrderEntity,
      method == freezed ? _value.method : method as PaymentMethod,
    ));
  }

  @override
  $OrderEntityCopyWith<$Res> get order {
    if (_value.order == null) {
      return null;
    }
    return $OrderEntityCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $PaymentMethodCopyWith<$Res> get method {
    if (_value.method == null) {
      return null;
    }
    return $PaymentMethodCopyWith<$Res>(_value.method, (value) {
      return _then(_value.copyWith(method: value));
    });
  }
}

/// @nodoc
class _$_OnPaymentSuccess implements _OnPaymentSuccess {
  const _$_OnPaymentSuccess(this.order, this.method)
      : assert(order != null),
        assert(method != null);

  @override
  final OrderEntity order;
  @override
  final PaymentMethod method;

  @override
  String toString() {
    return 'BasketFormEvent.onPaymentSuccess(order: $order, method: $method)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OnPaymentSuccess &&
            (identical(other.order, order) ||
                const DeepCollectionEquality().equals(other.order, order)) &&
            (identical(other.method, method) ||
                const DeepCollectionEquality().equals(other.method, method)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(order) ^
      const DeepCollectionEquality().hash(method);

  @override
  _$OnPaymentSuccessCopyWith<_OnPaymentSuccess> get copyWith =>
      __$OnPaymentSuccessCopyWithImpl<_OnPaymentSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return onPaymentSuccess(order, method);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (onPaymentSuccess != null) {
      return onPaymentSuccess(order, method);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return onPaymentSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (onPaymentSuccess != null) {
      return onPaymentSuccess(this);
    }
    return orElse();
  }
}

abstract class _OnPaymentSuccess implements BasketFormEvent {
  const factory _OnPaymentSuccess(OrderEntity order, PaymentMethod method) =
      _$_OnPaymentSuccess;

  OrderEntity get order;
  PaymentMethod get method;
  _$OnPaymentSuccessCopyWith<_OnPaymentSuccess> get copyWith;
}

/// @nodoc
abstract class _$OnPaymentFailedCopyWith<$Res> {
  factory _$OnPaymentFailedCopyWith(
          _OnPaymentFailed value, $Res Function(_OnPaymentFailed) then) =
      __$OnPaymentFailedCopyWithImpl<$Res>;
  $Res call({OrderEntity order, PaymentMethod method});

  $OrderEntityCopyWith<$Res> get order;
  $PaymentMethodCopyWith<$Res> get method;
}

/// @nodoc
class __$OnPaymentFailedCopyWithImpl<$Res>
    extends _$BasketFormEventCopyWithImpl<$Res>
    implements _$OnPaymentFailedCopyWith<$Res> {
  __$OnPaymentFailedCopyWithImpl(
      _OnPaymentFailed _value, $Res Function(_OnPaymentFailed) _then)
      : super(_value, (v) => _then(v as _OnPaymentFailed));

  @override
  _OnPaymentFailed get _value => super._value as _OnPaymentFailed;

  @override
  $Res call({
    Object order = freezed,
    Object method = freezed,
  }) {
    return _then(_OnPaymentFailed(
      order == freezed ? _value.order : order as OrderEntity,
      method == freezed ? _value.method : method as PaymentMethod,
    ));
  }

  @override
  $OrderEntityCopyWith<$Res> get order {
    if (_value.order == null) {
      return null;
    }
    return $OrderEntityCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }

  @override
  $PaymentMethodCopyWith<$Res> get method {
    if (_value.method == null) {
      return null;
    }
    return $PaymentMethodCopyWith<$Res>(_value.method, (value) {
      return _then(_value.copyWith(method: value));
    });
  }
}

/// @nodoc
class _$_OnPaymentFailed implements _OnPaymentFailed {
  const _$_OnPaymentFailed(this.order, this.method)
      : assert(order != null),
        assert(method != null);

  @override
  final OrderEntity order;
  @override
  final PaymentMethod method;

  @override
  String toString() {
    return 'BasketFormEvent.onPaymentFailed(order: $order, method: $method)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OnPaymentFailed &&
            (identical(other.order, order) ||
                const DeepCollectionEquality().equals(other.order, order)) &&
            (identical(other.method, method) ||
                const DeepCollectionEquality().equals(other.method, method)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(order) ^
      const DeepCollectionEquality().hash(method);

  @override
  _$OnPaymentFailedCopyWith<_OnPaymentFailed> get copyWith =>
      __$OnPaymentFailedCopyWithImpl<_OnPaymentFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initialize(),
    @required TResult customerIdentified(OrganikartUser customer),
    @required TResult previousOrderListUpdated(List<OrderEntity> orders),
    @required TResult addItem(Content product),
    @required TResult subtractItem(Content product),
    @required TResult selectedPayementMethod(PaymentMethod paymentMethod),
    @required TResult placeOrderIsClicked(),
    @required TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    @required TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return onPaymentFailed(order, method);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initialize(),
    TResult customerIdentified(OrganikartUser customer),
    TResult previousOrderListUpdated(List<OrderEntity> orders),
    TResult addItem(Content product),
    TResult subtractItem(Content product),
    TResult selectedPayementMethod(PaymentMethod paymentMethod),
    TResult placeOrderIsClicked(),
    TResult onPaymentSuccess(OrderEntity order, PaymentMethod method),
    TResult onPaymentFailed(OrderEntity order, PaymentMethod method),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (onPaymentFailed != null) {
      return onPaymentFailed(order, method);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initialize(_Initialize value),
    @required TResult customerIdentified(_CustomerIdentified value),
    @required TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    @required TResult addItem(_AddItem value),
    @required TResult subtractItem(_SubtractItem value),
    @required TResult selectedPayementMethod(_SelectedPayementMethod value),
    @required TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    @required TResult onPaymentSuccess(_OnPaymentSuccess value),
    @required TResult onPaymentFailed(_OnPaymentFailed value),
  }) {
    assert(initialize != null);
    assert(customerIdentified != null);
    assert(previousOrderListUpdated != null);
    assert(addItem != null);
    assert(subtractItem != null);
    assert(selectedPayementMethod != null);
    assert(placeOrderIsClicked != null);
    assert(onPaymentSuccess != null);
    assert(onPaymentFailed != null);
    return onPaymentFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initialize(_Initialize value),
    TResult customerIdentified(_CustomerIdentified value),
    TResult previousOrderListUpdated(_PreviousOrderListUpdated value),
    TResult addItem(_AddItem value),
    TResult subtractItem(_SubtractItem value),
    TResult selectedPayementMethod(_SelectedPayementMethod value),
    TResult placeOrderIsClicked(_PlaceOrderIsClicked value),
    TResult onPaymentSuccess(_OnPaymentSuccess value),
    TResult onPaymentFailed(_OnPaymentFailed value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (onPaymentFailed != null) {
      return onPaymentFailed(this);
    }
    return orElse();
  }
}

abstract class _OnPaymentFailed implements BasketFormEvent {
  const factory _OnPaymentFailed(OrderEntity order, PaymentMethod method) =
      _$_OnPaymentFailed;

  OrderEntity get order;
  PaymentMethod get method;
  _$OnPaymentFailedCopyWith<_OnPaymentFailed> get copyWith;
}

/// @nodoc
class _$BasketFormStateTearOff {
  const _$BasketFormStateTearOff();

// ignore: unused_element
  _BasketFormState call(
      {@required OrderEntity order,
      @required Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      @required Option<List<OrderEntity>> allPreviousOrders}) {
    return _BasketFormState(
      order: order,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption,
      allPreviousOrders: allPreviousOrders,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $BasketFormState = _$BasketFormStateTearOff();

/// @nodoc
mixin _$BasketFormState {
  OrderEntity get order;
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  Option<List<OrderEntity>> get allPreviousOrders;

  $BasketFormStateCopyWith<BasketFormState> get copyWith;
}

/// @nodoc
abstract class $BasketFormStateCopyWith<$Res> {
  factory $BasketFormStateCopyWith(
          BasketFormState value, $Res Function(BasketFormState) then) =
      _$BasketFormStateCopyWithImpl<$Res>;
  $Res call(
      {OrderEntity order,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      Option<List<OrderEntity>> allPreviousOrders});

  $OrderEntityCopyWith<$Res> get order;
}

/// @nodoc
class _$BasketFormStateCopyWithImpl<$Res>
    implements $BasketFormStateCopyWith<$Res> {
  _$BasketFormStateCopyWithImpl(this._value, this._then);

  final BasketFormState _value;
  // ignore: unused_field
  final $Res Function(BasketFormState) _then;

  @override
  $Res call({
    Object order = freezed,
    Object saveFailureOrSuccessOption = freezed,
    Object allPreviousOrders = freezed,
  }) {
    return _then(_value.copyWith(
      order: order == freezed ? _value.order : order as OrderEntity,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      allPreviousOrders: allPreviousOrders == freezed
          ? _value.allPreviousOrders
          : allPreviousOrders as Option<List<OrderEntity>>,
    ));
  }

  @override
  $OrderEntityCopyWith<$Res> get order {
    if (_value.order == null) {
      return null;
    }
    return $OrderEntityCopyWith<$Res>(_value.order, (value) {
      return _then(_value.copyWith(order: value));
    });
  }
}

/// @nodoc
abstract class _$BasketFormStateCopyWith<$Res>
    implements $BasketFormStateCopyWith<$Res> {
  factory _$BasketFormStateCopyWith(
          _BasketFormState value, $Res Function(_BasketFormState) then) =
      __$BasketFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {OrderEntity order,
      Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      Option<List<OrderEntity>> allPreviousOrders});

  @override
  $OrderEntityCopyWith<$Res> get order;
}

/// @nodoc
class __$BasketFormStateCopyWithImpl<$Res>
    extends _$BasketFormStateCopyWithImpl<$Res>
    implements _$BasketFormStateCopyWith<$Res> {
  __$BasketFormStateCopyWithImpl(
      _BasketFormState _value, $Res Function(_BasketFormState) _then)
      : super(_value, (v) => _then(v as _BasketFormState));

  @override
  _BasketFormState get _value => super._value as _BasketFormState;

  @override
  $Res call({
    Object order = freezed,
    Object saveFailureOrSuccessOption = freezed,
    Object allPreviousOrders = freezed,
  }) {
    return _then(_BasketFormState(
      order: order == freezed ? _value.order : order as OrderEntity,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption as Option<Either<InfraFailure, Unit>>,
      allPreviousOrders: allPreviousOrders == freezed
          ? _value.allPreviousOrders
          : allPreviousOrders as Option<List<OrderEntity>>,
    ));
  }
}

/// @nodoc
class _$_BasketFormState implements _BasketFormState {
  const _$_BasketFormState(
      {@required this.order,
      @required this.saveFailureOrSuccessOption,
      @required this.allPreviousOrders})
      : assert(order != null),
        assert(saveFailureOrSuccessOption != null),
        assert(allPreviousOrders != null);

  @override
  final OrderEntity order;
  @override
  final Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption;
  @override
  final Option<List<OrderEntity>> allPreviousOrders;

  @override
  String toString() {
    return 'BasketFormState(order: $order, saveFailureOrSuccessOption: $saveFailureOrSuccessOption, allPreviousOrders: $allPreviousOrders)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _BasketFormState &&
            (identical(other.order, order) ||
                const DeepCollectionEquality().equals(other.order, order)) &&
            (identical(other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption)) &&
            (identical(other.allPreviousOrders, allPreviousOrders) ||
                const DeepCollectionEquality()
                    .equals(other.allPreviousOrders, allPreviousOrders)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(order) ^
      const DeepCollectionEquality().hash(saveFailureOrSuccessOption) ^
      const DeepCollectionEquality().hash(allPreviousOrders);

  @override
  _$BasketFormStateCopyWith<_BasketFormState> get copyWith =>
      __$BasketFormStateCopyWithImpl<_BasketFormState>(this, _$identity);
}

abstract class _BasketFormState implements BasketFormState {
  const factory _BasketFormState(
      {@required
          OrderEntity order,
      @required
          Option<Either<InfraFailure, Unit>> saveFailureOrSuccessOption,
      @required
          Option<List<OrderEntity>> allPreviousOrders}) = _$_BasketFormState;

  @override
  OrderEntity get order;
  @override
  Option<Either<InfraFailure, Unit>> get saveFailureOrSuccessOption;
  @override
  Option<List<OrderEntity>> get allPreviousOrders;
  @override
  _$BasketFormStateCopyWith<_BasketFormState> get copyWith;
}
