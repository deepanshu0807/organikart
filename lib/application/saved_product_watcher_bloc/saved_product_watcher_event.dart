part of 'saved_product_watcher_bloc.dart';

@freezed
abstract class SavedProductWatcherEvent with _$SavedProductWatcherEvent {
  const factory SavedProductWatcherEvent.getMySavedProducts(
      OrganikartUser user) = _GetMySavedProducts;
  const factory SavedProductWatcherEvent.savedProductReceived(
      Either<InfraFailure, SavedProducts> savedProducts,
      OrganikartUser user) = _SavedProductReceived;
}
