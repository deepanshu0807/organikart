import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:organikart_shared/domain/features/i_customer_repo.dart';
import 'package:organikart_shared/domain/features/saved_products.dart';
import 'package:organikart_shared/infrastructure/features/customer_repo.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'saved_product_watcher_event.dart';
part 'saved_product_watcher_state.dart';
part 'saved_product_watcher_bloc.freezed.dart';

@injectable
class SavedProductWatcherBloc
    extends Bloc<SavedProductWatcherEvent, SavedProductWatcherState> {
  final ICustomerRepo _iCustomerRepo;
  SavedProductWatcherBloc(this._iCustomerRepo)
      : super(const SavedProductWatcherState.initial());

  StreamSubscription stream;

  @override
  Stream<SavedProductWatcherState> mapEventToState(
    SavedProductWatcherEvent event,
  ) async* {
    yield* event.map(
      getMySavedProducts: (e) async* {
        if (stream == null) {
          yield const SavedProductWatcherState.loadInProgress();
          stream = _iCustomerRepo.getMySavedProducts(e.user).listen((c) =>
              add(SavedProductWatcherEvent.savedProductReceived(c, e.user)));
        }
      },
      savedProductReceived: (e) async* {
        yield e.savedProducts.fold(
          (f) => SavedProductWatcherState.loadFailure(f),
          (c) => SavedProductWatcherState.loadSuccess(c, e.user),
        );
      },
    );
  }

  @override
  Future<void> close() {
    stream?.cancel();
    return super.close();
  }
}
