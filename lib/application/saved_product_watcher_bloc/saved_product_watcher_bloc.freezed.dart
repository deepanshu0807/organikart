// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'saved_product_watcher_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$SavedProductWatcherEventTearOff {
  const _$SavedProductWatcherEventTearOff();

// ignore: unused_element
  _GetMySavedProducts getMySavedProducts(OrganikartUser user) {
    return _GetMySavedProducts(
      user,
    );
  }

// ignore: unused_element
  _SavedProductReceived savedProductReceived(
      Either<InfraFailure, SavedProducts> savedProducts, OrganikartUser user) {
    return _SavedProductReceived(
      savedProducts,
      user,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SavedProductWatcherEvent = _$SavedProductWatcherEventTearOff();

/// @nodoc
mixin _$SavedProductWatcherEvent {
  OrganikartUser get user;

  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getMySavedProducts(OrganikartUser user),
    @required
        TResult savedProductReceived(
            Either<InfraFailure, SavedProducts> savedProducts,
            OrganikartUser user),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getMySavedProducts(OrganikartUser user),
    TResult savedProductReceived(
        Either<InfraFailure, SavedProducts> savedProducts, OrganikartUser user),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getMySavedProducts(_GetMySavedProducts value),
    @required TResult savedProductReceived(_SavedProductReceived value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getMySavedProducts(_GetMySavedProducts value),
    TResult savedProductReceived(_SavedProductReceived value),
    @required TResult orElse(),
  });

  $SavedProductWatcherEventCopyWith<SavedProductWatcherEvent> get copyWith;
}

/// @nodoc
abstract class $SavedProductWatcherEventCopyWith<$Res> {
  factory $SavedProductWatcherEventCopyWith(SavedProductWatcherEvent value,
          $Res Function(SavedProductWatcherEvent) then) =
      _$SavedProductWatcherEventCopyWithImpl<$Res>;
  $Res call({OrganikartUser user});

  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class _$SavedProductWatcherEventCopyWithImpl<$Res>
    implements $SavedProductWatcherEventCopyWith<$Res> {
  _$SavedProductWatcherEventCopyWithImpl(this._value, this._then);

  final SavedProductWatcherEvent _value;
  // ignore: unused_field
  final $Res Function(SavedProductWatcherEvent) _then;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(_value.copyWith(
      user: user == freezed ? _value.user : user as OrganikartUser,
    ));
  }

  @override
  $OrganikartUserCopyWith<$Res> get user {
    if (_value.user == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc
abstract class _$GetMySavedProductsCopyWith<$Res>
    implements $SavedProductWatcherEventCopyWith<$Res> {
  factory _$GetMySavedProductsCopyWith(
          _GetMySavedProducts value, $Res Function(_GetMySavedProducts) then) =
      __$GetMySavedProductsCopyWithImpl<$Res>;
  @override
  $Res call({OrganikartUser user});

  @override
  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class __$GetMySavedProductsCopyWithImpl<$Res>
    extends _$SavedProductWatcherEventCopyWithImpl<$Res>
    implements _$GetMySavedProductsCopyWith<$Res> {
  __$GetMySavedProductsCopyWithImpl(
      _GetMySavedProducts _value, $Res Function(_GetMySavedProducts) _then)
      : super(_value, (v) => _then(v as _GetMySavedProducts));

  @override
  _GetMySavedProducts get _value => super._value as _GetMySavedProducts;

  @override
  $Res call({
    Object user = freezed,
  }) {
    return _then(_GetMySavedProducts(
      user == freezed ? _value.user : user as OrganikartUser,
    ));
  }
}

/// @nodoc
class _$_GetMySavedProducts implements _GetMySavedProducts {
  const _$_GetMySavedProducts(this.user) : assert(user != null);

  @override
  final OrganikartUser user;

  @override
  String toString() {
    return 'SavedProductWatcherEvent.getMySavedProducts(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _GetMySavedProducts &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(user);

  @override
  _$GetMySavedProductsCopyWith<_GetMySavedProducts> get copyWith =>
      __$GetMySavedProductsCopyWithImpl<_GetMySavedProducts>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getMySavedProducts(OrganikartUser user),
    @required
        TResult savedProductReceived(
            Either<InfraFailure, SavedProducts> savedProducts,
            OrganikartUser user),
  }) {
    assert(getMySavedProducts != null);
    assert(savedProductReceived != null);
    return getMySavedProducts(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getMySavedProducts(OrganikartUser user),
    TResult savedProductReceived(
        Either<InfraFailure, SavedProducts> savedProducts, OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getMySavedProducts != null) {
      return getMySavedProducts(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getMySavedProducts(_GetMySavedProducts value),
    @required TResult savedProductReceived(_SavedProductReceived value),
  }) {
    assert(getMySavedProducts != null);
    assert(savedProductReceived != null);
    return getMySavedProducts(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getMySavedProducts(_GetMySavedProducts value),
    TResult savedProductReceived(_SavedProductReceived value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getMySavedProducts != null) {
      return getMySavedProducts(this);
    }
    return orElse();
  }
}

abstract class _GetMySavedProducts implements SavedProductWatcherEvent {
  const factory _GetMySavedProducts(OrganikartUser user) =
      _$_GetMySavedProducts;

  @override
  OrganikartUser get user;
  @override
  _$GetMySavedProductsCopyWith<_GetMySavedProducts> get copyWith;
}

/// @nodoc
abstract class _$SavedProductReceivedCopyWith<$Res>
    implements $SavedProductWatcherEventCopyWith<$Res> {
  factory _$SavedProductReceivedCopyWith(_SavedProductReceived value,
          $Res Function(_SavedProductReceived) then) =
      __$SavedProductReceivedCopyWithImpl<$Res>;
  @override
  $Res call(
      {Either<InfraFailure, SavedProducts> savedProducts, OrganikartUser user});

  @override
  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class __$SavedProductReceivedCopyWithImpl<$Res>
    extends _$SavedProductWatcherEventCopyWithImpl<$Res>
    implements _$SavedProductReceivedCopyWith<$Res> {
  __$SavedProductReceivedCopyWithImpl(
      _SavedProductReceived _value, $Res Function(_SavedProductReceived) _then)
      : super(_value, (v) => _then(v as _SavedProductReceived));

  @override
  _SavedProductReceived get _value => super._value as _SavedProductReceived;

  @override
  $Res call({
    Object savedProducts = freezed,
    Object user = freezed,
  }) {
    return _then(_SavedProductReceived(
      savedProducts == freezed
          ? _value.savedProducts
          : savedProducts as Either<InfraFailure, SavedProducts>,
      user == freezed ? _value.user : user as OrganikartUser,
    ));
  }
}

/// @nodoc
class _$_SavedProductReceived implements _SavedProductReceived {
  const _$_SavedProductReceived(this.savedProducts, this.user)
      : assert(savedProducts != null),
        assert(user != null);

  @override
  final Either<InfraFailure, SavedProducts> savedProducts;
  @override
  final OrganikartUser user;

  @override
  String toString() {
    return 'SavedProductWatcherEvent.savedProductReceived(savedProducts: $savedProducts, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SavedProductReceived &&
            (identical(other.savedProducts, savedProducts) ||
                const DeepCollectionEquality()
                    .equals(other.savedProducts, savedProducts)) &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(savedProducts) ^
      const DeepCollectionEquality().hash(user);

  @override
  _$SavedProductReceivedCopyWith<_SavedProductReceived> get copyWith =>
      __$SavedProductReceivedCopyWithImpl<_SavedProductReceived>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getMySavedProducts(OrganikartUser user),
    @required
        TResult savedProductReceived(
            Either<InfraFailure, SavedProducts> savedProducts,
            OrganikartUser user),
  }) {
    assert(getMySavedProducts != null);
    assert(savedProductReceived != null);
    return savedProductReceived(savedProducts, user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getMySavedProducts(OrganikartUser user),
    TResult savedProductReceived(
        Either<InfraFailure, SavedProducts> savedProducts, OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (savedProductReceived != null) {
      return savedProductReceived(savedProducts, user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getMySavedProducts(_GetMySavedProducts value),
    @required TResult savedProductReceived(_SavedProductReceived value),
  }) {
    assert(getMySavedProducts != null);
    assert(savedProductReceived != null);
    return savedProductReceived(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getMySavedProducts(_GetMySavedProducts value),
    TResult savedProductReceived(_SavedProductReceived value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (savedProductReceived != null) {
      return savedProductReceived(this);
    }
    return orElse();
  }
}

abstract class _SavedProductReceived implements SavedProductWatcherEvent {
  const factory _SavedProductReceived(
      Either<InfraFailure, SavedProducts> savedProducts,
      OrganikartUser user) = _$_SavedProductReceived;

  Either<InfraFailure, SavedProducts> get savedProducts;
  @override
  OrganikartUser get user;
  @override
  _$SavedProductReceivedCopyWith<_SavedProductReceived> get copyWith;
}

/// @nodoc
class _$SavedProductWatcherStateTearOff {
  const _$SavedProductWatcherStateTearOff();

// ignore: unused_element
  _Initial initial() {
    return const _Initial();
  }

// ignore: unused_element
  _DataTransferInProgress loadInProgress() {
    return const _DataTransferInProgress();
  }

// ignore: unused_element
  _LoadFailure loadFailure(InfraFailure<dynamic> failure) {
    return _LoadFailure(
      failure,
    );
  }

// ignore: unused_element
  _LoadSuccess loadSuccess(SavedProducts savedProducts, OrganikartUser user) {
    return _LoadSuccess(
      savedProducts,
      user,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $SavedProductWatcherState = _$SavedProductWatcherStateTearOff();

/// @nodoc
mixin _$SavedProductWatcherState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required
        TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $SavedProductWatcherStateCopyWith<$Res> {
  factory $SavedProductWatcherStateCopyWith(SavedProductWatcherState value,
          $Res Function(SavedProductWatcherState) then) =
      _$SavedProductWatcherStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SavedProductWatcherStateCopyWithImpl<$Res>
    implements $SavedProductWatcherStateCopyWith<$Res> {
  _$SavedProductWatcherStateCopyWithImpl(this._value, this._then);

  final SavedProductWatcherState _value;
  // ignore: unused_field
  final $Res Function(SavedProductWatcherState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$SavedProductWatcherStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc
class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'SavedProductWatcherState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required
        TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements SavedProductWatcherState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$DataTransferInProgressCopyWith<$Res> {
  factory _$DataTransferInProgressCopyWith(_DataTransferInProgress value,
          $Res Function(_DataTransferInProgress) then) =
      __$DataTransferInProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$DataTransferInProgressCopyWithImpl<$Res>
    extends _$SavedProductWatcherStateCopyWithImpl<$Res>
    implements _$DataTransferInProgressCopyWith<$Res> {
  __$DataTransferInProgressCopyWithImpl(_DataTransferInProgress _value,
      $Res Function(_DataTransferInProgress) _then)
      : super(_value, (v) => _then(v as _DataTransferInProgress));

  @override
  _DataTransferInProgress get _value => super._value as _DataTransferInProgress;
}

/// @nodoc
class _$_DataTransferInProgress implements _DataTransferInProgress {
  const _$_DataTransferInProgress();

  @override
  String toString() {
    return 'SavedProductWatcherState.loadInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _DataTransferInProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required
        TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadInProgress();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress(this);
    }
    return orElse();
  }
}

abstract class _DataTransferInProgress implements SavedProductWatcherState {
  const factory _DataTransferInProgress() = _$_DataTransferInProgress;
}

/// @nodoc
abstract class _$LoadFailureCopyWith<$Res> {
  factory _$LoadFailureCopyWith(
          _LoadFailure value, $Res Function(_LoadFailure) then) =
      __$LoadFailureCopyWithImpl<$Res>;
  $Res call({InfraFailure<dynamic> failure});

  $InfraFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$LoadFailureCopyWithImpl<$Res>
    extends _$SavedProductWatcherStateCopyWithImpl<$Res>
    implements _$LoadFailureCopyWith<$Res> {
  __$LoadFailureCopyWithImpl(
      _LoadFailure _value, $Res Function(_LoadFailure) _then)
      : super(_value, (v) => _then(v as _LoadFailure));

  @override
  _LoadFailure get _value => super._value as _LoadFailure;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_LoadFailure(
      failure == freezed ? _value.failure : failure as InfraFailure<dynamic>,
    ));
  }

  @override
  $InfraFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $InfraFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_LoadFailure implements _LoadFailure {
  const _$_LoadFailure(this.failure) : assert(failure != null);

  @override
  final InfraFailure<dynamic> failure;

  @override
  String toString() {
    return 'SavedProductWatcherState.loadFailure(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadFailure &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$LoadFailureCopyWith<_LoadFailure> get copyWith =>
      __$LoadFailureCopyWithImpl<_LoadFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required
        TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadFailure(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(this);
    }
    return orElse();
  }
}

abstract class _LoadFailure implements SavedProductWatcherState {
  const factory _LoadFailure(InfraFailure<dynamic> failure) = _$_LoadFailure;

  InfraFailure<dynamic> get failure;
  _$LoadFailureCopyWith<_LoadFailure> get copyWith;
}

/// @nodoc
abstract class _$LoadSuccessCopyWith<$Res> {
  factory _$LoadSuccessCopyWith(
          _LoadSuccess value, $Res Function(_LoadSuccess) then) =
      __$LoadSuccessCopyWithImpl<$Res>;
  $Res call({SavedProducts savedProducts, OrganikartUser user});

  $SavedProductsCopyWith<$Res> get savedProducts;
  $OrganikartUserCopyWith<$Res> get user;
}

/// @nodoc
class __$LoadSuccessCopyWithImpl<$Res>
    extends _$SavedProductWatcherStateCopyWithImpl<$Res>
    implements _$LoadSuccessCopyWith<$Res> {
  __$LoadSuccessCopyWithImpl(
      _LoadSuccess _value, $Res Function(_LoadSuccess) _then)
      : super(_value, (v) => _then(v as _LoadSuccess));

  @override
  _LoadSuccess get _value => super._value as _LoadSuccess;

  @override
  $Res call({
    Object savedProducts = freezed,
    Object user = freezed,
  }) {
    return _then(_LoadSuccess(
      savedProducts == freezed
          ? _value.savedProducts
          : savedProducts as SavedProducts,
      user == freezed ? _value.user : user as OrganikartUser,
    ));
  }

  @override
  $SavedProductsCopyWith<$Res> get savedProducts {
    if (_value.savedProducts == null) {
      return null;
    }
    return $SavedProductsCopyWith<$Res>(_value.savedProducts, (value) {
      return _then(_value.copyWith(savedProducts: value));
    });
  }

  @override
  $OrganikartUserCopyWith<$Res> get user {
    if (_value.user == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc
class _$_LoadSuccess implements _LoadSuccess {
  const _$_LoadSuccess(this.savedProducts, this.user)
      : assert(savedProducts != null),
        assert(user != null);

  @override
  final SavedProducts savedProducts;
  @override
  final OrganikartUser user;

  @override
  String toString() {
    return 'SavedProductWatcherState.loadSuccess(savedProducts: $savedProducts, user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadSuccess &&
            (identical(other.savedProducts, savedProducts) ||
                const DeepCollectionEquality()
                    .equals(other.savedProducts, savedProducts)) &&
            (identical(other.user, user) ||
                const DeepCollectionEquality().equals(other.user, user)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(savedProducts) ^
      const DeepCollectionEquality().hash(user);

  @override
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith =>
      __$LoadSuccessCopyWithImpl<_LoadSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required
        TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadSuccess(savedProducts, user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(SavedProducts savedProducts, OrganikartUser user),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(savedProducts, user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(this);
    }
    return orElse();
  }
}

abstract class _LoadSuccess implements SavedProductWatcherState {
  const factory _LoadSuccess(SavedProducts savedProducts, OrganikartUser user) =
      _$_LoadSuccess;

  SavedProducts get savedProducts;
  OrganikartUser get user;
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith;
}
