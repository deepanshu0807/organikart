part of 'saved_product_watcher_bloc.dart';

@freezed
abstract class SavedProductWatcherState with _$SavedProductWatcherState {
  const factory SavedProductWatcherState.initial() = _Initial;
  const factory SavedProductWatcherState.loadInProgress() =
      _DataTransferInProgress;

  const factory SavedProductWatcherState.loadFailure(InfraFailure failure) =
      _LoadFailure;
  const factory SavedProductWatcherState.loadSuccess(
    SavedProducts savedProducts,
    OrganikartUser user,
  ) = _LoadSuccess;
}
