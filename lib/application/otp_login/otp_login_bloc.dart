import 'dart:async';

import 'package:bloc/bloc.dart';

import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/auth/otp_process_state.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'otp_login_event.dart';
part 'otp_login_state.dart';
part 'otp_login_bloc.freezed.dart';

@injectable
class OtpLoginBloc extends Bloc<OtpLoginEvent, OtpLoginState> {
  final IAuth _iAuth;
  OtpLoginBloc(this._iAuth) : super(OtpLoginState.initial());

  @override
  Stream<OtpLoginState> mapEventToState(
    OtpLoginEvent event,
  ) async* {
    yield* event.map(
      nameChanged: (e) async* {
        yield state.copyWith(
          nameOption: some(Name(e.name)),
          otp: OTP(""),
          verId: "",
          state: const OTPProcessState.initial(),
          authFailureOrSuccessOption: none(),
        );
      },
      phoneNumberChanged: (e) async* {
        yield state.copyWith(
          phoneNumber: PhoneNumber(e.phone),
          otp: OTP(""),
          verId: "",
          state: const OTPProcessState.initial(),
          authFailureOrSuccessOption: none(),
        );
      },
      submitPhoneNumberForOTP: (e) async* {
        yield state.copyWith(
          otp: OTP(""),
          verId: "",
          showErrorMessages: true,
          state: const OTPProcessState.phoneNumberSubmiting(),
          authFailureOrSuccessOption: none(),
        );

        // final result = await _iAuth.loginwithPhoneNumber(
        //   phoneNumber: state.phoneNumber,
        //   onCodeSent: codeSent,
        //   onCodeTimeOut: codeAutoRetrievalTimeout,
        //   onAutoVerificationFailed: verificationFailed,
        //   onAutoVerification: verificationCompleted,
        // );

        // // print(result);
        // yield result.fold(
        //   (l) {
        //     return state.copyWith(
        //       otp: OTP(""),
        //       verId: "",
        //       showErrorMessages: true,
        //       state: OTPProcessState.phoneNumberSubmitFailed(l),
        //       authFailureOrSuccessOption: some(left(l)),
        //     );
        //   },
        //   (r) {
        //     return state.copyWith(
        //       otp: OTP(""),
        //       verId: "",
        //       showErrorMessages: true,
        //       state: const OTPProcessState.phoneNumberSubmitSuccess(),
        //       authFailureOrSuccessOption: none(),
        //     );
        //   },
        // );
      },
      submitPhoneNumberForOTPSuccess: (e) async* {
        yield state.copyWith(
          state: const OTPProcessState.waitingForOTP(),
          otp: OTP(""),
          verId: e.verId,
          authFailureOrSuccessOption: none(),
        );
      },
      submitPhoneNumberForOTPFailed: (e) async* {
        yield state.copyWith(
          state: OTPProcessState.phoneNumberSubmitFailed(e.failure),
          otp: OTP(""),
          verId: "",
          authFailureOrSuccessOption: none(),
        );
      },
      autoVerifiedSuccess: (e) async* {
        state.nameOption.fold(
          () {},
          (a) {
            if (a.isValid()) {
              _iAuth.updateUserName(name: a);
            }
          },
        );
        yield state.copyWith(
          showErrorMessages: true,
          state: const OTPProcessState.otpVerified(),
          authFailureOrSuccessOption: some(right(unit)),
        );

        Future.delayed(const Duration(seconds: 2), () {
          add(const OtpLoginEvent.initialize());
        });
      },
      codeAutoRetrievalTimeout: (e) async* {
        yield state.copyWith(
          state: const OTPProcessState.waitingForOTPExpired(),
          verId: e.verId,
          authFailureOrSuccessOption: none(),
        );
      },
      otpChanged: (e) async* {
        yield state.copyWith(
          otp: OTP(e.otp),
          authFailureOrSuccessOption: none(),
        );
      },
      submitOTP: (e) async* {
        yield state.copyWith(
          otp: OTP(""),
          showErrorMessages: true,
          state: const OTPProcessState.submitingOTP(),
          authFailureOrSuccessOption: none(),
        );

        final result = await _iAuth.verifyOTP(
            verId: state.verId, otp: state.otp.getOrElse(""));

        // // print(result);
        yield result.fold(
          (l) {
            return state.copyWith(
              showErrorMessages: true,
              state: OTPProcessState.otpVerificationFailed(l),
              authFailureOrSuccessOption: some(left(l)),
            );
          },
          (r) {
            state.nameOption.fold(
              () {},
              (a) {
                if (a.isValid()) {
                  _iAuth.updateUserName(name: a);
                }
              },
            );
            return state.copyWith(
              showErrorMessages: true,
              state: const OTPProcessState.otpVerified(),
              authFailureOrSuccessOption: some(right(unit)),
            );
          },
        );

        Future.delayed(const Duration(seconds: 2), () {
          add(const OtpLoginEvent.initialize());
        });
      },
      initialize: (e) async* {
        yield OtpLoginState.initial();
      },
    );
  }

  Function(OrganikartUser user) get verificationCompleted => (user) {
        add(const OtpLoginEvent.autoVerifiedSuccess());
      };
  Function(AuthFailure failure) get verificationFailed => (failure) {
        add(OtpLoginEvent.submitPhoneNumberForOTPFailed(failure));
      };
  Function(String verCode) get codeSent => (v) {
        add(OtpLoginEvent.submitPhoneNumberForOTPSuccess(v));
      };
  Function(String verCode) get codeAutoRetrievalTimeout => (v) {
        add(OtpLoginEvent.codeAutoRetrievalTimeout(v));
      };
}
