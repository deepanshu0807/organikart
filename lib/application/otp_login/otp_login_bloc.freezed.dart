// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'otp_login_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$OtpLoginEventTearOff {
  const _$OtpLoginEventTearOff();

// ignore: unused_element
  _NameChanged nameChanged(String name) {
    return _NameChanged(
      name,
    );
  }

// ignore: unused_element
  _PhoneNumberChanged phoneNumberChanged(String phone) {
    return _PhoneNumberChanged(
      phone,
    );
  }

// ignore: unused_element
  _SubmitPhoneNumberForOTP submitPhoneNumberForOTP() {
    return const _SubmitPhoneNumberForOTP();
  }

// ignore: unused_element
  _SubmitPhoneNumberForOTPSuccess submitPhoneNumberForOTPSuccess(String verId) {
    return _SubmitPhoneNumberForOTPSuccess(
      verId,
    );
  }

// ignore: unused_element
  _SubmitPhoneNumberForOTPFailed submitPhoneNumberForOTPFailed(
      AuthFailure<dynamic> failure) {
    return _SubmitPhoneNumberForOTPFailed(
      failure,
    );
  }

// ignore: unused_element
  _AutoVerifiedSuccess autoVerifiedSuccess() {
    return const _AutoVerifiedSuccess();
  }

// ignore: unused_element
  _CodeAutoRetrievalTimeout codeAutoRetrievalTimeout(String verId) {
    return _CodeAutoRetrievalTimeout(
      verId,
    );
  }

// ignore: unused_element
  _OtpChanged otpChanged(String otp) {
    return _OtpChanged(
      otp,
    );
  }

// ignore: unused_element
  _SubmitOTP submitOTP() {
    return const _SubmitOTP();
  }

// ignore: unused_element
  _Initialize initialize() {
    return const _Initialize();
  }
}

/// @nodoc
// ignore: unused_element
const $OtpLoginEvent = _$OtpLoginEventTearOff();

/// @nodoc
mixin _$OtpLoginEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $OtpLoginEventCopyWith<$Res> {
  factory $OtpLoginEventCopyWith(
          OtpLoginEvent value, $Res Function(OtpLoginEvent) then) =
      _$OtpLoginEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$OtpLoginEventCopyWithImpl<$Res>
    implements $OtpLoginEventCopyWith<$Res> {
  _$OtpLoginEventCopyWithImpl(this._value, this._then);

  final OtpLoginEvent _value;
  // ignore: unused_field
  final $Res Function(OtpLoginEvent) _then;
}

/// @nodoc
abstract class _$NameChangedCopyWith<$Res> {
  factory _$NameChangedCopyWith(
          _NameChanged value, $Res Function(_NameChanged) then) =
      __$NameChangedCopyWithImpl<$Res>;
  $Res call({String name});
}

/// @nodoc
class __$NameChangedCopyWithImpl<$Res> extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$NameChangedCopyWith<$Res> {
  __$NameChangedCopyWithImpl(
      _NameChanged _value, $Res Function(_NameChanged) _then)
      : super(_value, (v) => _then(v as _NameChanged));

  @override
  _NameChanged get _value => super._value as _NameChanged;

  @override
  $Res call({
    Object name = freezed,
  }) {
    return _then(_NameChanged(
      name == freezed ? _value.name : name as String,
    ));
  }
}

/// @nodoc
class _$_NameChanged implements _NameChanged {
  const _$_NameChanged(this.name) : assert(name != null);

  @override
  final String name;

  @override
  String toString() {
    return 'OtpLoginEvent.nameChanged(name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NameChanged &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(name);

  @override
  _$NameChangedCopyWith<_NameChanged> get copyWith =>
      __$NameChangedCopyWithImpl<_NameChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return nameChanged(name);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(name);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return nameChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (nameChanged != null) {
      return nameChanged(this);
    }
    return orElse();
  }
}

abstract class _NameChanged implements OtpLoginEvent {
  const factory _NameChanged(String name) = _$_NameChanged;

  String get name;
  _$NameChangedCopyWith<_NameChanged> get copyWith;
}

/// @nodoc
abstract class _$PhoneNumberChangedCopyWith<$Res> {
  factory _$PhoneNumberChangedCopyWith(
          _PhoneNumberChanged value, $Res Function(_PhoneNumberChanged) then) =
      __$PhoneNumberChangedCopyWithImpl<$Res>;
  $Res call({String phone});
}

/// @nodoc
class __$PhoneNumberChangedCopyWithImpl<$Res>
    extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$PhoneNumberChangedCopyWith<$Res> {
  __$PhoneNumberChangedCopyWithImpl(
      _PhoneNumberChanged _value, $Res Function(_PhoneNumberChanged) _then)
      : super(_value, (v) => _then(v as _PhoneNumberChanged));

  @override
  _PhoneNumberChanged get _value => super._value as _PhoneNumberChanged;

  @override
  $Res call({
    Object phone = freezed,
  }) {
    return _then(_PhoneNumberChanged(
      phone == freezed ? _value.phone : phone as String,
    ));
  }
}

/// @nodoc
class _$_PhoneNumberChanged implements _PhoneNumberChanged {
  const _$_PhoneNumberChanged(this.phone) : assert(phone != null);

  @override
  final String phone;

  @override
  String toString() {
    return 'OtpLoginEvent.phoneNumberChanged(phone: $phone)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PhoneNumberChanged &&
            (identical(other.phone, phone) ||
                const DeepCollectionEquality().equals(other.phone, phone)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(phone);

  @override
  _$PhoneNumberChangedCopyWith<_PhoneNumberChanged> get copyWith =>
      __$PhoneNumberChangedCopyWithImpl<_PhoneNumberChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return phoneNumberChanged(phone);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberChanged != null) {
      return phoneNumberChanged(phone);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return phoneNumberChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (phoneNumberChanged != null) {
      return phoneNumberChanged(this);
    }
    return orElse();
  }
}

abstract class _PhoneNumberChanged implements OtpLoginEvent {
  const factory _PhoneNumberChanged(String phone) = _$_PhoneNumberChanged;

  String get phone;
  _$PhoneNumberChangedCopyWith<_PhoneNumberChanged> get copyWith;
}

/// @nodoc
abstract class _$SubmitPhoneNumberForOTPCopyWith<$Res> {
  factory _$SubmitPhoneNumberForOTPCopyWith(_SubmitPhoneNumberForOTP value,
          $Res Function(_SubmitPhoneNumberForOTP) then) =
      __$SubmitPhoneNumberForOTPCopyWithImpl<$Res>;
}

/// @nodoc
class __$SubmitPhoneNumberForOTPCopyWithImpl<$Res>
    extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$SubmitPhoneNumberForOTPCopyWith<$Res> {
  __$SubmitPhoneNumberForOTPCopyWithImpl(_SubmitPhoneNumberForOTP _value,
      $Res Function(_SubmitPhoneNumberForOTP) _then)
      : super(_value, (v) => _then(v as _SubmitPhoneNumberForOTP));

  @override
  _SubmitPhoneNumberForOTP get _value =>
      super._value as _SubmitPhoneNumberForOTP;
}

/// @nodoc
class _$_SubmitPhoneNumberForOTP implements _SubmitPhoneNumberForOTP {
  const _$_SubmitPhoneNumberForOTP();

  @override
  String toString() {
    return 'OtpLoginEvent.submitPhoneNumberForOTP()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SubmitPhoneNumberForOTP);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitPhoneNumberForOTP();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitPhoneNumberForOTP != null) {
      return submitPhoneNumberForOTP();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitPhoneNumberForOTP(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitPhoneNumberForOTP != null) {
      return submitPhoneNumberForOTP(this);
    }
    return orElse();
  }
}

abstract class _SubmitPhoneNumberForOTP implements OtpLoginEvent {
  const factory _SubmitPhoneNumberForOTP() = _$_SubmitPhoneNumberForOTP;
}

/// @nodoc
abstract class _$SubmitPhoneNumberForOTPSuccessCopyWith<$Res> {
  factory _$SubmitPhoneNumberForOTPSuccessCopyWith(
          _SubmitPhoneNumberForOTPSuccess value,
          $Res Function(_SubmitPhoneNumberForOTPSuccess) then) =
      __$SubmitPhoneNumberForOTPSuccessCopyWithImpl<$Res>;
  $Res call({String verId});
}

/// @nodoc
class __$SubmitPhoneNumberForOTPSuccessCopyWithImpl<$Res>
    extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$SubmitPhoneNumberForOTPSuccessCopyWith<$Res> {
  __$SubmitPhoneNumberForOTPSuccessCopyWithImpl(
      _SubmitPhoneNumberForOTPSuccess _value,
      $Res Function(_SubmitPhoneNumberForOTPSuccess) _then)
      : super(_value, (v) => _then(v as _SubmitPhoneNumberForOTPSuccess));

  @override
  _SubmitPhoneNumberForOTPSuccess get _value =>
      super._value as _SubmitPhoneNumberForOTPSuccess;

  @override
  $Res call({
    Object verId = freezed,
  }) {
    return _then(_SubmitPhoneNumberForOTPSuccess(
      verId == freezed ? _value.verId : verId as String,
    ));
  }
}

/// @nodoc
class _$_SubmitPhoneNumberForOTPSuccess
    implements _SubmitPhoneNumberForOTPSuccess {
  const _$_SubmitPhoneNumberForOTPSuccess(this.verId) : assert(verId != null);

  @override
  final String verId;

  @override
  String toString() {
    return 'OtpLoginEvent.submitPhoneNumberForOTPSuccess(verId: $verId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SubmitPhoneNumberForOTPSuccess &&
            (identical(other.verId, verId) ||
                const DeepCollectionEquality().equals(other.verId, verId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(verId);

  @override
  _$SubmitPhoneNumberForOTPSuccessCopyWith<_SubmitPhoneNumberForOTPSuccess>
      get copyWith => __$SubmitPhoneNumberForOTPSuccessCopyWithImpl<
          _SubmitPhoneNumberForOTPSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitPhoneNumberForOTPSuccess(verId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitPhoneNumberForOTPSuccess != null) {
      return submitPhoneNumberForOTPSuccess(verId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitPhoneNumberForOTPSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitPhoneNumberForOTPSuccess != null) {
      return submitPhoneNumberForOTPSuccess(this);
    }
    return orElse();
  }
}

abstract class _SubmitPhoneNumberForOTPSuccess implements OtpLoginEvent {
  const factory _SubmitPhoneNumberForOTPSuccess(String verId) =
      _$_SubmitPhoneNumberForOTPSuccess;

  String get verId;
  _$SubmitPhoneNumberForOTPSuccessCopyWith<_SubmitPhoneNumberForOTPSuccess>
      get copyWith;
}

/// @nodoc
abstract class _$SubmitPhoneNumberForOTPFailedCopyWith<$Res> {
  factory _$SubmitPhoneNumberForOTPFailedCopyWith(
          _SubmitPhoneNumberForOTPFailed value,
          $Res Function(_SubmitPhoneNumberForOTPFailed) then) =
      __$SubmitPhoneNumberForOTPFailedCopyWithImpl<$Res>;
  $Res call({AuthFailure<dynamic> failure});

  $AuthFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$SubmitPhoneNumberForOTPFailedCopyWithImpl<$Res>
    extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$SubmitPhoneNumberForOTPFailedCopyWith<$Res> {
  __$SubmitPhoneNumberForOTPFailedCopyWithImpl(
      _SubmitPhoneNumberForOTPFailed _value,
      $Res Function(_SubmitPhoneNumberForOTPFailed) _then)
      : super(_value, (v) => _then(v as _SubmitPhoneNumberForOTPFailed));

  @override
  _SubmitPhoneNumberForOTPFailed get _value =>
      super._value as _SubmitPhoneNumberForOTPFailed;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_SubmitPhoneNumberForOTPFailed(
      failure == freezed ? _value.failure : failure as AuthFailure<dynamic>,
    ));
  }

  @override
  $AuthFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $AuthFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_SubmitPhoneNumberForOTPFailed
    implements _SubmitPhoneNumberForOTPFailed {
  const _$_SubmitPhoneNumberForOTPFailed(this.failure)
      : assert(failure != null);

  @override
  final AuthFailure<dynamic> failure;

  @override
  String toString() {
    return 'OtpLoginEvent.submitPhoneNumberForOTPFailed(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SubmitPhoneNumberForOTPFailed &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$SubmitPhoneNumberForOTPFailedCopyWith<_SubmitPhoneNumberForOTPFailed>
      get copyWith => __$SubmitPhoneNumberForOTPFailedCopyWithImpl<
          _SubmitPhoneNumberForOTPFailed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitPhoneNumberForOTPFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitPhoneNumberForOTPFailed != null) {
      return submitPhoneNumberForOTPFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitPhoneNumberForOTPFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitPhoneNumberForOTPFailed != null) {
      return submitPhoneNumberForOTPFailed(this);
    }
    return orElse();
  }
}

abstract class _SubmitPhoneNumberForOTPFailed implements OtpLoginEvent {
  const factory _SubmitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure) =
      _$_SubmitPhoneNumberForOTPFailed;

  AuthFailure<dynamic> get failure;
  _$SubmitPhoneNumberForOTPFailedCopyWith<_SubmitPhoneNumberForOTPFailed>
      get copyWith;
}

/// @nodoc
abstract class _$AutoVerifiedSuccessCopyWith<$Res> {
  factory _$AutoVerifiedSuccessCopyWith(_AutoVerifiedSuccess value,
          $Res Function(_AutoVerifiedSuccess) then) =
      __$AutoVerifiedSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$AutoVerifiedSuccessCopyWithImpl<$Res>
    extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$AutoVerifiedSuccessCopyWith<$Res> {
  __$AutoVerifiedSuccessCopyWithImpl(
      _AutoVerifiedSuccess _value, $Res Function(_AutoVerifiedSuccess) _then)
      : super(_value, (v) => _then(v as _AutoVerifiedSuccess));

  @override
  _AutoVerifiedSuccess get _value => super._value as _AutoVerifiedSuccess;
}

/// @nodoc
class _$_AutoVerifiedSuccess implements _AutoVerifiedSuccess {
  const _$_AutoVerifiedSuccess();

  @override
  String toString() {
    return 'OtpLoginEvent.autoVerifiedSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _AutoVerifiedSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return autoVerifiedSuccess();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (autoVerifiedSuccess != null) {
      return autoVerifiedSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return autoVerifiedSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (autoVerifiedSuccess != null) {
      return autoVerifiedSuccess(this);
    }
    return orElse();
  }
}

abstract class _AutoVerifiedSuccess implements OtpLoginEvent {
  const factory _AutoVerifiedSuccess() = _$_AutoVerifiedSuccess;
}

/// @nodoc
abstract class _$CodeAutoRetrievalTimeoutCopyWith<$Res> {
  factory _$CodeAutoRetrievalTimeoutCopyWith(_CodeAutoRetrievalTimeout value,
          $Res Function(_CodeAutoRetrievalTimeout) then) =
      __$CodeAutoRetrievalTimeoutCopyWithImpl<$Res>;
  $Res call({String verId});
}

/// @nodoc
class __$CodeAutoRetrievalTimeoutCopyWithImpl<$Res>
    extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$CodeAutoRetrievalTimeoutCopyWith<$Res> {
  __$CodeAutoRetrievalTimeoutCopyWithImpl(_CodeAutoRetrievalTimeout _value,
      $Res Function(_CodeAutoRetrievalTimeout) _then)
      : super(_value, (v) => _then(v as _CodeAutoRetrievalTimeout));

  @override
  _CodeAutoRetrievalTimeout get _value =>
      super._value as _CodeAutoRetrievalTimeout;

  @override
  $Res call({
    Object verId = freezed,
  }) {
    return _then(_CodeAutoRetrievalTimeout(
      verId == freezed ? _value.verId : verId as String,
    ));
  }
}

/// @nodoc
class _$_CodeAutoRetrievalTimeout implements _CodeAutoRetrievalTimeout {
  const _$_CodeAutoRetrievalTimeout(this.verId) : assert(verId != null);

  @override
  final String verId;

  @override
  String toString() {
    return 'OtpLoginEvent.codeAutoRetrievalTimeout(verId: $verId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _CodeAutoRetrievalTimeout &&
            (identical(other.verId, verId) ||
                const DeepCollectionEquality().equals(other.verId, verId)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(verId);

  @override
  _$CodeAutoRetrievalTimeoutCopyWith<_CodeAutoRetrievalTimeout> get copyWith =>
      __$CodeAutoRetrievalTimeoutCopyWithImpl<_CodeAutoRetrievalTimeout>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return codeAutoRetrievalTimeout(verId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (codeAutoRetrievalTimeout != null) {
      return codeAutoRetrievalTimeout(verId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return codeAutoRetrievalTimeout(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (codeAutoRetrievalTimeout != null) {
      return codeAutoRetrievalTimeout(this);
    }
    return orElse();
  }
}

abstract class _CodeAutoRetrievalTimeout implements OtpLoginEvent {
  const factory _CodeAutoRetrievalTimeout(String verId) =
      _$_CodeAutoRetrievalTimeout;

  String get verId;
  _$CodeAutoRetrievalTimeoutCopyWith<_CodeAutoRetrievalTimeout> get copyWith;
}

/// @nodoc
abstract class _$OtpChangedCopyWith<$Res> {
  factory _$OtpChangedCopyWith(
          _OtpChanged value, $Res Function(_OtpChanged) then) =
      __$OtpChangedCopyWithImpl<$Res>;
  $Res call({String otp});
}

/// @nodoc
class __$OtpChangedCopyWithImpl<$Res> extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$OtpChangedCopyWith<$Res> {
  __$OtpChangedCopyWithImpl(
      _OtpChanged _value, $Res Function(_OtpChanged) _then)
      : super(_value, (v) => _then(v as _OtpChanged));

  @override
  _OtpChanged get _value => super._value as _OtpChanged;

  @override
  $Res call({
    Object otp = freezed,
  }) {
    return _then(_OtpChanged(
      otp == freezed ? _value.otp : otp as String,
    ));
  }
}

/// @nodoc
class _$_OtpChanged implements _OtpChanged {
  const _$_OtpChanged(this.otp) : assert(otp != null);

  @override
  final String otp;

  @override
  String toString() {
    return 'OtpLoginEvent.otpChanged(otp: $otp)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OtpChanged &&
            (identical(other.otp, otp) ||
                const DeepCollectionEquality().equals(other.otp, otp)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(otp);

  @override
  _$OtpChangedCopyWith<_OtpChanged> get copyWith =>
      __$OtpChangedCopyWithImpl<_OtpChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return otpChanged(otp);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (otpChanged != null) {
      return otpChanged(otp);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return otpChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (otpChanged != null) {
      return otpChanged(this);
    }
    return orElse();
  }
}

abstract class _OtpChanged implements OtpLoginEvent {
  const factory _OtpChanged(String otp) = _$_OtpChanged;

  String get otp;
  _$OtpChangedCopyWith<_OtpChanged> get copyWith;
}

/// @nodoc
abstract class _$SubmitOTPCopyWith<$Res> {
  factory _$SubmitOTPCopyWith(
          _SubmitOTP value, $Res Function(_SubmitOTP) then) =
      __$SubmitOTPCopyWithImpl<$Res>;
}

/// @nodoc
class __$SubmitOTPCopyWithImpl<$Res> extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$SubmitOTPCopyWith<$Res> {
  __$SubmitOTPCopyWithImpl(_SubmitOTP _value, $Res Function(_SubmitOTP) _then)
      : super(_value, (v) => _then(v as _SubmitOTP));

  @override
  _SubmitOTP get _value => super._value as _SubmitOTP;
}

/// @nodoc
class _$_SubmitOTP implements _SubmitOTP {
  const _$_SubmitOTP();

  @override
  String toString() {
    return 'OtpLoginEvent.submitOTP()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _SubmitOTP);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitOTP();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitOTP != null) {
      return submitOTP();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return submitOTP(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (submitOTP != null) {
      return submitOTP(this);
    }
    return orElse();
  }
}

abstract class _SubmitOTP implements OtpLoginEvent {
  const factory _SubmitOTP() = _$_SubmitOTP;
}

/// @nodoc
abstract class _$InitializeCopyWith<$Res> {
  factory _$InitializeCopyWith(
          _Initialize value, $Res Function(_Initialize) then) =
      __$InitializeCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitializeCopyWithImpl<$Res> extends _$OtpLoginEventCopyWithImpl<$Res>
    implements _$InitializeCopyWith<$Res> {
  __$InitializeCopyWithImpl(
      _Initialize _value, $Res Function(_Initialize) _then)
      : super(_value, (v) => _then(v as _Initialize));

  @override
  _Initialize get _value => super._value as _Initialize;
}

/// @nodoc
class _$_Initialize implements _Initialize {
  const _$_Initialize();

  @override
  String toString() {
    return 'OtpLoginEvent.initialize()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initialize);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult nameChanged(String name),
    @required TResult phoneNumberChanged(String phone),
    @required TResult submitPhoneNumberForOTP(),
    @required TResult submitPhoneNumberForOTPSuccess(String verId),
    @required
        TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    @required TResult autoVerifiedSuccess(),
    @required TResult codeAutoRetrievalTimeout(String verId),
    @required TResult otpChanged(String otp),
    @required TResult submitOTP(),
    @required TResult initialize(),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return initialize();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult nameChanged(String name),
    TResult phoneNumberChanged(String phone),
    TResult submitPhoneNumberForOTP(),
    TResult submitPhoneNumberForOTPSuccess(String verId),
    TResult submitPhoneNumberForOTPFailed(AuthFailure<dynamic> failure),
    TResult autoVerifiedSuccess(),
    TResult codeAutoRetrievalTimeout(String verId),
    TResult otpChanged(String otp),
    TResult submitOTP(),
    TResult initialize(),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult nameChanged(_NameChanged value),
    @required TResult phoneNumberChanged(_PhoneNumberChanged value),
    @required TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    @required
        TResult submitPhoneNumberForOTPSuccess(
            _SubmitPhoneNumberForOTPSuccess value),
    @required
        TResult submitPhoneNumberForOTPFailed(
            _SubmitPhoneNumberForOTPFailed value),
    @required TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    @required TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    @required TResult otpChanged(_OtpChanged value),
    @required TResult submitOTP(_SubmitOTP value),
    @required TResult initialize(_Initialize value),
  }) {
    assert(nameChanged != null);
    assert(phoneNumberChanged != null);
    assert(submitPhoneNumberForOTP != null);
    assert(submitPhoneNumberForOTPSuccess != null);
    assert(submitPhoneNumberForOTPFailed != null);
    assert(autoVerifiedSuccess != null);
    assert(codeAutoRetrievalTimeout != null);
    assert(otpChanged != null);
    assert(submitOTP != null);
    assert(initialize != null);
    return initialize(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult nameChanged(_NameChanged value),
    TResult phoneNumberChanged(_PhoneNumberChanged value),
    TResult submitPhoneNumberForOTP(_SubmitPhoneNumberForOTP value),
    TResult submitPhoneNumberForOTPSuccess(
        _SubmitPhoneNumberForOTPSuccess value),
    TResult submitPhoneNumberForOTPFailed(_SubmitPhoneNumberForOTPFailed value),
    TResult autoVerifiedSuccess(_AutoVerifiedSuccess value),
    TResult codeAutoRetrievalTimeout(_CodeAutoRetrievalTimeout value),
    TResult otpChanged(_OtpChanged value),
    TResult submitOTP(_SubmitOTP value),
    TResult initialize(_Initialize value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initialize != null) {
      return initialize(this);
    }
    return orElse();
  }
}

abstract class _Initialize implements OtpLoginEvent {
  const factory _Initialize() = _$_Initialize;
}

/// @nodoc
class _$OtpLoginStateTearOff {
  const _$OtpLoginStateTearOff();

// ignore: unused_element
  _OtpLoginState call(
      {@required Option<Name> nameOption,
      @required PhoneNumber phoneNumber,
      @required OTP otp,
      @required String verId,
      @required bool showErrorMessages,
      @required OTPProcessState<dynamic> state,
      @required Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption}) {
    return _OtpLoginState(
      nameOption: nameOption,
      phoneNumber: phoneNumber,
      otp: otp,
      verId: verId,
      showErrorMessages: showErrorMessages,
      state: state,
      authFailureOrSuccessOption: authFailureOrSuccessOption,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $OtpLoginState = _$OtpLoginStateTearOff();

/// @nodoc
mixin _$OtpLoginState {
  Option<Name> get nameOption;
  PhoneNumber get phoneNumber;
  OTP get otp;
  String get verId;
  bool get showErrorMessages;
  OTPProcessState<dynamic> get state;
  Option<Either<AuthFailure, Unit>> get authFailureOrSuccessOption;

  $OtpLoginStateCopyWith<OtpLoginState> get copyWith;
}

/// @nodoc
abstract class $OtpLoginStateCopyWith<$Res> {
  factory $OtpLoginStateCopyWith(
          OtpLoginState value, $Res Function(OtpLoginState) then) =
      _$OtpLoginStateCopyWithImpl<$Res>;
  $Res call(
      {Option<Name> nameOption,
      PhoneNumber phoneNumber,
      OTP otp,
      String verId,
      bool showErrorMessages,
      OTPProcessState<dynamic> state,
      Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption});

  $OTPProcessStateCopyWith<dynamic, $Res> get state;
}

/// @nodoc
class _$OtpLoginStateCopyWithImpl<$Res>
    implements $OtpLoginStateCopyWith<$Res> {
  _$OtpLoginStateCopyWithImpl(this._value, this._then);

  final OtpLoginState _value;
  // ignore: unused_field
  final $Res Function(OtpLoginState) _then;

  @override
  $Res call({
    Object nameOption = freezed,
    Object phoneNumber = freezed,
    Object otp = freezed,
    Object verId = freezed,
    Object showErrorMessages = freezed,
    Object state = freezed,
    Object authFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      nameOption: nameOption == freezed
          ? _value.nameOption
          : nameOption as Option<Name>,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber as PhoneNumber,
      otp: otp == freezed ? _value.otp : otp as OTP,
      verId: verId == freezed ? _value.verId : verId as String,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages as bool,
      state:
          state == freezed ? _value.state : state as OTPProcessState<dynamic>,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption as Option<Either<AuthFailure, Unit>>,
    ));
  }

  @override
  $OTPProcessStateCopyWith<dynamic, $Res> get state {
    if (_value.state == null) {
      return null;
    }
    return $OTPProcessStateCopyWith<dynamic, $Res>(_value.state, (value) {
      return _then(_value.copyWith(state: value));
    });
  }
}

/// @nodoc
abstract class _$OtpLoginStateCopyWith<$Res>
    implements $OtpLoginStateCopyWith<$Res> {
  factory _$OtpLoginStateCopyWith(
          _OtpLoginState value, $Res Function(_OtpLoginState) then) =
      __$OtpLoginStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Option<Name> nameOption,
      PhoneNumber phoneNumber,
      OTP otp,
      String verId,
      bool showErrorMessages,
      OTPProcessState<dynamic> state,
      Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption});

  @override
  $OTPProcessStateCopyWith<dynamic, $Res> get state;
}

/// @nodoc
class __$OtpLoginStateCopyWithImpl<$Res>
    extends _$OtpLoginStateCopyWithImpl<$Res>
    implements _$OtpLoginStateCopyWith<$Res> {
  __$OtpLoginStateCopyWithImpl(
      _OtpLoginState _value, $Res Function(_OtpLoginState) _then)
      : super(_value, (v) => _then(v as _OtpLoginState));

  @override
  _OtpLoginState get _value => super._value as _OtpLoginState;

  @override
  $Res call({
    Object nameOption = freezed,
    Object phoneNumber = freezed,
    Object otp = freezed,
    Object verId = freezed,
    Object showErrorMessages = freezed,
    Object state = freezed,
    Object authFailureOrSuccessOption = freezed,
  }) {
    return _then(_OtpLoginState(
      nameOption: nameOption == freezed
          ? _value.nameOption
          : nameOption as Option<Name>,
      phoneNumber: phoneNumber == freezed
          ? _value.phoneNumber
          : phoneNumber as PhoneNumber,
      otp: otp == freezed ? _value.otp : otp as OTP,
      verId: verId == freezed ? _value.verId : verId as String,
      showErrorMessages: showErrorMessages == freezed
          ? _value.showErrorMessages
          : showErrorMessages as bool,
      state:
          state == freezed ? _value.state : state as OTPProcessState<dynamic>,
      authFailureOrSuccessOption: authFailureOrSuccessOption == freezed
          ? _value.authFailureOrSuccessOption
          : authFailureOrSuccessOption as Option<Either<AuthFailure, Unit>>,
    ));
  }
}

/// @nodoc
class _$_OtpLoginState implements _OtpLoginState {
  const _$_OtpLoginState(
      {@required this.nameOption,
      @required this.phoneNumber,
      @required this.otp,
      @required this.verId,
      @required this.showErrorMessages,
      @required this.state,
      @required this.authFailureOrSuccessOption})
      : assert(nameOption != null),
        assert(phoneNumber != null),
        assert(otp != null),
        assert(verId != null),
        assert(showErrorMessages != null),
        assert(state != null),
        assert(authFailureOrSuccessOption != null);

  @override
  final Option<Name> nameOption;
  @override
  final PhoneNumber phoneNumber;
  @override
  final OTP otp;
  @override
  final String verId;
  @override
  final bool showErrorMessages;
  @override
  final OTPProcessState<dynamic> state;
  @override
  final Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption;

  @override
  String toString() {
    return 'OtpLoginState(nameOption: $nameOption, phoneNumber: $phoneNumber, otp: $otp, verId: $verId, showErrorMessages: $showErrorMessages, state: $state, authFailureOrSuccessOption: $authFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _OtpLoginState &&
            (identical(other.nameOption, nameOption) ||
                const DeepCollectionEquality()
                    .equals(other.nameOption, nameOption)) &&
            (identical(other.phoneNumber, phoneNumber) ||
                const DeepCollectionEquality()
                    .equals(other.phoneNumber, phoneNumber)) &&
            (identical(other.otp, otp) ||
                const DeepCollectionEquality().equals(other.otp, otp)) &&
            (identical(other.verId, verId) ||
                const DeepCollectionEquality().equals(other.verId, verId)) &&
            (identical(other.showErrorMessages, showErrorMessages) ||
                const DeepCollectionEquality()
                    .equals(other.showErrorMessages, showErrorMessages)) &&
            (identical(other.state, state) ||
                const DeepCollectionEquality().equals(other.state, state)) &&
            (identical(other.authFailureOrSuccessOption,
                    authFailureOrSuccessOption) ||
                const DeepCollectionEquality().equals(
                    other.authFailureOrSuccessOption,
                    authFailureOrSuccessOption)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(nameOption) ^
      const DeepCollectionEquality().hash(phoneNumber) ^
      const DeepCollectionEquality().hash(otp) ^
      const DeepCollectionEquality().hash(verId) ^
      const DeepCollectionEquality().hash(showErrorMessages) ^
      const DeepCollectionEquality().hash(state) ^
      const DeepCollectionEquality().hash(authFailureOrSuccessOption);

  @override
  _$OtpLoginStateCopyWith<_OtpLoginState> get copyWith =>
      __$OtpLoginStateCopyWithImpl<_OtpLoginState>(this, _$identity);
}

abstract class _OtpLoginState implements OtpLoginState {
  const factory _OtpLoginState(
          {@required
              Option<Name> nameOption,
          @required
              PhoneNumber phoneNumber,
          @required
              OTP otp,
          @required
              String verId,
          @required
              bool showErrorMessages,
          @required
              OTPProcessState<dynamic> state,
          @required
              Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption}) =
      _$_OtpLoginState;

  @override
  Option<Name> get nameOption;
  @override
  PhoneNumber get phoneNumber;
  @override
  OTP get otp;
  @override
  String get verId;
  @override
  bool get showErrorMessages;
  @override
  OTPProcessState<dynamic> get state;
  @override
  Option<Either<AuthFailure, Unit>> get authFailureOrSuccessOption;
  @override
  _$OtpLoginStateCopyWith<_OtpLoginState> get copyWith;
}
