part of 'otp_login_bloc.dart';

@freezed
abstract class OtpLoginEvent with _$OtpLoginEvent {
  const factory OtpLoginEvent.nameChanged(String name) = _NameChanged;
  const factory OtpLoginEvent.phoneNumberChanged(String phone) =
      _PhoneNumberChanged;

  const factory OtpLoginEvent.submitPhoneNumberForOTP() =
      _SubmitPhoneNumberForOTP;
  const factory OtpLoginEvent.submitPhoneNumberForOTPSuccess(String verId) =
      _SubmitPhoneNumberForOTPSuccess;
  const factory OtpLoginEvent.submitPhoneNumberForOTPFailed(
      AuthFailure failure) = _SubmitPhoneNumberForOTPFailed;

  const factory OtpLoginEvent.autoVerifiedSuccess() = _AutoVerifiedSuccess;
  const factory OtpLoginEvent.codeAutoRetrievalTimeout(String verId) =
      _CodeAutoRetrievalTimeout;

  const factory OtpLoginEvent.otpChanged(String otp) = _OtpChanged;

  const factory OtpLoginEvent.submitOTP() = _SubmitOTP;
  const factory OtpLoginEvent.initialize() = _Initialize;
  // const factory OtpLoginEvent.login() = _Login;
}
