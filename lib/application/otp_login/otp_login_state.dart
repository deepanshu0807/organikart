part of 'otp_login_bloc.dart';

// enum OTPLoginProcessState {
//   initial,
//   phoneNumberSubmitSuccess,
//   phoneNumberSubmitFailed,
//   waitingForOTP,
//   waitingForOTPExpired,
//   submitOTPSuccess,
//   submitOTPFailed,
//   otpVerified,
//   otpVerificationFailed,
// }

@freezed
abstract class OtpLoginState with _$OtpLoginState {
  const factory OtpLoginState({
    @required Option<Name> nameOption,
    @required PhoneNumber phoneNumber,
    @required OTP otp,
    @required String verId,
    @required bool showErrorMessages,
    @required OTPProcessState state,
    @required Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption,
  }) = _OtpLoginState;

  factory OtpLoginState.initial() => OtpLoginState(
        nameOption: none(),
        phoneNumber: PhoneNumber(''),
        otp: OTP(''),
        verId: "",
        showErrorMessages: false,
        state: const OTPProcessState.initial(),
        authFailureOrSuccessOption: none(),
      );
}
