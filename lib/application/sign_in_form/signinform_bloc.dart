import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';

import 'package:meta/meta.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'signinform_event.dart';
part 'signinform_state.dart';
part 'signinform_bloc.freezed.dart';

@injectable
class SignInFormBloc extends Bloc<SignInFormEvent, SignInFormState> {
  final IAuth _auth;
  SignInFormBloc(this._auth) : super(SignInFormState.initial());

  @override
  Stream<SignInFormState> mapEventToState(
    SignInFormEvent event,
  ) async* {
    yield* event.map(
      emailChanged: (e) async* {
        yield state.copyWith(
          emailAddress: EmailAddress(e.emailStr),
          isSubmitting: false,
          authFailureOrSuccessOption: none(),
        );
      },
      passwordChanged: (e) async* {
        yield state.copyWith(
          password: Password(e.passwordStr),
          isSubmitting: false,
          authFailureOrSuccessOption: none(),
        );
      },
      registerWithEmailAndPasswordPressed: (e) async* {},
      signInWithEmailAndPasswordPressed: _loginPressed,
    );
  }

  Stream<SignInFormState> _loginPressed(
      EvSignInWithEmailAndPasswordPressed e) async* {
    final isEValid = state.emailAddress.isValid();
    final isPValid = state.password.isValid();
    if (isEValid && isPValid) {
      yield state.copyWith(
        isSubmitting: true,
        authFailureOrSuccessOption: none(),
      );

      final failureOrSuccess = await _auth.loginEmailAndPassword(
        email: state.emailAddress,
        password: state.password,
      );

      yield state.copyWith(
        isSubmitting: false,
        authFailureOrSuccessOption: some(failureOrSuccess),
      );
    } else {
      yield state.copyWith(
        showErrorMessages: true,
        isSubmitting: false,
        authFailureOrSuccessOption: none(),
      );
    }
  }
}
