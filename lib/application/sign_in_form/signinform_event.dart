part of 'signinform_bloc.dart';

@freezed
abstract class SignInFormEvent with _$SignInFormEvent {
  const factory SignInFormEvent.emailChanged(String emailStr) = EvEmailChanged;
  const factory SignInFormEvent.passwordChanged(String passwordStr) =
      EvPasswordChanged;
  const factory SignInFormEvent.registerWithEmailAndPasswordPressed() =
      EvRegisterWithEmailAndPasswordPressed;
  const factory SignInFormEvent.signInWithEmailAndPasswordPressed() =
      EvSignInWithEmailAndPasswordPressed;
}
