part of 'customer_details_watcher_bloc.dart';

@freezed
abstract class CustomerDetailsWatcherEvent with _$CustomerDetailsWatcherEvent {
  const factory CustomerDetailsWatcherEvent.getMySavedDetails(
    OrganikartUser authUser,
  ) = _GetMySavedDetails;
  const factory CustomerDetailsWatcherEvent.mySavedDetailsReceived(
      Either<InfraFailure, OrganikartUser> storeUser,
      OrganikartUser authUser) = _MySavedDetailsReceive;
}
