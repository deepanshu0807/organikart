// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'customer_details_watcher_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

/// @nodoc
class _$CustomerDetailsWatcherEventTearOff {
  const _$CustomerDetailsWatcherEventTearOff();

// ignore: unused_element
  _GetMySavedDetails getMySavedDetails(OrganikartUser authUser) {
    return _GetMySavedDetails(
      authUser,
    );
  }

// ignore: unused_element
  _MySavedDetailsReceive mySavedDetailsReceived(
      Either<InfraFailure, OrganikartUser> storeUser, OrganikartUser authUser) {
    return _MySavedDetailsReceive(
      storeUser,
      authUser,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CustomerDetailsWatcherEvent = _$CustomerDetailsWatcherEventTearOff();

/// @nodoc
mixin _$CustomerDetailsWatcherEvent {
  OrganikartUser get authUser;

  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getMySavedDetails(OrganikartUser authUser),
    @required
        TResult mySavedDetailsReceived(
            Either<InfraFailure, OrganikartUser> storeUser,
            OrganikartUser authUser),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getMySavedDetails(OrganikartUser authUser),
    TResult mySavedDetailsReceived(
        Either<InfraFailure, OrganikartUser> storeUser,
        OrganikartUser authUser),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getMySavedDetails(_GetMySavedDetails value),
    @required TResult mySavedDetailsReceived(_MySavedDetailsReceive value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getMySavedDetails(_GetMySavedDetails value),
    TResult mySavedDetailsReceived(_MySavedDetailsReceive value),
    @required TResult orElse(),
  });

  $CustomerDetailsWatcherEventCopyWith<CustomerDetailsWatcherEvent>
      get copyWith;
}

/// @nodoc
abstract class $CustomerDetailsWatcherEventCopyWith<$Res> {
  factory $CustomerDetailsWatcherEventCopyWith(
          CustomerDetailsWatcherEvent value,
          $Res Function(CustomerDetailsWatcherEvent) then) =
      _$CustomerDetailsWatcherEventCopyWithImpl<$Res>;
  $Res call({OrganikartUser authUser});

  $OrganikartUserCopyWith<$Res> get authUser;
}

/// @nodoc
class _$CustomerDetailsWatcherEventCopyWithImpl<$Res>
    implements $CustomerDetailsWatcherEventCopyWith<$Res> {
  _$CustomerDetailsWatcherEventCopyWithImpl(this._value, this._then);

  final CustomerDetailsWatcherEvent _value;
  // ignore: unused_field
  final $Res Function(CustomerDetailsWatcherEvent) _then;

  @override
  $Res call({
    Object authUser = freezed,
  }) {
    return _then(_value.copyWith(
      authUser:
          authUser == freezed ? _value.authUser : authUser as OrganikartUser,
    ));
  }

  @override
  $OrganikartUserCopyWith<$Res> get authUser {
    if (_value.authUser == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.authUser, (value) {
      return _then(_value.copyWith(authUser: value));
    });
  }
}

/// @nodoc
abstract class _$GetMySavedDetailsCopyWith<$Res>
    implements $CustomerDetailsWatcherEventCopyWith<$Res> {
  factory _$GetMySavedDetailsCopyWith(
          _GetMySavedDetails value, $Res Function(_GetMySavedDetails) then) =
      __$GetMySavedDetailsCopyWithImpl<$Res>;
  @override
  $Res call({OrganikartUser authUser});

  @override
  $OrganikartUserCopyWith<$Res> get authUser;
}

/// @nodoc
class __$GetMySavedDetailsCopyWithImpl<$Res>
    extends _$CustomerDetailsWatcherEventCopyWithImpl<$Res>
    implements _$GetMySavedDetailsCopyWith<$Res> {
  __$GetMySavedDetailsCopyWithImpl(
      _GetMySavedDetails _value, $Res Function(_GetMySavedDetails) _then)
      : super(_value, (v) => _then(v as _GetMySavedDetails));

  @override
  _GetMySavedDetails get _value => super._value as _GetMySavedDetails;

  @override
  $Res call({
    Object authUser = freezed,
  }) {
    return _then(_GetMySavedDetails(
      authUser == freezed ? _value.authUser : authUser as OrganikartUser,
    ));
  }
}

/// @nodoc
class _$_GetMySavedDetails implements _GetMySavedDetails {
  const _$_GetMySavedDetails(this.authUser) : assert(authUser != null);

  @override
  final OrganikartUser authUser;

  @override
  String toString() {
    return 'CustomerDetailsWatcherEvent.getMySavedDetails(authUser: $authUser)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _GetMySavedDetails &&
            (identical(other.authUser, authUser) ||
                const DeepCollectionEquality()
                    .equals(other.authUser, authUser)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(authUser);

  @override
  _$GetMySavedDetailsCopyWith<_GetMySavedDetails> get copyWith =>
      __$GetMySavedDetailsCopyWithImpl<_GetMySavedDetails>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getMySavedDetails(OrganikartUser authUser),
    @required
        TResult mySavedDetailsReceived(
            Either<InfraFailure, OrganikartUser> storeUser,
            OrganikartUser authUser),
  }) {
    assert(getMySavedDetails != null);
    assert(mySavedDetailsReceived != null);
    return getMySavedDetails(authUser);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getMySavedDetails(OrganikartUser authUser),
    TResult mySavedDetailsReceived(
        Either<InfraFailure, OrganikartUser> storeUser,
        OrganikartUser authUser),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getMySavedDetails != null) {
      return getMySavedDetails(authUser);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getMySavedDetails(_GetMySavedDetails value),
    @required TResult mySavedDetailsReceived(_MySavedDetailsReceive value),
  }) {
    assert(getMySavedDetails != null);
    assert(mySavedDetailsReceived != null);
    return getMySavedDetails(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getMySavedDetails(_GetMySavedDetails value),
    TResult mySavedDetailsReceived(_MySavedDetailsReceive value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (getMySavedDetails != null) {
      return getMySavedDetails(this);
    }
    return orElse();
  }
}

abstract class _GetMySavedDetails implements CustomerDetailsWatcherEvent {
  const factory _GetMySavedDetails(OrganikartUser authUser) =
      _$_GetMySavedDetails;

  @override
  OrganikartUser get authUser;
  @override
  _$GetMySavedDetailsCopyWith<_GetMySavedDetails> get copyWith;
}

/// @nodoc
abstract class _$MySavedDetailsReceiveCopyWith<$Res>
    implements $CustomerDetailsWatcherEventCopyWith<$Res> {
  factory _$MySavedDetailsReceiveCopyWith(_MySavedDetailsReceive value,
          $Res Function(_MySavedDetailsReceive) then) =
      __$MySavedDetailsReceiveCopyWithImpl<$Res>;
  @override
  $Res call(
      {Either<InfraFailure, OrganikartUser> storeUser,
      OrganikartUser authUser});

  @override
  $OrganikartUserCopyWith<$Res> get authUser;
}

/// @nodoc
class __$MySavedDetailsReceiveCopyWithImpl<$Res>
    extends _$CustomerDetailsWatcherEventCopyWithImpl<$Res>
    implements _$MySavedDetailsReceiveCopyWith<$Res> {
  __$MySavedDetailsReceiveCopyWithImpl(_MySavedDetailsReceive _value,
      $Res Function(_MySavedDetailsReceive) _then)
      : super(_value, (v) => _then(v as _MySavedDetailsReceive));

  @override
  _MySavedDetailsReceive get _value => super._value as _MySavedDetailsReceive;

  @override
  $Res call({
    Object storeUser = freezed,
    Object authUser = freezed,
  }) {
    return _then(_MySavedDetailsReceive(
      storeUser == freezed
          ? _value.storeUser
          : storeUser as Either<InfraFailure, OrganikartUser>,
      authUser == freezed ? _value.authUser : authUser as OrganikartUser,
    ));
  }
}

/// @nodoc
class _$_MySavedDetailsReceive implements _MySavedDetailsReceive {
  const _$_MySavedDetailsReceive(this.storeUser, this.authUser)
      : assert(storeUser != null),
        assert(authUser != null);

  @override
  final Either<InfraFailure, OrganikartUser> storeUser;
  @override
  final OrganikartUser authUser;

  @override
  String toString() {
    return 'CustomerDetailsWatcherEvent.mySavedDetailsReceived(storeUser: $storeUser, authUser: $authUser)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MySavedDetailsReceive &&
            (identical(other.storeUser, storeUser) ||
                const DeepCollectionEquality()
                    .equals(other.storeUser, storeUser)) &&
            (identical(other.authUser, authUser) ||
                const DeepCollectionEquality()
                    .equals(other.authUser, authUser)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(storeUser) ^
      const DeepCollectionEquality().hash(authUser);

  @override
  _$MySavedDetailsReceiveCopyWith<_MySavedDetailsReceive> get copyWith =>
      __$MySavedDetailsReceiveCopyWithImpl<_MySavedDetailsReceive>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult getMySavedDetails(OrganikartUser authUser),
    @required
        TResult mySavedDetailsReceived(
            Either<InfraFailure, OrganikartUser> storeUser,
            OrganikartUser authUser),
  }) {
    assert(getMySavedDetails != null);
    assert(mySavedDetailsReceived != null);
    return mySavedDetailsReceived(storeUser, authUser);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult getMySavedDetails(OrganikartUser authUser),
    TResult mySavedDetailsReceived(
        Either<InfraFailure, OrganikartUser> storeUser,
        OrganikartUser authUser),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (mySavedDetailsReceived != null) {
      return mySavedDetailsReceived(storeUser, authUser);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult getMySavedDetails(_GetMySavedDetails value),
    @required TResult mySavedDetailsReceived(_MySavedDetailsReceive value),
  }) {
    assert(getMySavedDetails != null);
    assert(mySavedDetailsReceived != null);
    return mySavedDetailsReceived(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult getMySavedDetails(_GetMySavedDetails value),
    TResult mySavedDetailsReceived(_MySavedDetailsReceive value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (mySavedDetailsReceived != null) {
      return mySavedDetailsReceived(this);
    }
    return orElse();
  }
}

abstract class _MySavedDetailsReceive implements CustomerDetailsWatcherEvent {
  const factory _MySavedDetailsReceive(
      Either<InfraFailure, OrganikartUser> storeUser,
      OrganikartUser authUser) = _$_MySavedDetailsReceive;

  Either<InfraFailure, OrganikartUser> get storeUser;
  @override
  OrganikartUser get authUser;
  @override
  _$MySavedDetailsReceiveCopyWith<_MySavedDetailsReceive> get copyWith;
}

/// @nodoc
class _$CustomerDetailsWatcherStateTearOff {
  const _$CustomerDetailsWatcherStateTearOff();

// ignore: unused_element
  _Initial initial() {
    return const _Initial();
  }

// ignore: unused_element
  _DataTransferInProgress loadInProgress() {
    return const _DataTransferInProgress();
  }

// ignore: unused_element
  _LoadFailure loadFailure(InfraFailure<dynamic> failure) {
    return _LoadFailure(
      failure,
    );
  }

// ignore: unused_element
  _LoadSuccess loadSuccess(OrganikartUser storeUser) {
    return _LoadSuccess(
      storeUser,
    );
  }
}

/// @nodoc
// ignore: unused_element
const $CustomerDetailsWatcherState = _$CustomerDetailsWatcherStateTearOff();

/// @nodoc
mixin _$CustomerDetailsWatcherState {
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult loadSuccess(OrganikartUser storeUser),
  });
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(OrganikartUser storeUser),
    @required TResult orElse(),
  });
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  });
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  });
}

/// @nodoc
abstract class $CustomerDetailsWatcherStateCopyWith<$Res> {
  factory $CustomerDetailsWatcherStateCopyWith(
          CustomerDetailsWatcherState value,
          $Res Function(CustomerDetailsWatcherState) then) =
      _$CustomerDetailsWatcherStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$CustomerDetailsWatcherStateCopyWithImpl<$Res>
    implements $CustomerDetailsWatcherStateCopyWith<$Res> {
  _$CustomerDetailsWatcherStateCopyWithImpl(this._value, this._then);

  final CustomerDetailsWatcherState _value;
  // ignore: unused_field
  final $Res Function(CustomerDetailsWatcherState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$CustomerDetailsWatcherStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc
class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'CustomerDetailsWatcherState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult loadSuccess(OrganikartUser storeUser),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(OrganikartUser storeUser),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements CustomerDetailsWatcherState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$DataTransferInProgressCopyWith<$Res> {
  factory _$DataTransferInProgressCopyWith(_DataTransferInProgress value,
          $Res Function(_DataTransferInProgress) then) =
      __$DataTransferInProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$DataTransferInProgressCopyWithImpl<$Res>
    extends _$CustomerDetailsWatcherStateCopyWithImpl<$Res>
    implements _$DataTransferInProgressCopyWith<$Res> {
  __$DataTransferInProgressCopyWithImpl(_DataTransferInProgress _value,
      $Res Function(_DataTransferInProgress) _then)
      : super(_value, (v) => _then(v as _DataTransferInProgress));

  @override
  _DataTransferInProgress get _value => super._value as _DataTransferInProgress;
}

/// @nodoc
class _$_DataTransferInProgress implements _DataTransferInProgress {
  const _$_DataTransferInProgress();

  @override
  String toString() {
    return 'CustomerDetailsWatcherState.loadInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _DataTransferInProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult loadSuccess(OrganikartUser storeUser),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadInProgress();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(OrganikartUser storeUser),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadInProgress != null) {
      return loadInProgress(this);
    }
    return orElse();
  }
}

abstract class _DataTransferInProgress implements CustomerDetailsWatcherState {
  const factory _DataTransferInProgress() = _$_DataTransferInProgress;
}

/// @nodoc
abstract class _$LoadFailureCopyWith<$Res> {
  factory _$LoadFailureCopyWith(
          _LoadFailure value, $Res Function(_LoadFailure) then) =
      __$LoadFailureCopyWithImpl<$Res>;
  $Res call({InfraFailure<dynamic> failure});

  $InfraFailureCopyWith<dynamic, $Res> get failure;
}

/// @nodoc
class __$LoadFailureCopyWithImpl<$Res>
    extends _$CustomerDetailsWatcherStateCopyWithImpl<$Res>
    implements _$LoadFailureCopyWith<$Res> {
  __$LoadFailureCopyWithImpl(
      _LoadFailure _value, $Res Function(_LoadFailure) _then)
      : super(_value, (v) => _then(v as _LoadFailure));

  @override
  _LoadFailure get _value => super._value as _LoadFailure;

  @override
  $Res call({
    Object failure = freezed,
  }) {
    return _then(_LoadFailure(
      failure == freezed ? _value.failure : failure as InfraFailure<dynamic>,
    ));
  }

  @override
  $InfraFailureCopyWith<dynamic, $Res> get failure {
    if (_value.failure == null) {
      return null;
    }
    return $InfraFailureCopyWith<dynamic, $Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc
class _$_LoadFailure implements _LoadFailure {
  const _$_LoadFailure(this.failure) : assert(failure != null);

  @override
  final InfraFailure<dynamic> failure;

  @override
  String toString() {
    return 'CustomerDetailsWatcherState.loadFailure(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadFailure &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @override
  _$LoadFailureCopyWith<_LoadFailure> get copyWith =>
      __$LoadFailureCopyWithImpl<_LoadFailure>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult loadSuccess(OrganikartUser storeUser),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadFailure(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(OrganikartUser storeUser),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadFailure != null) {
      return loadFailure(this);
    }
    return orElse();
  }
}

abstract class _LoadFailure implements CustomerDetailsWatcherState {
  const factory _LoadFailure(InfraFailure<dynamic> failure) = _$_LoadFailure;

  InfraFailure<dynamic> get failure;
  _$LoadFailureCopyWith<_LoadFailure> get copyWith;
}

/// @nodoc
abstract class _$LoadSuccessCopyWith<$Res> {
  factory _$LoadSuccessCopyWith(
          _LoadSuccess value, $Res Function(_LoadSuccess) then) =
      __$LoadSuccessCopyWithImpl<$Res>;
  $Res call({OrganikartUser storeUser});

  $OrganikartUserCopyWith<$Res> get storeUser;
}

/// @nodoc
class __$LoadSuccessCopyWithImpl<$Res>
    extends _$CustomerDetailsWatcherStateCopyWithImpl<$Res>
    implements _$LoadSuccessCopyWith<$Res> {
  __$LoadSuccessCopyWithImpl(
      _LoadSuccess _value, $Res Function(_LoadSuccess) _then)
      : super(_value, (v) => _then(v as _LoadSuccess));

  @override
  _LoadSuccess get _value => super._value as _LoadSuccess;

  @override
  $Res call({
    Object storeUser = freezed,
  }) {
    return _then(_LoadSuccess(
      storeUser == freezed ? _value.storeUser : storeUser as OrganikartUser,
    ));
  }

  @override
  $OrganikartUserCopyWith<$Res> get storeUser {
    if (_value.storeUser == null) {
      return null;
    }
    return $OrganikartUserCopyWith<$Res>(_value.storeUser, (value) {
      return _then(_value.copyWith(storeUser: value));
    });
  }
}

/// @nodoc
class _$_LoadSuccess implements _LoadSuccess {
  const _$_LoadSuccess(this.storeUser) : assert(storeUser != null);

  @override
  final OrganikartUser storeUser;

  @override
  String toString() {
    return 'CustomerDetailsWatcherState.loadSuccess(storeUser: $storeUser)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadSuccess &&
            (identical(other.storeUser, storeUser) ||
                const DeepCollectionEquality()
                    .equals(other.storeUser, storeUser)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(storeUser);

  @override
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith =>
      __$LoadSuccessCopyWithImpl<_LoadSuccess>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object>({
    @required TResult initial(),
    @required TResult loadInProgress(),
    @required TResult loadFailure(InfraFailure<dynamic> failure),
    @required TResult loadSuccess(OrganikartUser storeUser),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadSuccess(storeUser);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object>({
    TResult initial(),
    TResult loadInProgress(),
    TResult loadFailure(InfraFailure<dynamic> failure),
    TResult loadSuccess(OrganikartUser storeUser),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(storeUser);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object>({
    @required TResult initial(_Initial value),
    @required TResult loadInProgress(_DataTransferInProgress value),
    @required TResult loadFailure(_LoadFailure value),
    @required TResult loadSuccess(_LoadSuccess value),
  }) {
    assert(initial != null);
    assert(loadInProgress != null);
    assert(loadFailure != null);
    assert(loadSuccess != null);
    return loadSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object>({
    TResult initial(_Initial value),
    TResult loadInProgress(_DataTransferInProgress value),
    TResult loadFailure(_LoadFailure value),
    TResult loadSuccess(_LoadSuccess value),
    @required TResult orElse(),
  }) {
    assert(orElse != null);
    if (loadSuccess != null) {
      return loadSuccess(this);
    }
    return orElse();
  }
}

abstract class _LoadSuccess implements CustomerDetailsWatcherState {
  const factory _LoadSuccess(OrganikartUser storeUser) = _$_LoadSuccess;

  OrganikartUser get storeUser;
  _$LoadSuccessCopyWith<_LoadSuccess> get copyWith;
}
