part of 'customer_details_watcher_bloc.dart';

@freezed
abstract class CustomerDetailsWatcherState with _$CustomerDetailsWatcherState {
  const factory CustomerDetailsWatcherState.initial() = _Initial;
  const factory CustomerDetailsWatcherState.loadInProgress() =
      _DataTransferInProgress;
  const factory CustomerDetailsWatcherState.loadFailure(InfraFailure failure) =
      _LoadFailure;
  const factory CustomerDetailsWatcherState.loadSuccess(
      OrganikartUser storeUser) = _LoadSuccess;
}
