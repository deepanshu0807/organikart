import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:dartz/dartz.dart';
import 'package:organikart_shared/domain/features/i_customer_repo.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

part 'customer_details_watcher_event.dart';
part 'customer_details_watcher_state.dart';
part 'customer_details_watcher_bloc.freezed.dart';

@lazySingleton
class CustomerDetailsWatcherBloc
    extends Bloc<CustomerDetailsWatcherEvent, CustomerDetailsWatcherState> {
  final ICustomerRepo iCustomerRepo;
  CustomerDetailsWatcherBloc(this.iCustomerRepo)
      : super(const CustomerDetailsWatcherState.initial());

  StreamSubscription stream;

  @override
  Stream<CustomerDetailsWatcherState> mapEventToState(
    CustomerDetailsWatcherEvent event,
  ) async* {
    yield* event.map(
      getMySavedDetails: (e) async* {
        if (stream == null) {
          yield const CustomerDetailsWatcherState.loadInProgress();
          stream = iCustomerRepo.getStoreUser(e.authUser).listen((c) => add(
              CustomerDetailsWatcherEvent.mySavedDetailsReceived(
                  c, e.authUser)));
        }
      },
      mySavedDetailsReceived: (e) async* {
        yield e.storeUser.fold(
          (f) {
            return CustomerDetailsWatcherState.loadFailure(f);
          },
          (c) {
            return CustomerDetailsWatcherState.loadSuccess(c);
          },
        );
      },
    );
  }

  @override
  Future<void> close() {
    stream?.cancel();
    return super.close();
  }
}
