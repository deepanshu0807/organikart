import 'package:organikart/presentation/view/introductory_view/sign_in.dart';
import 'package:organikart/presentation/view/introductory_view/signup.dart';
import 'package:organikart/presentation/view/introductory_view/splashview.dart';
import 'package:organikart/presentation/view/introductory_view/walkthrough_view.dart';
import 'package:organikart/presentation/view/main_view/main_page.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

@MaterialAutoRouter(
    generateNavigationHelperExtension: true,
    preferRelativeImports: true,
    pathPrefix: "",
    routesClassName: 'Routes',
    routes: <AutoRoute>[
      MaterialRoute(page: SplashView, name: "splash", initial: true),
      MaterialRoute(page: WalkThroughView, name: "onboarding"),
      MaterialRoute(page: SignIn, name: "signin"),
      MaterialRoute(page: SignUp, name: "signup"),
      MaterialRoute(page: MainPage, name: "main"),
    ])
class $AppRouter {}
