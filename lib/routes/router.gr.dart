// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../presentation/view/introductory_view/sign_in.dart';
import '../presentation/view/introductory_view/signup.dart';
import '../presentation/view/introductory_view/splashview.dart';
import '../presentation/view/introductory_view/walkthrough_view.dart';
import '../presentation/view/main_view/main_page.dart';

class Routes {
  static const String splash = '/';
  static const String onboarding = 'walk-through-view';
  static const String signin = 'sign-in';
  static const String signup = 'sign-up';
  static const String main = 'main-page';
  static const all = <String>{
    splash,
    onboarding,
    signin,
    signup,
    main,
  };
}

class AppRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.splash, page: SplashView),
    RouteDef(Routes.onboarding, page: WalkThroughView),
    RouteDef(Routes.signin, page: SignIn),
    RouteDef(Routes.signup, page: SignUp),
    RouteDef(Routes.main, page: MainPage),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    SplashView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SplashView(),
        settings: data,
      );
    },
    WalkThroughView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => WalkThroughView(),
        settings: data,
      );
    },
    SignIn: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SignIn(),
        settings: data,
      );
    },
    SignUp: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SignUp(),
        settings: data,
      );
    },
    MainPage: (data) {
      final args = data.getArgs<MainPageArguments>(
        orElse: () => MainPageArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => MainPage(
          key: args.key,
          title: args.title,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension AppRouterExtendedNavigatorStateX on ExtendedNavigatorState {
  Future<dynamic> pushSplash() => push<dynamic>(Routes.splash);

  Future<dynamic> pushOnboarding() => push<dynamic>(Routes.onboarding);

  Future<dynamic> pushSignin() => push<dynamic>(Routes.signin);

  Future<dynamic> pushSignup() => push<dynamic>(Routes.signup);

  Future<dynamic> pushMain({
    Key key,
    String title,
  }) =>
      push<dynamic>(
        Routes.main,
        arguments: MainPageArguments(key: key, title: title),
      );
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// MainPage arguments holder class
class MainPageArguments {
  final Key key;
  final String title;
  MainPageArguments({this.key, this.title});
}
