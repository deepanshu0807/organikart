import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesManager {

  /*
  * Instantiation of the SharedPreferences library, its variable
  * */
  static final _isWalkThrough = "isWalkThrough";

  /*
  * Method returns whether WalkThrough done, if not false
  * */
  static Future<bool> getIsWalkThrough() async {
    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getBool(_isWalkThrough) ?? false;
  }

  /*
  * Method that saves whether WalkThrough is done
  * */
  static Future<bool> setIsWalkThrough(bool value) async {
    final SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setBool(_isWalkThrough, value);
  }

}