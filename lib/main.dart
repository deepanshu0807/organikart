import 'package:flutter/material.dart';
import 'package:organikart/routes/router.gr.dart';
import 'package:organikart_shared/application/auth/auth_watcher_bloc.dart';
import 'package:organikart_shared/application/category_watcher_bloc/category_watcher_bloc.dart';
import 'package:organikart_shared/organikart_shared_package.dart';

import 'application/basket_form_bloc/basket_form_bloc.dart';
import 'application/customer_details_form_bloc/customer_details_form_bloc.dart';
import 'application/customer_details_watcher_bloc/customer_details_watcher_bloc.dart';
import 'application/saved_product_form_bloc/saved_product_form_bloc.dart';
import 'application/saved_product_watcher_bloc/saved_product_watcher_bloc.dart';
import 'application/sign_in_form/signinform_bloc.dart';
import 'application/sign_up_form/signupform_bloc.dart';
import 'config/di/injection.dart';
import 'presentation/util/utility.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  configureInjection(Environment.prod);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        ScreenUtil.init(
          constraints,
          designSize: const Size(375, 812),
          allowFontScaling: true,
        );
        return MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (_) => getIt<AuthWatcherBloc>()
                ..add(
                  const AuthWatcherEvent.authCheckRequested(),
                ),
            ),
            BlocProvider(create: (_) => getIt<SignInFormBloc>()),
            BlocProvider(create: (_) => getIt<SignupformBloc>()),
            BlocProvider(create: (_) => getIt<CustomerDetailsWatcherBloc>()),
            BlocProvider(create: (_) => getIt<SavedProductWatcherBloc>()),
            BlocProvider(create: (_) => getIt<SavedProductFormBloc>()),
            BlocProvider(create: (_) => getIt<CustomerDetailsFormBloc>()),
            BlocProvider(create: (_) => getIt<BasketFormBloc>()),
            BlocProvider(
                create: (_) => getIt<CategoryWatcherBloc>()
                  ..add(CategoryWatcherEvent.getCategories())),
            BlocProvider(
                create: (_) => getIt<ContentWatcherBloc>()
                  ..add(ContentWatcherEvent.getContent())),
          ],
          child: MaterialApp(
            title: 'Organikart',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primaryColor: AppColors.getPrimaryColor(),
              accentColor: AppColors.getSecondaryColor(),
              splashColor: AppColors.getSecondaryColor(),
              highlightColor: AppColors.getPrimaryColor(),
            ),
            // home: SplashView(),
            builder: (context, child) {
              return ExtendedNavigator<AppRouter>(
                router: AppRouter(),
              );
            },
          ),
        );
      },
    );
  }
}
